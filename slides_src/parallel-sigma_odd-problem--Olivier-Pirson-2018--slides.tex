% -*- coding: utf-8 -*-
\documentclass[a4paper,ps2pdf,9pt]{beamer}
\usepackage{sty/opislides}
\usepackage{sty/checked}
\title{Parallel Numerical Verification of the sigma\_odd problem}

\def\EMAILOPi{olivier.pirson.opi@gmail.com}
\def\OPILINK{http://www.opimedia.be/}
\def\BITBUCKETLINK{https://bitbucket.org/OPiMedia/parallel-sigma\_odd-problem}
\def\SLIDESLINK{https://speakerdeck.com/opimedia/parallel-numerical-verification-of-the-s-odd-problem}

\newcommand{\emailOPi}{\href{mailto:\EMAILOPi?subject=[sigma_odd]}{\color{black}\path|\EMAILOPi|}}

\author{Olivier \textsc{Pirson}}

\newcommand{\course}{INFO-Y100 (4004940ENR) Parallel systems}

\institute{}
\def\LASTDATE{September 11, 2019}
\date{\LASTDATE}

\newcommand{\keywordsPDF}{divisor function, sigma\_odd problem, varsigma\_odd,
  parallelism, multi-threading, message-passing, GPU, OpenCL}

\hypersetup{pdfauthor={Olivier Pirson <\EMAILOPi>}}
\hypersetup{pdfsubject={Experiment parallel technologies (multi-threading, message-passing and OpenCL) on the numerical verification of the sigma\_odd problem.}}
\hypersetup{pdfkeywords={\keywordsPDF}}

\logo{\includegraphics[height=30pt]{eps/graphs/graph_circular_1_1001}}

\sloppy

\begin{document}
\begin{frame}[plain]
  %\printDraft
  \hypersetup{urlcolor=black}%
  \null\hspace*{-2.5em}%
  \parbox{\textwidth}{
    \centering\footnotesize
    \href{https://www.ulb.be/}{\color{black}\textsc{Université Libre de Bruxelles}}\\
    \textsc{\href{https://www.ulb.be/facs/sciences/info/}
      {\color{black}Computer Science Department}}

    \medskip
    \href{http://parallel.vub.ac.be/education/parsys/}{\color{black}\textsc{\course}}\\

    \medskip
    \textsc{Project}\\
    \rule{\linewidth}{0.2mm}\\
    {\LARGE\scshape
      \href{\SLIDESLINK}{Parallel}\\
      \href{\SLIDESLINK}{Numerical Verification of}\\[1ex]
      \href{\SLIDESLINK}{the $\sigmaodd$ problem}}
    \rule{\linewidth}{0.2mm}\\[1ex]
    \textsc{Presentation}

    \smallskip
    \includegraphics[width=\widthof{\textsc{Presentation}}]{eps/varsigma_odd_problem}

    \smallskip
    \href{http://www.opimedia.be/}{\color{black}Olivier \textsc{Pirson}}
    --- \emailOPi

    \href{https://orcid.org/0000-0001-6296-9659}
         {\color{black}\includegraphics[height=1.5ex]{eps/ORCID}%
           \ \path|orcid.org/0000-0001-6296-9659|}

    \smallskip
    {December 15, 2017\tiny\\
      (Last modifications: \LASTDATE)}

    \medskip
    {\scriptsize\href{\SLIDESLINK}{\includegraphics[height=\heightof{h}]{eps/Speaker-Deck}\ %
        \path|https://{\small speakerdeck.com/opimedia}/parallel-numerical-verification-of-the-s-odd-problem|}}

    \medskip
    \href{https://www.ulb.be/}{\includegraphics[height=0.75cm]{eps/ULB}}\hspace{0.75cm}%
    \href{https://www.vub.be/en/}{\includegraphics[height=0.75cm]{eps/VUB}}}
\end{frame}

\author{}
\date{}
\title{\mbox{}\hspace*{-45em}\linebreak
  Parallel Numerical Verification of the $\sigmaodd$ problem}


\AtBeginSection{
  \begin{frame}
    \tableofcontents[currentsection]
  \end{frame}
}



\section{The problem}
\begin{frame}
  \frametitle{The $\sigmaodd$ and $\varsigmaodd$ functions}

  $\emphDef{\sigma(n)} =$ sum of all divisors of $n$\hfill
  (sigma)\\
  $\emphDef{\sigmaodd(n)} =$ sum of \textit{odd} divisors of $n$\hfill
  (sigma odd)

  \bigskip
  All divisors of 18: $\{1, 2, 3, 6, 9, 18\}$\\
  Only odd divisors: $\{1, 3, 9\}$
  \hfil
  so $\sigmaodd(18) = 13$

  \medskip
  All divisors of 19: $\{1, 19\}$\\
  Only odd divisors: $\{1, 19\}$
  \hfil
  so $\sigmaodd(19) = 20$

  \bigskip
  $\emphDef{\varsigmaodd(n)} = \sigmaodd(n)$ divided by 2 until to be odd\hfill
  (varsigma odd)

  $\varsigmaodd(18) = 13$

  $\varsigmaodd(19) = 5$

  \bigskip
  \small
  \input{tex/table/first_values.tex}%
\end{frame}


\begin{frame}
  \frametitle{The $\sigmaodd$ problem: an iteration problem}

  \bigskip
  We iterate the $\varsigmaodd$ (or equivalently $\sigmaodd$) function\\
  and we observe that we always reach 1.

  \bigskip
  \bigskip
  \bigskip
  Numbers in {\color{orange}orange} are square numbers.

  For all $n$ odd and square number ($\neq 1$):\\
  $\varsigmaodd(n) = \sigmaodd(n) > n$

  \bigskip
  But we observe that for almost other odd numbers $n$:\\
  $\varsigmaodd(n) < n$

  \bigskip
  \bigskip
  \bigskip
  Note that even numbers are not interesting\\
  for this problem, because\\
  $\sigmaodd(2n) = \sigmaodd(n)$.\\
  and $\varsigmaodd(2n) = \varsigmaodd(n)$.

  \mbox{}\\[-61ex]
  \null\hfill\hrefIncludegraphics[height=0.95\textheight]{graphs/graph_1_85}
\end{frame}


\begin{frame}
  \frametitle{The $\sigmaodd$ problem: an iteration problem}
  The point in the middle of this picture is the number 1.

  \medskip
  \centering
  \hrefIncludegraphics[height=7cm]{graphs/graph_circular_1_1001}%
\end{frame}


\begin{frame}
  \frametitle{The $\sigmaodd$ problem is a conjecture}
  Does the iteration always reaches 1?

  \bigskip
  The $\sigmaodd$ problem
  is the \textbf{conjecture} that is \textbf{always} true,\\
  what ever the starting number (integer $\geq 1$).

  \bigskip
  Successfully checked
  for each $n$ until $\varsigmaoddChecked$\\
  with programs developed for this work.

  Previous result known was $2^{30}$.

  \bigskip
  Moreover,
  \fbox{$n \leq 10^{11}\ \Longrightarrow\ \varsigmaodd^{15}(n) = 1$}
\end{frame}



\section{Computation}
\subsection[Simple algo.]{Simple algorithm}
\begin{frame}
  \frametitle{Numerical verification by the simple direct algorithm}
  For each odd number:

  \begin{algorithm}[H]
    \Suppressnumber
    \
    \lstinputlisting[morekeywords={print,to}]{txt/algo/first_check_varsigma_odd.txt}
    \caption{\texttt{first\_check\_varsigma\_odd(first\_n, last\_n)}}
  \end{algorithm}
\end{frame}


\begin{frame}
  \frametitle{Numerical verification by the simple direct algorithm}
  Simply iterate $\varsigmaodd$ until to have a little number:

  \begin{algorithm}[H]
    \Suppressnumber
    \lstinputlisting[morekeywords={exit,print,to}]{txt/algo/first_iterate_varsigma_odd_until_lower.txt}
    \caption{\texttt{first\_iterate\_varsigma\_odd\_until\_lower(n)}}
  \end{algorithm}
\end{frame}


\begin{frame}
  \frametitle{Computation of $\sigmaodd(n)$}

  Assume $n$ odd:

  \bigskip
  $n = p_1^{\alpha_1} \times p_2^{\alpha_2} \times p_3^{\alpha_3}
  \times \dots \times p_k^{\alpha_k}$
  with $p_i$ distinct prime numbers

  \bigskip
  $\sigmaodd(n) = \frac{p_1^{\alpha+1} - 1}{p_1 - 1}
  \times \frac{p_2^{\alpha+1} - 1}{p_2 - 1}
  \times \frac{p_3^{\alpha+1} - 1}{p_3 - 1}
  \times \dots
  \times \frac{p_k^{\alpha+1} - 1}{p_k - 1}$

  \bigskip
  \bigskip
  Thus, to verify the conjecture we must factorize\\
  (other ways are less efficient).
\end{frame}


\subsection{Better algorithm}
\begin{frame}
  \frametitle{Use properties to avoid a lot of computations}
  For each $n$,
  we want to check there exists
  $k$ such that
  $\sigmaodd^k(n) = 1$

  \begin{itemize}
  \item
    It is equivalent to check there exists
    $k$
    such that
    $\varsigmaodd^k(n) < n$.\\
    That reduces the path that will be compute.
  \item
    Only odd numbers must be check (50\%).
  \item
    Other numbers can be avoided (remains $\simeq 33$\%).
  \item
    Almost numbers reach smaller number in \textbf{only one step}!

    Exceptions identified \textit{before} computation: square numbers.

    The other exceptions (called \emphDef{bad} numbers) are very rare.

    \medskip
    So instead to iterate we will compute only one step\\
    and \textbf{keep exceptions} that will be check separately
    (very fast).
  \item
    $\varsigmaodd(a b) \leq \varsigmaodd(a)\,\varsigmaodd(b)$\\
    \quad$\longrightarrow$ shortcut in the factorization (the most heavy work)\\
    (with use of previous known bad numbers\\
    or with general upper bound).
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Transformed problem}
  With these properties
  we have transformed
  the necessity to compute
  the \textbf{complete iteration} of $\sigmaodd$\\
  (and thus the \textbf{complete factorization})\\
  of \textbf{each} number\\
  to this both \textit{improved}
  and \textit{simpler} (relatively to other possible optimizations) algorithm:

  \medskip
  compute \textbf{only one}\\
  (eventually \textbf{partially}) iteration of $\varsigmaodd$\\
  for only \textbf{some} numbers.

  \bigskip
  \bigskip
  \bigskip
  \flushright
  \textit{``The cheapest, fastest and most reliable components of a computer system
    are those that aren't there.''}\\
  --- Gordon \textsc{Bell}
\end{frame}


\begin{frame}
  \frametitle{Transformed problem}
  \hfill
  (\href{progs/docs/html/sequential_8hpp.html}{\texttt{progs/src/sequential/sequential/sequential.hpp}})\\[-3ex]
  \begin{algorithm}[H]
    \Suppressnumber
    \lstinputlisting[morekeywords={not,print,to}]{txt/algo/sequential_check_gentle_varsigma_odd.txt}
    \caption{\texttt{sequential\_check\_gentle\_varsigma\_odd(first\_n, last\_n)}}
  \end{algorithm}

  \forcenewline[-5ex]%
  $\emphDef{d \divides n}$ means that $d$ is a divisor of $n$.

  $\emphDef{d \exactlydivides n}$ means that $d$ is a divisor of $n$,
  but $d^2$ is not.
\end{frame}


\begin{frame}
  \frametitle{Transformed problem}
  Computes (eventually partially) $\varsigmaodd(n)$ by the factorization of $n$\\
  and returns True if and only if $\varsigmaodd(n) < n$.

  \forcenewline[-4ex]%
  \begin{algorithm}[H]
    \Suppressnumber
    \lstinputlisting[morekeywords={to,True}]{txt/algo/sequential_is_varsigma_odd_lower.txt}
    \caption{\texttt{sequential\_is\_varsigma\_odd\_lower(n, bad\_table, bad\_first\_n)}}
  \end{algorithm}
\end{frame}


\begin{frame}
  \frametitle{Factorization shortcut}
  When we found a prime factor,\\
  it may be possible to shortcut the complete factorization.

  \bigskip
  For example, with a first prime factor $p_1$ of $n$:\\
  $n = p_1^{\alpha_1} n'$

  \bigskip
  $\sigmaodd(n) = \frac{p_1^{\alpha+1} - 1}{p_1 - 1} \times \sigmaodd(n')$

  \bigskip
  $\sigmaodd(n) \leq \frac{p_1^{\alpha+1} - 1}{p_1 - 1}\
  \times$ upper bound of $\sigmaodd(n')
  \quad
  < n$?
  \textbf{If} yes, then \textbf{stop}

  \bigskip
  Upper bound always true:\\
  $\sigmaodd(n') \leq 2 n' \left\lceil\sqrt[8]{n'}\right\rceil$

  \bigskip
  It is the same for the $\varsigmaodd$ function,
  with some additional division(s) by 2.

  \bigskip
  And if $n'$ is \emphDef{gentle} (odd but neither square neither bad):\\
  $\varsigmaodd(n') < n'$
  \qquad
  (so it can be possible to shortcut ``often'').
\end{frame}


\begin{frame}
  \frametitle{Stop and restart}
  Note that the program can be stopped
  (or executed until some \texttt{last\_n})
  and restarted with the last value checked.

  \bigskip
  In fact, it is possible to compute different ranges of numbers separately
  (in the same time or not).

  \bigskip
  If all required numbers are checked
  (with odd square numbers and bad numbers checked,
  for example by the naive way, which is fast for these rare numbers)
  until number $N$,
  then the conclusion is for all $n$ such that $n \leq N$,
  the iteration of $\sigmaodd$ (and $\varsigmaodd$) from $n$ reaches 1
  (what we wanted to achieve).
\end{frame}



\section{Parallel implementations}
\begin{frame}
  \frametitle{Performance with one thread/process}
  First, the comparison between sequential,
  three multi-threading
  and two message-passing
  implementations
  (for only \textbf{one} thread/process).

  \smallskip
  \centering
  By checking numbers between 1 and \textbf{\nombre{20000001}}.\\
  On a personal computer with \textbf{4 cores}, 2 threads by core.

  \hrefIncludegraphics[height=6cm]{benchmark/compare}%
\end{frame}


\subsection{Multi-threads}
\begin{frame}
  \frametitle{Multi-threads (\href{https://en.cppreference.com/w/cpp/thread/thread}
    {thread} of \cpluspluslogo11)}
  \small
  3 different implementations:
  \hfill
  (\href{progs/docs/html/threads_8hpp.html}{\texttt{progs/src/threads/threads/threads.hpp}})
  \begin{itemize}
  \item
    \textbf{One by one}\\
    Each slave computes independently \textbf{one} number
    and sends a boolean to the master.
    The master also computes one number, and \textbf{waits} everybody.
    And so forth with next numbers.

    \medskip
    Silly implementation; just to try.
    Very inefficient.
    The barrier is a big limitation
    because each number has a different factorization time.
  \item
    \textbf{By range}\\
    Like one by one but each slave receives a \textbf{range} of numbers
    (by these extremities), computes and returns the (very little) set of bad numbers founds.
    The master computes a smaller range, and waits everybody.
    And so forth with next numbers.

    \medskip
    Really better because computation is more well balanced,
    due to an average of the factorization time.
  \item
    \textbf{``Dynamic''}\\
    Like by range, but the master do \textbf{not waits},
    gives new range when a slave is free,
    and computes also the rest of the time.

    \medskip
    \textbf{Very good occupation} for each thread
    (see graph in following slides).
  \end{itemize}

  All threads share the \textbf{same prime number tables}.
\end{frame}


\begin{frame}
  \frametitle{Multi-threads --- one by one}
  \hrefIncludegraphics[height=6cm]{benchmark/threads_one_by_one}%
\end{frame}


\begin{frame}
  \frametitle{Multi-threads --- by range}
  \hrefIncludegraphics[height=6cm]{benchmark/threads_by_range}%
\end{frame}


\begin{frame}
  \frametitle{Multi-threads --- ``dynamic''}
  \hrefIncludegraphics[height=6cm]{benchmark/threads_dynamic}%
\end{frame}


\subsection[Message-passing]
           {Message-passing (Open MPI)}
\begin{frame}
  \frametitle{Message-passing (\href{https://www.open-mpi.org/}{Open MPI})}
  2 implementations:
  \hfill
  (\href{progs/docs/html/mpi_8hpp.html}{\texttt{progs/src/mpi/mpi/mpi.hpp}})
  \begin{itemize}
  \item
    \textbf{One by one}\\
    One element, barrier.
    Very inefficient; just to try.
  \item
    \textbf{``Dynamic''}\\
    By range and does not wait.
  \end{itemize}

  \smallskip
  Same algorithms than for multi-threading.

  \smallskip
  But exchange information by messages.
  (That could be between different machines,
  but these results was computed on only one computer.)
  Little impact if size of range is important
  compared to the small quantity of these information.

  \smallskip
  Messages from the master to each slave:\\
  The unique number or the extremities of the range,
  and the new (rare) bad numbers found by other threads.

  \smallskip
  Messages from each slave to the master:\\
  A boolean or a array of the new (rare) bad numbers found.

  \medskip
  Main differences with multi-threading:
  \textbf{exchanges between processes},\\
  and each process have its \textbf{own prime numbers table}.
\end{frame}


\begin{frame}
  \frametitle{Message-passing --- ``dynamic''}
  \hrefIncludegraphics[height=6cm]{benchmark/mpi_dynamic}%
\end{frame}


\subsection{GPU (OpenCL)}
\begin{frame}
  \frametitle{GPU (\href{https://www.khronos.org/opencl/}{OpenCL})}
  Only one implementation:
  \hfill
  (\href{progs/docs/html/opencl_8hpp.html}{\texttt{progs/src/opencl/opencl/opencl.hpp}})

  \begin{itemize}
    \item
      \textbf{By list of numbers}\\
      The CPU selects a list of numbers to be check\\
      and sends them to the GPU.

      The GPU compute completely $\varsigma(n)$ for each $n$ received
      (without to use a list of bad numbers
      and without to shortcut the factorization).

      Then the GPU returns a corresponding list of booleans to the CPU.
      And so forth.
  \end{itemize}

  \bigskip
  Instead a direct computation of $\varsigma(n)$ during the factorization,\\
  this implementation \textbf{collects before all prime factors} of $n$.\\
  That makes it easier the parallel work.

  \bigskip
  The important improvements of the algorithm
  (the \textbf{shortcut} of the factorization)
  was also \textbf{removed},
  because that did not gave better results,
  due to the complexification of branching.
\end{frame}

\begin{frame}
  \frametitle{GPU (OpenCL): explanations of bad results}
  The computation is massively parallel (if big list of numbers).\\
  But the efficiency is limited
  by the difference of the factorization process for each number.
  The algorithm, by the nature of the computation of the problem
  by factorization,
  is more or less a random succession of conditional branches.
  And the nature of the parallel computation by GPUs loses a lot of power on that.

  \bigskip
  More the list of numbers is big
  and more the computation is \textit{ideally} parallel.
  But more this list is big
  and more the computation of each number disturbs the progress of the others.

  \bigskip
  Moreover,
  all numbers quickly factorized wait the end of the others.

  \bigskip
  Also,
  GPUs give the best of their power
  on floating point computations.
  This problem is an integer problem.

  \bigskip
  A completely different approach could be better.
\end{frame}

\begin{frame}
  \frametitle{GPU (OpenCL): old GPU used during tests}
  The poor performances on the OpenCL implementation\\
  are also due to the old GPU used:\\
  a graphic card NVIDIA quadro FX 1800 with 768 Mio.

  \bigskip
  This GPU has no cache for the global memory.\\
  And the main loop iterates on prime numbers in this global memory.

  \bigskip
  More modern GPU could use the native OpenCL function
  \href{https://www.khronos.org/registry/OpenCL/sdk/2.0/docs/man/xhtml/ctz.html}
       {\texttt{ctz}}
  (instead a loop).

  \bigskip
  \bigskip
  Nevertheless, with the maximum list of numbers possible for this GPU,
  the OpenCL implementation has a little (disappointing) gain of performance
  compared to the sequential implementation.
\end{frame}

\begin{frame}
  \frametitle{GPU (OpenCL) --- by list of numbers}
  \hrefIncludegraphics[height=6cm]{benchmark/opencl_powers2}%
\end{frame}



\section{Results}
\begin{frame}
  \frametitle{Results}
  Results are produced on a computer with only 4 cores,
  that explains the decrease in gains beginning at 5 cores.

  \bigskip
  Results with Open MPI are a little strange,
  because for some parameters they are better than the sequential
  implementation.
  It is like as if \texttt{mpirun} on the sequential program
  made it faster.\\
  Theoretically the overhead of the MPI implementation
  should be bigger than the multi-thread implementation,
  due to the communication between processes
  (but tests were made on a single computer).\\
  The implementation is almost identical to the multi-thread version
  and all computation results are identical,
  thus it must be correct.\\
  \textit{Maybe} the GCC compiler required with Open MPI
  optimizes better this code than the clang compiler
  used for sequential and multi-thread versions.\\
  \textit{Maybe} is due to a little imprecision in the measures.

  \bigskip
  The two better implementations
  (``dynamic'' algorithm with threads and Open MPI)
  are both pretty close to the ideal.
\end{frame}


\subsection{Speedup}
\begin{frame}
  \frametitle{Speedup}
  \hrefIncludegraphics[height=6cm]{benchmark/speedup}%
\end{frame}


\begin{frame}
  \frametitle{Speedup with OpenCL}
  \hrefIncludegraphics[height=6cm]{benchmark/speedup_opencl}%
\end{frame}


\subsection{Efficiency}
\begin{frame}
  \frametitle{Efficiency}
  \hrefIncludegraphics[height=6cm]{benchmark/efficiency}%
\end{frame}


\begin{frame}
  \frametitle{Efficiency with OpenCL}
  \hrefIncludegraphics[height=6cm]{benchmark/efficiency_opencl}%
\end{frame}


\subsection{Overhead}
\begin{frame}
  \frametitle{Overhead}
  \hrefIncludegraphics[height=6cm]{benchmark/overhead}%
\end{frame}


\begin{frame}
  \frametitle{Overhead only until 4 cores}
  \hrefIncludegraphics[height=6cm]{benchmark/overhead_4}%
\end{frame}


\begin{frame}
  \frametitle{Overhead with OpenCL}
  \hrefIncludegraphics[height=6cm]{benchmark/overhead_opencl}%
\end{frame}


\subsection{Benchmarks tables}
\begin{frame}
  \frametitle{Benchmarks table: sequential, threads \&{} message passing}
  \tiny
  \forcenewline[-9ex]%
  \begin{tabular}{@{}|@{\ }l@{\ }|@{\ }l@{\ }|@{\ }r@{\ }|@{\ }H@{}r@{\ }|@{\ }r@{\ }|@{\ }r@{\ }|@{\ }r@{\ }|@{}}
    \hline
    Technology & Algorithm & \# threads/process & ms & \text{Time in s} & Speedup & Efficiency & Overhead\\
    \hline
    \input{tex/table/not_opencl.tex}
    \hline
  \end{tabular}
\end{frame}


\begin{frame}
  \frametitle{Benchmarks table: OpenCL}
  \small
  \begin{tabular}{@{}|@{\ }H@{}H@{}r@{\ }|@{\ }Hr@{\ }|@{\ }r@{\ }|@{\ }r@{\ }|@{\ }r@{\ }|@{}}
    \hline
    & & Size list & ms & \text{Time in s} & Speedup & Efficiency & Overhead\\
    \hline
    \input{tex/table/opencl.tex}
    \hline
  \end{tabular}
\end{frame}


\section{}
\begin{frame}
  \frametitle{The end}
  \vfill
  \vfill
  \begin{center}
    All results, documents, \cpluspluslogo/OpenCL, \LaTeX{} sources\\
    and references are available on Bitbucket:\\
    \hrefShow{\BITBUCKETLINK}\\
    \href{\BITBUCKETLINK}{\includegraphics[height=3ex]{eps/Bitbucket}}
  \end{center}
\end{frame}

\end{document}
