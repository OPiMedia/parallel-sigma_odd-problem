#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
cumulative_distribution.py

Usage: cumulative_distribution.py column [skip]

  column   index of the column to be counted
  skip     number of the first lines to be skipped

Read a column of integers in a TSV file
and print a triple columns: n   (# of n)   n/(# of n).

January 19, 2018

GPLv3 --- Copyright (C) 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import sys


#
# Main
######
def main():
    """
    Main
    """
    column = 0
    skip = 0

    try:
        if len(sys.argv) > 1:
            column = int(sys.argv[1])
            if len(sys.argv) > 2:
                skip = int(sys.argv[2])
    except:
        exit(1)

    assert column >= 0, column
    assert skip >= 0, skip

    for _ in range(skip):
        sys.stdin.readline()

    nb = 0

    print('n\t#\tn/#')

    for line in sys.stdin:
        line = line.strip()
        pieces = line.split()

        try:
            n = int(pieces[column])
        except:
            break

        assert n > 0, n

        nb += 1
        print(n, nb, float(n)/nb, sep='\t')


if __name__ == '__main__':
    main()
