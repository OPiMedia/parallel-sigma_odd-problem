#!/bin/bash -l

#PBS -l nodes=1:ppn=1
#PBS -l walltime=0:10:00
#PBS -l mem=70mb
#PBS -l file=500mb
#PBS -o check_square_sequential.log
#PBS -e check_square_sequential.err
#PBS -N check_square_sequential


echo "=== Config module ==="
module purge
module load GCCcore


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/sequential/
./check_square --first 100000000001 --last 100000000000000 --print > sequential_check_square_100000000001_100000000000000.tsv


echo "=== Done ==="
