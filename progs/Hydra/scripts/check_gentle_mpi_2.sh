#!/bin/bash -l

#PBS -l nodes=2:ppn=1
#PBS -l walltime=4:30:00
#PBS -l mem=250mb
#PBS -l file=300mb
#PBS -o check_gentle_mpi_2.log
#PBS -e check_gentle_mpi_2.err
#PBS -N check_gentle_mpi_2


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --last 5000000001 > mpi_check_gentle_3_5000000001__2.tsv


echo "=== Done ==="
