#!/bin/bash -l

#PBS -l nodes=4:ppn=1
#PBS -l walltime=2:30:00
#PBS -l mem=320mb
#PBS -l file=400mb
#PBS -o check_gentle_mpi_4.log
#PBS -e check_gentle_mpi_4.err
#PBS -N check_gentle_mpi_4


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --last 5000000001 > mpi_check_gentle_3_5000000001__4.tsv


echo "=== Done ==="
