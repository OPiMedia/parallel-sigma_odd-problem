#!/bin/bash -l

#PBS -l nodes=1:ppn=1
#PBS -l walltime=4:20:00
#PBS -l mem=70mb
#PBS -l file=60mb
#PBS -o check_gentle_sequential.log
#PBS -e check_gentle_sequential.err
#PBS -N check_gentle_sequential


echo "=== Config module ==="
module purge
module load GCCcore


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/sequential/
./check_gentle_sequential --last 5000000001 > sequential_check_gentle_3_5000000001.tsv


echo "=== Done ==="
