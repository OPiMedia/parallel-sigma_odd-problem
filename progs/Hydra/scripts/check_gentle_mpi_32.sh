#!/bin/bash -l

#PBS -l nodes=8:ppn=4
#PBS -l walltime=0:30:00
#PBS -l mem=2240mb
#PBS -l file=3200mb
#PBS -o check_gentle_mpi_32.log
#PBS -e check_gentle_mpi_32.err
#PBS -N check_gentle_mpi_32


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --last 5000000001 > mpi_check_gentle_3_5000000001__32.tsv


echo "=== Done ==="
