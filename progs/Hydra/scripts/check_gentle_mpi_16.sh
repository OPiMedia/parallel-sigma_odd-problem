#!/bin/bash -l

#PBS -l nodes=8:ppn=2
#PBS -l walltime=1:00:00
#PBS -l mem=1120mb
#PBS -l file=1600mb
#PBS -o check_gentle_mpi_16.log
#PBS -e check_gentle_mpi_16.err
#PBS -N check_gentle_mpi_16


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --last 5000000001 > mpi_check_gentle_3_5000000001__16.tsv


echo "=== Done ==="
