#!/bin/bash -l

#PBS -l nodes=8:ppn=1
#PBS -l walltime=1:30:00
#PBS -l mem=700mb
#PBS -l file=1200mb
#PBS -o check_gentle_mpi_8.log
#PBS -e check_gentle_mpi_8.err
#PBS -N check_gentle_mpi_8


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --last 5000000001 > mpi_check_gentle_3_5000000001__8.tsv


echo "=== Done ==="
