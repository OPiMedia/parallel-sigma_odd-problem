#!/bin/bash -l

#PBS -l nodes=1:ppn=8
#PBS -l walltime=0:40:00
#PBS -l mem=80mb
#PBS -l file=60mb
#PBS -o check_gentle_threads_8.log
#PBS -e check_gentle_threads_8.err
#PBS -N check_gentle_threads_8


echo "=== Config module ==="
module purge
module load GCCcore


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/threads/
./check_gentle_threads --dynamic --last 5000000001 --nb-thread 8 > threads_check_gentle_3_5000000001__8.tsv


echo "=== Done ==="
