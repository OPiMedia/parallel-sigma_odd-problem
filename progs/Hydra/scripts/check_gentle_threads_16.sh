#!/bin/bash -l

#PBS -l nodes=1:ppn=16
#PBS -l walltime=0:25:00
#PBS -l mem=90mb
#PBS -l file=60mb
#PBS -o check_gentle_threads_16.log
#PBS -e check_gentle_threads_16.err
#PBS -N check_gentle_threads_16


echo "=== Config module ==="
module purge
module load GCCcore


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/threads/
./check_gentle_threads --dynamic --last 5000000001 --nb-thread 16 > threads_check_gentle_3_5000000001__16.tsv


echo "=== Done ==="
