#!/bin/bash -l

#PBS -l nodes=1:ppn=1
#PBS -l walltime=0:01:00
#PBS -l mem=1mb
#PBS -l file=1mb
#PBS -o show_vars.log
#PBS -e show_vars.err
#PBS -N show_vars


echo "=== Config module ==="
module purge
module load GCCcore

echo "=== GCCcore on $HOSTNAME - " `date` " ==="
cd $HOME
echo "\$HOME: $HOME"
echo "\$LD_LIBRARY_PATH: $LD_LIBRARY_PATH"
echo "\$LIBRARY_PATH: $LIBRARY_PATH"
echo "\$TMPDIR: $TMPDIR"


echo "=== Config module ==="
module purge
module load OpenMPI

echo "=== OpenMPI on $HOSTNAME - " `date` " ==="
cd $HOME
echo "\$HOME: $HOME"
echo "\$LD_LIBRARY_PATH: $LD_LIBRARY_PATH"
echo "\$LIBRARY_PATH: $LIBRARY_PATH"
echo "\$TMPDIR: $TMPDIR"


echo "=== Done ==="
