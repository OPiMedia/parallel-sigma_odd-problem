#!/bin/bash -l

#PBS -l nodes=1:ppn=1
#PBS -l walltime=4:20:00
#PBS -l mem=100mb
#PBS -l file=60mb
#PBS -o check_gentle_mpi_1.log
#PBS -e check_gentle_mpi_1.err
#PBS -N check_gentle_mpi_1


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --last 5000000001 > mpi_check_gentle_3_5000000001__1.tsv


echo "=== Done ==="
