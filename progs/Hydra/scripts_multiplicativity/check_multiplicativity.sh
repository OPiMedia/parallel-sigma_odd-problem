#!/bin/bash -l

#PBS -l nodes=1:ppn=1
#PBS -l walltime=48:00:00
#PBS -l mem=70mb
#PBS -l file=500mb
#PBS -o check_multiplicativity.log
#PBS -e check_multiplicativity.err
#PBS -N check_multiplicativity


echo "=== Config module ==="
module purge
module load GCCcore


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/sequential/
./check_multiplicativity --last 1000001


echo "=== Done ==="
