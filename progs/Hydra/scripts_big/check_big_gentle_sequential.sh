#!/bin/bash -l

#PBS -l nodes=1:ppn=1
#PBS -l walltime=6:30:00
#PBS -l mem=75mb
#PBS -l file=60mb
#PBS -o check_big_gentle_sequential.log
#PBS -e check_big_gentle_sequential.err
#PBS -N check_big_gentle_sequential


echo "=== Config module ==="
module purge
module load GCCcore


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/sequential/
./check_gentle_sequential --first 5000000001 --last 10000000001 --bad-table ../../tables/sequential_check_bad_3_5000000001.tsv > sequential_check_gentle_5000000001_10000000001.tsv


echo "=== Done ==="
