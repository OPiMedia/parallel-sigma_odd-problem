#!/bin/bash -l

#PBS -l nodes=8:ppn=1
#PBS -l walltime=2:00:00
#PBS -l mem=700mb
#PBS -l file=1200mb
#PBS -o check_big_gentle_mpi_8.log
#PBS -e check_big_gentle_mpi_8.err
#PBS -N check_big_gentle_mpi_8


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --first 5000000001 --last 10000000001 --bad-table ../../tables/sequential_check_bad_3_5000000001.tsv > mpi_check_gentle_5000000001_10000000001__8.tsv


echo "=== Done ==="
