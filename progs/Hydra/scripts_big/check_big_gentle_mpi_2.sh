#!/bin/bash -l

#PBS -l nodes=2:ppn=1
#PBS -l walltime=8:00:00
#PBS -l mem=300mb
#PBS -l file=300mb
#PBS -o check_big_gentle_mpi_2.log
#PBS -e check_big_gentle_mpi_2.err
#PBS -N check_big_gentle_mpi_2


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --first 5000000001 --last 10000000001 --bad-table ../../tables/sequential_check_bad_3_5000000001.tsv > mpi_check_gentle_5000000001_10000000001__2.tsv


echo "=== Done ==="
