#!/bin/bash -l

#PBS -l nodes=1:ppn=2
#PBS -l walltime=3:30:00
#PBS -l mem=70mb
#PBS -l file=60mb
#PBS -o check_big_gentle_threads_2.log
#PBS -e check_big_gentle_threads_2.err
#PBS -N check_big_gentle_threads_2


echo "=== Config module ==="
module purge
module load GCCcore


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/threads/
./check_gentle_threads --dynamic --first 5000000001 --last 10000000001 --nb-thread 2 --bad-table ../../tables/sequential_check_bad_3_5000000001.tsv > threads_check_gentle_5000000001_10000000001__2.tsv


echo "=== Done ==="
