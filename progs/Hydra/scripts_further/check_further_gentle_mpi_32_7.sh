#!/bin/bash -l

#PBS -l nodes=8:ppn=4
#PBS -l walltime=5:00:00
#PBS -l mem=4000mb
#PBS -l file=3200mb
#PBS -o check_further_gentle_mpi_32_7.log
#PBS -e check_further_gentle_mpi_32_7.err
#PBS -N check_further_gentle_mpi_32_7


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --first 69000000001 --last 70000000001 --bad-table ../../tables/bads_known.txt > mpi_further_69000000001_70000000001_32.tsv


echo "=== Done ==="
