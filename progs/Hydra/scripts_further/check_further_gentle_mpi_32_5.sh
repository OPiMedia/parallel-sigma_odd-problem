#!/bin/bash -l

#PBS -l nodes=8:ppn=4
#PBS -l walltime=5:00:00
#PBS -l mem=4000mb
#PBS -l file=3200mb
#PBS -o check_further_gentle_mpi_32_5.log
#PBS -e check_further_gentle_mpi_32_5.err
#PBS -N check_further_gentle_mpi_32_5


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Run job on $HOSTNAME - " `date` " ==="
cd $HOME/sigmaodd/progs/src/mpi/
mpirun ./check_gentle_mpi --dynamic --first 67000000001 --last 68000000001 --bad-table ../../tables/bads_known.txt > mpi_further_67000000001_68000000001_32.tsv


echo "=== Done ==="
