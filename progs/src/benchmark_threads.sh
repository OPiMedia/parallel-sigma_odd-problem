#!/bin/sh

LAST=$1


cd threads

echo === threads/one by one ===
for i in 1 2 3 4 5 6 7 8
do
    ./check_gentle_threads --last $LAST --no-print --one-by-one --nb-thread $i
    echo ------
done

echo === threads/by range ===
for i in 1 2 3 4 5 6 7 8
do
    ./check_gentle_threads --last $LAST --no-print --by-range --nb-thread $i
    echo ------
done

echo === threads/dynamic ===
for i in 1 2 3 4 5 6 7 8
do
    ./check_gentle_threads --last $LAST --no-print --dynamic --nb-thread $i
    echo ------
done

cd ..
