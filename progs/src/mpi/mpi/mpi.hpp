/* -*- coding: latin-1 -*- */
/** \file mpi/mpi/mpi.hpp (January 17, 2018)
 * \brief
 * Implementation of the message-passing (MPI) algorithms presented in the report.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_MPI_MPI_MPI_HPP_
#define PROGS_SRC_MPI_MPI_MPI_HPP_

// \cond
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
#pragma GCC diagnostic ignored "-Weffc++"
#include <mpi.h>
#pragma GCC diagnostic pop

#include <set>
#include <string>
#include <utility>
// \endcond

#include "../../common/sigmaodd/helper.hpp"
#include "../../common/sigmaodd/primes.hpp"


namespace mpi {

  using nat_type = sigmaodd::nat_type;
  using prime_type = sigmaodd::prime_type;



  /* *******
   * Types *
   *********/

  /** \brief
   * Type of MPI rank.
   */
  typedef unsigned int rank_type;


  /** \brief
   * Structure for first_n, last_n, bad_first_n and bad_last_n values.
   */
  typedef
  struct first_last_bad_bad_struct {
    nat_type first_n;
    nat_type last_n;
    nat_type bad_first_n;
    nat_type bad_last_n;
  } first_last_bad_bad_type;


  /** \brief
   * Structure for n, first_n and last_n values.
   */
  typedef
  struct n_bad_bad_struct {
    nat_type n;
    nat_type bad_first_n;
    nat_type bad_last_n;
  } n_bad_bad_type;



  /* ************
   * Prototypes *
   **************/

  /** \brief
   * Return true iff the process send an empty finished message.
   */
  inline
  bool
  is_finished_from(rank_type from_rank);


  /** \brief
   * Return true iff rank correspond to the master instance.
   */
  constexpr
  bool
  is_master(rank_type rank);


  /** \brief
   * Check in the order all odd gentle numbers between first_n and last_n,
   * and if print_bad
   *     then print all bad numbers between first_n and last_n (included).
   * The consequence of the result is that:
   * if (all numbers < first_n respect the conjecture)
   *     and (all perfect squares < last_n respect the conjecture)
   *     and (all bad numbers < last_n respect the conjecture)
   * then all numbers < last_n respect the conjecture.
   *
   * One master process dispatch one range of numbers for each of the (nb_process - 1) slaves process
   * and compute itself one range.
   * There is no barrier, the master process dispatch new range for free slave process
   * and compute itself one new range.
   * And so forth with next numbers.
   *
   * Printing is do in increasing order.
   *
   * sigmaodd::primes.cpp must be compiled without the macro PRIME16.
   *
   * @param first_n 3 <= odd <= last_n
   * @param last_n <= MAX_POSSIBLE_N
   * @param previous_bad_table if not empty then must be contains all bad numbers < first_n
   * @param print_bad
   * @param range_size even
   * @param master_range_size even
   *
   * @return the set of all bad numbers between first_n and last_n (included)
   */
  std::set<nat_type>
  mpi_check_gentle_varsigma_odd__dynamic
  (nat_type first_n, nat_type last_n,
   const std::set<nat_type> &previous_bad_table = std::set<nat_type>(),
   bool print_bad = true,
   unsigned int range_size = 20000,
   unsigned int master_range_size = 200);


  /**
   * Check in the order all odd gentle numbers between first_n and last_n,
   * and if print_bad
   *     then print all bad numbers between first_n and last_n (included).
   * The consequence of the result is that:
   * if (all numbers < first_n respect the conjecture)
   *     and (all perfect squares < last_n respect the conjecture)
   *     and (all bad numbers < last_n respect the conjecture)
   * then all numbers < last_n respect the conjecture.
   *
   * One master process dispatch one number for each of the (nb_process - 1) slaves process
   * and compute itself one number.
   * Then wait that they all finish, and so forth with next numbers.
   *
   * Printing is do in not deterministic order.
   *
   * sigmaodd::primes.cpp must be compiled without the macro PRIME16.
   *
   * @param first_n 3 <= odd <= last_n
   * @param last_n <= MAX_POSSIBLE_N
   * @param previous_bad_table if not empty then must be contains all bad numbers < first_n
   * @param print_bad
   *
   * @return the set of all bad numbers between first_n and last_n (included)
   */
  std::set<nat_type>
  mpi_check_gentle_varsigma_odd__one_by_one
  (nat_type first_n, nat_type last_n,
   const std::set<nat_type> &previous_bad_table = std::set<nat_type>(),
   bool print_bad = true);


  /** \brief
   * Return the number of process.
   */
  inline
  unsigned int
  mpi_nb_process();


  /** \brief
   * Return the processor name.
   */
  inline
  std::string
  mpi_processor_name();


  /** \brief
   * Return the rank of this process.
   */
  inline
  unsigned int
  mpi_rank();


  /** \brief
   * Computation of one n by a slave,
   * for the mpi_check_gentle_varsigma_odd__one_by_one() master function.
   */
  bool
  mpi_slave_wait_compute_n();


  /** \brief
   * Computation of one range by a slave,
   * for the mpi_check_gentle_varsigma_odd__dynamic() master function.
   */
  bool
  mpi_slave_wait_compute_range();

  /** \brief
   * Print the Open MPI version and the library version.
   */
  void
  print_mpi_versions();


  /** \brief
   * Send bad values to a process.
   */
  void
  send_bad_table_to(const std::set<nat_type> &bad_table, rank_type to_rank);


  /** \brief
   * Send a boolean value to the master.
   */
  inline
  void
  send_bool_to_master(bool b);


  /** \brief
   * Send a finished empty message to the master.
   */
  inline
  void
  send_finished_to_master();


  /** \brief
   * Send first_n, last_n, bad_first and bad_last_n to a process.
   */
  inline
  void
  send_first_last_bad_bad_to(nat_type first_n, nat_type last_n,
                             nat_type bad_first_n, nat_type bad_last_n,
                             rank_type to_rank);


  /** \brief
   * Send n, first_n and last_n to a process.
   */
  inline
  void
  send_n_bad_bad_to(nat_type n, nat_type first_n, nat_type last_n,
                    rank_type to_rank);


  /** \brief
   * Send a value to a process.
   */
  inline
  void
  send_n_to(nat_type n, rank_type to_rank);


  /** \brief
   * Send a range (first_n, last_n) to a process.
   */
  inline
  void
  send_range_to(nat_type first_n, nat_type last_n, rank_type to_rank);


  /** \brief
   * Send a size to a process.
   */
  inline
  void
  send_size_to(unsigned int size, rank_type to_rank);


  /** \brief
   * Wait and receive bad values from a process.
   */
  std::set<nat_type>
  wait_bad_table_from(rank_type from_rank);


  /** \brief
   * Wait and receive a boolean value from a process.
   */
  inline
  bool
  wait_bool_from(rank_type from_rank);


  /** \brief
   * Wait and received a finished empty message from a process.
   */
  inline
  void
  wait_finished_from(rank_type from_rank);


  /** \brief
   * Wait and receive values first_n, last_n and bad_last_n from the master.
   */
  inline
  first_last_bad_bad_type
  wait_first_last_bad_bad_from_master();


  /** \brief
   * Wait and receive values n, first_n and last_n from the master.
   */
  inline
  n_bad_bad_type
  wait_n_bad_bad_from_master();


  /** \brief
   * Wait and receive a value  from the master.
   */
  inline
  nat_type
  wait_n_from_master();


  /** \brief
   * Wait and receive a range (first_n, last_n) from the master.
   */
  inline
  std::pair<nat_type, nat_type>
  wait_range_from_master();


  /** \brief
   * Wait and receive a size from a process.
   */
  inline
  unsigned int
  wait_size_from(rank_type from_rank);

}  // namespace mpi

#include "mpi__inline.hpp"

#endif  // PROGS_SRC_MPI_MPI_MPI_HPP_
