/* -*- coding: latin-1 -*- */
/** \file mpi/mpi/mpi__inline.hpp (January 17, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_MPI_MPI_MPI__INLINE_HPP_
#define PROGS_SRC_MPI_MPI_MPI__INLINE_HPP_

// \cond
#include <cassert>

#include <string>
#include <utility>
// \endcond


namespace mpi {

  /* ********************
   * constexpr function *
   **********************/

  constexpr
  bool
  is_master(rank_type rank) {
    return rank == 0;
  }



  /* ******************
   * inline functions *
   ********************/
  inline
  bool
  is_finished_from(rank_type from_rank) {
    int is_finished;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Iprobe(from_rank, 0, MPI_COMM_WORLD, &is_finished, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop

    return is_finished;
  }


  inline
  unsigned int
  mpi_nb_process() {
    unsigned int nb_process;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Comm_size(MPI_COMM_WORLD, reinterpret_cast<int*>(&nb_process));
#pragma GCC diagnostic pop

    return nb_process;
  }


  inline
  std::string
  mpi_processor_name() {
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    unsigned int len;

    MPI_Get_processor_name(processor_name, reinterpret_cast<int*>(&len));

    return std::string(processor_name);
  }


  inline
  unsigned int
  mpi_rank() {
    unsigned int rank;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Comm_rank(MPI_COMM_WORLD, reinterpret_cast<int*>(&rank));
#pragma GCC diagnostic pop

    return rank;
  }


  inline
  void
  send_bool_to_master(bool b) {
    const uint32_t n = b;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Send(&n, 1, MPI_UINT32_T, 0, 0, MPI_COMM_WORLD);
#pragma GCC diagnostic pop
  }


  inline
  void
  send_finished_to_master() {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Send(nullptr, 0, MPI_UINT32_T, 0, 0, MPI_COMM_WORLD);
#pragma GCC diagnostic pop
  }


  inline
  void
  send_first_last_bad_bad_to(nat_type first_n, nat_type last_n,
                             nat_type bad_first_n, nat_type bad_last_n,
                             rank_type to_rank) {
    assert(sizeof(nat_type) == 8);

    const uint64_t data[4] = {first_n, last_n, bad_first_n, bad_last_n};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Send(data, 4, MPI_UINT64_T, to_rank, 0, MPI_COMM_WORLD);
#pragma GCC diagnostic pop
  }


  inline
  void
  send_n_bad_bad_to(nat_type n, nat_type first_n, nat_type last_n,
                    rank_type to_rank) {
    assert(sizeof(nat_type) == 8);

    const uint64_t data[3] = {n, first_n, last_n};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Send(data, 3, MPI_UINT64_T, to_rank, 0, MPI_COMM_WORLD);
#pragma GCC diagnostic pop
  }


  inline
  void
  send_n_to(nat_type n, rank_type to_rank) {
    assert(sizeof(nat_type) == 8);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Send(&n, 1, MPI_UINT64_T, to_rank, 0, MPI_COMM_WORLD);
#pragma GCC diagnostic pop
  }


  inline
  void
  send_range_to(nat_type first_n, nat_type last_n, rank_type to_rank) {
    assert(sizeof(nat_type) == 8);

    const nat_type data[2] = {first_n, last_n};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Send(data, 2, MPI_UINT64_T, to_rank, 0, MPI_COMM_WORLD);
#pragma GCC diagnostic pop
  }


  inline
  void
  send_size_to(unsigned int size, rank_type to_rank) {
    uint32_t n = size;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Send(&n, 1, MPI_UINT32_T, to_rank, 0, MPI_COMM_WORLD);
#pragma GCC diagnostic pop
  }


  inline
  bool
  wait_bool_from(rank_type from_rank) {
    uint32_t n;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Recv(&n, 1, MPI_UINT32_T, from_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop

    return n;
  }


  inline
  void
  wait_finished_from(rank_type from_rank) {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Recv(nullptr, 0, MPI_UINT32_T, from_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop
  }


  inline
  first_last_bad_bad_type
  wait_first_last_bad_bad_from_master() {
    assert(sizeof(nat_type) == 8);

    uint64_t data[4];

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Recv(data, 4, MPI_UINT64_T, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop

    return {data[0], data[1], data[2], data[3]};
  }


  inline
  n_bad_bad_type
  wait_n_bad_bad_from_master() {
    assert(sizeof(nat_type) == 8);

    uint64_t data[3];

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Recv(data, 3, MPI_UINT64_T, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop

    return {data[0], data[1], data[2]};
  }


  inline
  nat_type
  wait_n_from_master() {
    assert(sizeof(nat_type) == 8);

    uint64_t n;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Recv(&n, 1, MPI_UINT64_T, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop

    return n;
  }


  inline
  std::pair<nat_type, nat_type>
  wait_range_from_master() {
    assert(sizeof(nat_type) == 8);

    nat_type data[2];

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Recv(data, 2, MPI_UINT64_T, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop

    return std::make_pair(data[0], data[1]);
  }


  inline
  unsigned int
  wait_size_from(rank_type from_rank) {
    uint32_t n;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wold-style-cast"
    MPI_Recv(&n, 1, MPI_UINT32_T, from_rank, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
#pragma GCC diagnostic pop

    return n;
  }
}  // namespace mpi

#endif  // PROGS_SRC_MPI_MPI_MPI__INLINE_HPP_
