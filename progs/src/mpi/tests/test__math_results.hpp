/* -*- coding: latin-1 -*- */

/** \file mpi/tests/test__math_results.hpp (August 30, 2020)
 *
 * GPLv3 --- Copyright (C) 2017, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>
#include <sstream>
#include <vector>

#include "../../common/tests/test_helper.hpp"

#include "../mpi/mpi.hpp"


using namespace mpi;


class Test__sigmaodd__sigmaodd : public CxxTest::TestSuite {
public:
  void test__bad_3_1000000001() {
    SHOW_FUNC_NAME;

    const std::vector<sigmaodd::nat_type> bads_list
      = sigmaodd::load_numbers("../../tables/tsv/mpi_check_gentle_3_1000000001.tsv");
    const std::set<nat_type> bads(bads_list.cbegin(), bads_list.cend());

    TS_ASSERT_EQUALS(bads_list.size(), 7070);
    TS_ASSERT_EQUALS(bads.size(), 7070);

    const std::vector<sigmaodd::nat_type> process_bads_list
      = sigmaodd::load_numbers("../../tables/tsv/sequential_check_gentle_3_1000000001.tsv");
    const std::set<nat_type>
      process_bads(process_bads_list.cbegin(), process_bads_list.cend());

    TS_ASSERT_EQUALS(bads, process_bads);
  }


  void test__bad_3_5000000001() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__bad_5000000001_10000000001() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }

};
