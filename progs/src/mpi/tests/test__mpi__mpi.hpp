/* -*- coding: latin-1 -*- */

/** \file mpi/tests/test__mpi__mpi.hpp (January 17, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>
#include <sstream>
#include <vector>

#include "../../common/tests/test_helper.hpp"

#include "../mpi/mpi.hpp"

#include "../../sequential/sequential/sequential.hpp"


using namespace mpi;



// All bad numbers <= 4000000
const std::vector<nat_type>
bad_table_to_4000000_list{2205, 19845, 108045, 143325, 178605, 187425,
    236925, 266805, 319725, 353925, 372645, 407925, 452025, 462825, 584325,
    637245, 646425, 658125, 672525, 789525, 796005, 804825, 845325,
    920205, 972405, 981225, 1007325, 1055925, 1069425,
    1102725, 1113525, 1116225, 1166445, 1201725, 1245825, 1289925, 1378125, 1380825,
    1442925, 1510425, 1547325, 1573605, 1607445, 1642725, 1660725, 1686825, 1730925,
    1854405, 1907325, 1965645, 1995525, 2119005, 2127825, 2132325, 2171925, 2401245,
    2524725, 2568825, 2657025, 2833425, 2877525, 2965725, 3018645, 3053925, 3098025,
    3185325, 3230325, 3353805, 3450825, 3494925, 3583125, 3671325, 3706605, 3715425,
    3847725, 3891825};

const std::set<nat_type> bad_table_to_4000000(bad_table_to_4000000_list.cbegin(),
                                              bad_table_to_4000000_list.cend());



class Test__sigmaodd__sigmaodd : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    helper::print_intern_config_compiler();
    print_mpi_versions();

    TS_ASSERT(sigmaodd::read_primes_table());

    TS_ASSERT_DIFFERS(sigmaodd::array_odd_primes_,nullptr);

    TS_ASSERT_EQUALS(bad_table_to_4000000_list.size(), 76);
    TS_ASSERT_EQUALS(bad_table_to_4000000.size(), 76);

    for (nat_type n : bad_table_to_4000000_list) {
      TS_ASSERT(!sigmaodd::is_square(n));
      TS_ASSERT_LESS_THAN(n, sequential::sequential_varsigma_odd(n));

      // Empirical observations
      TS_ASSERT(sigmaodd::is_divide(9, n));  // seems always true
      TS_ASSERT(sigmaodd::is_divide(5, n));  // only always true for bad numbers of this little table
    }

    MPI_Init(nullptr, nullptr);

    std::cout << "Process id: " << mpi::mpi_rank()
              << "\t# process: " << mpi::mpi_nb_process()
              << "\tProcess name: " << mpi::mpi_processor_name() << std::endl;
  }


  void test__MOST_OF_FUNCTIONS() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__mpi_check_gentle_varsigma_odd__one_by_one() {
    SHOW_FUNC_NAME;

    mpi_check_gentle_varsigma_odd__one_by_one(3, bad_table_to_4000000_list[0] - 1);
    mpi_check_gentle_varsigma_odd__one_by_one(3, bad_table_to_4000000_list[3] - 1);

    const std::set<nat_type> bad_table1
      = mpi_check_gentle_varsigma_odd__one_by_one(3, 4000000,
                                                  std::set<nat_type>(),
                                                  false);
    const std::set<nat_type> bad_table2
      = mpi_check_gentle_varsigma_odd__one_by_one(bad_table_to_4000000_list[62] - 2, 4000000,
                                                  std::set<nat_type>(),
                                                  false);
    const std::set<nat_type> bad_table3
      = mpi_check_gentle_varsigma_odd__one_by_one(bad_table_to_4000000_list[62], 4000000,
                                                  std::set<nat_type>(),
                                                  false);

    TS_ASSERT_EQUALS(bad_table1, bad_table_to_4000000);
    TS_ASSERT_EQUALS(bad_table2, bad_table3);
    TS_ASSERT_EQUALS(bad_table_to_4000000_list[62], 3018645);

    TS_ASSERT_LESS_THAN(bad_table2.size(), bad_table1.size());
    for (nat_type n : bad_table1) {
      if (n < bad_table_to_4000000_list[62]) {
        TS_ASSERT_EQUALS(bad_table2.find(n), bad_table2.cend());
      }
      else {
        TS_ASSERT_DIFFERS(bad_table2.find(n), bad_table2.cend());
      }
    }

    // Same that sequential implementation
    TS_ASSERT_EQUALS
      (mpi_check_gentle_varsigma_odd__one_by_one(bad_table_to_4000000_list[62], 4000000,
                                                 std::set<nat_type>(),
                                                 false),
       sequential::sequential_check_gentle_varsigma_odd(bad_table_to_4000000_list[62], 4000000,
                                                        false));
  }


  void test__sequential_print_in_order() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test_close() {
    SHOW_FUNC_NAME;

    MPI_Finalize();
  }
};
