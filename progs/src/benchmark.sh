#!/bin/sh

# ~20 minutes

LAST=20000001
BEEP='\a'

./benchmark_sequential.sh $LAST | tee benchmark.log
echo $BEEP
./benchmark_threads.sh $LAST | tee --append benchmark.log
echo $BEEP
./benchmark_mpi.sh $LAST | tee --append benchmark.log
echo $BEEP
./benchmark_opencl.sh $LAST | tee --append benchmark.log
echo $BEEP
