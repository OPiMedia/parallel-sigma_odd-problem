/* -*- coding: latin-1 -*- */
/** \file threads/threads/threads.hpp (January 6, 2018)
 * \brief
 * Implementation of the threads parallel algorithms presented in the report.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_THREADS_THREADS_THREADS_HPP_
#define PROGS_SRC_THREADS_THREADS_THREADS_HPP_

// \cond
#include <set>
#include <thread>
#include <vector>
// \endcond

#include "../../common/sigmaodd/helper.hpp"
#include "../../common/sigmaodd/primes.hpp"


namespace threads {

  using nat_type = sigmaodd::nat_type;
  using prime_type = sigmaodd::prime_type;



  /* ************
   * Prototypes *
   **************/

  /**
   * Check in the order all odd gentle numbers between first_n and last_n,
   * and if print_bad
   *     then print all bad numbers between first_n and last_n (included).
   * The consequence of the result is that:
   * if (all numbers < first_n respect the conjecture)
   *     and (all perfect squares < last_n respect the conjecture)
   *     and (all bad numbers < last_n respect the conjecture)
   * then all numbers < last_n respect the conjecture.
   *
   * One master thread dispatch one range of numbers for each of the (nb_thread - 1) slaves threads
   * and compute itself one range.
   * Then wait that they all finish, and so forth with next numbers.
   *
   * Printing is do in increasing order.
   *
   * sigmaodd::primes.cpp must be compiled without the macro PRIME16.
   *
   * @param nb_thread >= 1
   * @param first_n 3 <= odd <= last_n
   * @param last_n <= MAX_POSSIBLE_N
   * @param previous_bad_table if not empty then must be contains all bad numbers < first_n
   * @param print_bad
   * @param range_size even
   *
   * @return the set of all bad numbers between first_n and last_n (included)
   */
  std::set<nat_type>
  threads_check_gentle_varsigma_odd__by_range
  (unsigned int nb_thread,
   nat_type first_n, nat_type last_n,
   const std::set<nat_type> &previous_bad_table = std::set<nat_type>(),
   bool print_bad = true,
   unsigned int range_size = 2000);


  /** \brief
   * Check in the order all odd gentle numbers between first_n and last_n,
   * and if print_bad
   *     then print all bad numbers between first_n and last_n (included).
   * The consequence of the result is that:
   * if (all numbers < first_n respect the conjecture)
   *     and (all perfect squares < last_n respect the conjecture)
   *     and (all bad numbers < last_n respect the conjecture)
   * then all numbers < last_n respect the conjecture.
   *
   * One master thread dispatch one range of numbers for each of the (nb_thread - 1) slaves threads
   * and compute itself one range.
   * There is no barrier, the master thread dispatch new range for free slave threads
   * and compute itself one new range.
   * And so forth with next numbers.
   *
   * Printing is do in increasing order.
   *
   * sigmaodd::primes.cpp must be compiled without the macro PRIME16.
   *
   * @param nb_thread >= 1
   * @param first_n 3 <= odd <= last_n
   * @param last_n <= MAX_POSSIBLE_N
   * @param previous_bad_table if not empty then must be contains all bad numbers < first_n
   * @param print_bad
   * @param range_size even
   * @param master_range_size even
   *
   * @return the set of all bad numbers between first_n and last_n (included)
   */
  std::set<nat_type>
  threads_check_gentle_varsigma_odd__dynamic
  (unsigned int nb_thread,
   nat_type first_n, nat_type last_n,
   const std::set<nat_type> &previous_bad_table = std::set<nat_type>(),
   bool print_bad = true,
   unsigned int range_size = 20000,
   unsigned int master_range_size = 200);


  /**
   * Check in the order all odd gentle numbers between first_n and last_n,
   * and if print_bad
   *     then print all bad numbers between first_n and last_n (included).
   * The consequence of the result is that:
   * if (all numbers < first_n respect the conjecture)
   *     and (all perfect squares < last_n respect the conjecture)
   *     and (all bad numbers < last_n respect the conjecture)
   * then all numbers < last_n respect the conjecture.
   *
   * One master thread dispatch one number for each of the (nb_thread - 1) slaves threads
   * and compute itself one number.
   * Then wait that they all finish, and so forth with next numbers.
   *
   * Printing is do in not deterministic order.
   *
   * sigmaodd::primes.cpp must be compiled without the macro PRIME16.
   *
   * @param nb_thread >= 1
   * @param first_n 3 <= odd <= last_n
   * @param last_n <= MAX_POSSIBLE_N
   * @param previous_bad_table if not empty then must be contains all bad numbers < first_n
   * @param print_bad
   *
   * @return the set of all bad numbers between first_n and last_n (included)
   */
  std::set<nat_type>
  threads_check_gentle_varsigma_odd__one_by_one
  (unsigned int nb_thread,
   nat_type first_n, nat_type last_n,
   const std::set<nat_type> &previous_bad_table = std::set<nat_type>(),
   bool print_bad = true);

}  // namespace threads

#endif  // PROGS_SRC_THREADS_THREADS_THREADS_HPP_
