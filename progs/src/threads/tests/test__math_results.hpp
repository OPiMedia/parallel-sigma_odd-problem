/* -*- coding: latin-1 -*- */

/** \file threads/tests/test__math_results.hpp (August 30, 2020)
 *
 * GPLv3 --- Copyright (C) 2017, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>
#include <sstream>
#include <vector>

#include "../../common/tests/test_helper.hpp"

#include "../threads/threads.hpp"


using namespace threads;


class Test__sigmaodd__sigmaodd : public CxxTest::TestSuite {
public:
  void test__bad_3_1000000001() {
    SHOW_FUNC_NAME;

    const std::vector<sigmaodd::nat_type> bads_list
      = sigmaodd::load_numbers("../../tables/tsv/threads_check_gentle_3_1000000001.tsv");
    const std::set<nat_type> bads(bads_list.cbegin(), bads_list.cend());

    TS_ASSERT_EQUALS(bads_list.size(), 7070);
    TS_ASSERT_EQUALS(bads.size(), 7070);

    const std::vector<sigmaodd::nat_type> sequential_bads_list
      = sigmaodd::load_numbers("../../tables/tsv/sequential_check_gentle_3_1000000001.tsv");
    const std::set<nat_type>
      sequential_bads(sequential_bads_list.cbegin(), sequential_bads_list.cend());

    TS_ASSERT_EQUALS(bads, sequential_bads);
  }


  void test__bad_3_5000000001() {
    SHOW_FUNC_NAME;

    const std::vector<sigmaodd::nat_type> bads_list
      = sigmaodd::load_numbers("../../tables/tsv/threads_check_gentle_3_5000000001.tsv");
    const std::set<nat_type> bads(bads_list.cbegin(), bads_list.cend());

    TS_ASSERT_EQUALS(bads_list.size(), 29517);
    TS_ASSERT_EQUALS(bads.size(), 29517);

    const std::vector<sigmaodd::nat_type> sequential_bads_list
      = sigmaodd::load_numbers("../../tables/tsv/sequential_check_gentle_3_5000000001.tsv");
    const std::set<nat_type>
      sequential_bads(sequential_bads_list.cbegin(), sequential_bads_list.cend());

    TS_ASSERT_EQUALS(bads, sequential_bads);
  }


  void test__bad_5000000001_10000000001() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }

};
