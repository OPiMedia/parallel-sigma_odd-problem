#!/bin/sh

LAST=$1

cd opencl

echo === OpenCL/\"start\" ===
./start_opencl

echo === OpenCL/decimal ===
for i in 100 500 1000 2000 3000 4000 5000 6000 7000 8000 9000 10000 11000 12000 13000 14000 15000 16000 17000 18000 19000 20000
do
    ./check_gentle_opencl --last $LAST --no-print --nb-opencl $i
    echo ------
done

echo === OpenCL/powers of 2 ===
for i in 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144
do
    ./check_gentle_opencl --last $LAST --no-print --nb-opencl $i
    echo ------
done

cd ..
