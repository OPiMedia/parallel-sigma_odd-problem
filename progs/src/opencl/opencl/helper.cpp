/* -*- coding: latin-1 -*- */
/** \file opencl/opencl/helper.cpp (January 8, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cassert>

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
// \endcond

#include "../../common/helper/helper.hpp"

#include "helper.hpp"


namespace opencl {

  /* **********
   * Constant *
   ************/

  const std::map<int32_t, std::string> errors_map {
    // Extracted from /usr/include/CL/cl.h
#include "errors_map.txt"
  };



  /* ***********
   * Functions *
   *************/

  // Define function to get device information for each type.
  // https://www.khronos.org/registry/OpenCL/sdk/1.1/docs/man/xhtml/clGetDeviceInfo.html
#ifdef __CL_ENABLE_EXCEPTIONS
#define GET_DEVICE_INFO_NAME_TYPE(NAME, TYPE)                           \
  TYPE                                                                  \
  get_device_info_##NAME(const cl::Device &device, cl_device_info info) { \
    TYPE value;                                                         \
                                                                        \
    try {                                                               \
      const cl_int error = device.getInfo(info, &value);                \
                                                                        \
      if (error != CL_SUCCESS) {                                        \
        std::cerr << "! Device::getInfo(" << info << ") error in get "  \
                  << #TYPE << ": \""                                    \
                  << error_name(error) << '"' <<std::endl;              \
      }                                                                 \
    }                                                                   \
    catch (const cl::Error &e) {                                        \
      std::cerr << "! Device::getInfo(" << info << ") exception in get " \
                << #TYPE << ": \""                                      \
                << e.what() << '"' << std::endl;                        \
    }                                                                   \
                                                                        \
    return value;                                                       \
  }
#else
#define GET_DEVICE_INFO_NAME_TYPE(NAME, TYPE)                           \
  TYPE                                                                  \
  get_device_info_##NAME(const cl::Device &device, cl_device_info info) { \
    TYPE value;                                                         \
                                                                        \
    const cl_int error = device.getInfo(info, &value);                  \
                                                                        \
    if (error != CL_SUCCESS) {                                          \
      std::cerr << "! Device::getInfo(" << info << ") error in get "    \
                << #TYPE << ": \""                                      \
                << error_name(error) << '"' <<std::endl;                \
    }                                                                   \
                                                                        \
    return value;                                                       \
  }
#endif

#define GET_DEVICE_INFO_TYPE(TYPE) GET_DEVICE_INFO_NAME_TYPE(TYPE, TYPE)

  GET_DEVICE_INFO_TYPE(cl_bool)

  GET_DEVICE_INFO_TYPE(cl_device_fp_config)

  GET_DEVICE_INFO_TYPE(cl_device_local_mem_type)

  GET_DEVICE_INFO_TYPE(cl_device_mem_cache_type)

  GET_DEVICE_INFO_TYPE(cl_device_type)

  GET_DEVICE_INFO_TYPE(cl_uint)

  GET_DEVICE_INFO_TYPE(cl_ulong)

  GET_DEVICE_INFO_TYPE(size_t)

  GET_DEVICE_INFO_NAME_TYPE(vector_size_t, VECTOR_CLASS<::size_t>)

#undef GET_DEVICE_INFO_NAME_TYPE
#undef GET_DEVICE_INFO_TYPE


  std::string
  get_device_info_string(const cl::Device &device, cl_device_info info) {
    std::string value;

#ifdef __CL_ENABLE_EXCEPTIONS
    try {
#endif
      const cl_int error = device.getInfo(info, &value);

      if (error != CL_SUCCESS) {
        std::cerr << "! Device::getInfo(" << info << ") error in get string: \""
                  << error_name(error) << '"' << std::endl;
      }
#ifdef __CL_ENABLE_EXCEPTIONS
    }
    catch (const cl::Error &e) {
      std::cerr << "! Device::getInfo(" << info << ") exception to get string: \""
                << e.what() << '"' << std::endl;
    }
#endif

    return helper::remove_last_if_null(value);
  }


  cl::Device
  get_first_device_gpu() {
    std::vector<cl::Platform> platforms;

    cl::Platform::get(&platforms);
    for (cl::Platform platform : platforms) {
      std::vector<cl::Device> devices;

      platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

      if (!devices.empty()) {
        return devices[0];
      }
    }

    std::cerr << "! No GPU found!" << std::endl;

    exit(EXIT_FAILURE);
  }


  std::string
  error_name(cl_int code) {
      const auto it = errors_map.find(code);

      return (it == errors_map.cend()
              ? "unknow"
              : it->second);
  }


  void
  print_device(const cl::Device &device) {
    std::cout << "  Driver version: \"";
    std::cout << get_device_info_string(device, CL_DRIVER_VERSION) << '"' << std::endl;

    std::cout << "  Device name: \"";
    std::cout << get_device_info_string(device, CL_DEVICE_NAME) << '"' << std::endl;

    std::cout << "         address_bits: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_ADDRESS_BITS) << std::endl;

    std::cout << "         available?: ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_AVAILABLE)) << std::endl;

    std::cout << "         built in kernels: \"";
    std::cout << get_device_info_string(device, CL_DEVICE_BUILT_IN_KERNELS) << '"' << std::endl;

    std::cout << "         compiler available?: ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_COMPILER_AVAILABLE)) << std::endl;

    std::cout << "         double fp config: ";
    std::cout << helper::to_string(get_device_info_cl_device_fp_config(device, CL_DEVICE_DOUBLE_FP_CONFIG)) << std::endl;

    std::cout << "         endian little? ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_ENDIAN_LITTLE)) << std::endl;

    std::cout << "         extensions: \"";
    std::cout << get_device_info_string(device, CL_DEVICE_EXTENSIONS) << '"' << std::endl;

    std::cout << "         global mem cache size: ";
    std::cout << get_device_info_cl_ulong(device, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE) << std::endl;

    std::cout << "         global mem cache type: ";
    std::cout << get_device_info_cl_device_mem_cache_type(device, CL_DEVICE_GLOBAL_MEM_CACHE_TYPE) << std::endl;

    std::cout << "         global mem cacheline size: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE) << std::endl;

    std::cout << "         global mem size: ";
    std::cout << get_device_info_cl_ulong(device, CL_DEVICE_GLOBAL_MEM_SIZE) << std::endl;

    std::cout << "         image max array size: ";
    std::cout << get_device_info_size_t(device, CL_DEVICE_IMAGE_MAX_ARRAY_SIZE) << std::endl;

    std::cout << "         image max buffer size: ";
    std::cout << get_device_info_size_t(device, CL_DEVICE_IMAGE_MAX_BUFFER_SIZE) << std::endl;

    std::cout << "         image support? ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_IMAGE_SUPPORT)) << std::endl;

    std::cout << "         linker available?: ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_LINKER_AVAILABLE)) << std::endl;

    std::cout << "         local mem size: ";
    std::cout << get_device_info_cl_ulong(device, CL_DEVICE_LOCAL_MEM_SIZE) << std::endl;

    std::cout << "         local mem type: ";
    std::cout << get_device_info_cl_device_local_mem_type(device, CL_DEVICE_LOCAL_MEM_TYPE) << std::endl;

    std::cout << "         max clock frequency: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_MAX_CLOCK_FREQUENCY) << std::endl;

    std::cout << "         max compute units: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_MAX_COMPUTE_UNITS) << std::endl;

    std::cout << "         max constant buffer size: ";
    std::cout << get_device_info_cl_ulong(device, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE) << std::endl;

    std::cout << "         max mem alloc size: ";
    std::cout << get_device_info_cl_ulong(device, CL_DEVICE_MAX_MEM_ALLOC_SIZE) << std::endl;

    std::cout << "         max work group size: ";
    std::cout << get_device_info_size_t(device, CL_DEVICE_MAX_WORK_GROUP_SIZE) << std::endl;

    std::cout << "         max work item sizes:";
    for (size_t item_size : get_device_info_vector_size_t(device, CL_DEVICE_MAX_WORK_ITEM_SIZES)) {
      std::cout << ' ' << item_size;
    }
    std::cout << std::endl;

    std::cout << "         mem base addr align: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_MEM_BASE_ADDR_ALIGN) << std::endl;

    std::cout << "         min_data_type_align_size: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE) << std::endl;

    std::cout << "         OpenCL C version: \"";
    std::cout << get_device_info_string(device, CL_DEVICE_OPENCL_C_VERSION) << '"' << std::endl;

    std::cout << "         Profile: \"";
    std::cout << get_device_info_string(device, CL_DEVICE_PROFILE) << '"' << std::endl;

    std::cout << "         SPIR versions: \"";
    std::cout << get_device_info_string(device, CL_DEVICE_SPIR_VERSIONS) << '"' << std::endl;

    std::cout << "         type: ";
    std::cout << get_device_info_cl_device_type(device, CL_DEVICE_TYPE) << std::endl;

    std::cout << "         version: \"";
    std::cout << get_device_info_string(device, CL_DEVICE_VERSION) << '"' << std::endl;

#ifdef CL_DEVICE_COMPUTE_CAPABILITY_MAJOR_NV
    // https://www.khronos.org/registry/OpenCL/extensions/nv/cl_nv_device_attribute_query.txt
    std::cout << "         compute capability major NV: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_COMPUTE_CAPABILITY_MAJOR_NV) << std::endl;

    std::cout << "         compute capability minor NV: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_COMPUTE_CAPABILITY_MINOR_NV) << std::endl;

    std::cout << "         registers per block NV: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_REGISTERS_PER_BLOCK_NV) << std::endl;

    std::cout << "         warp size NV: ";
    std::cout << get_device_info_cl_uint(device, CL_DEVICE_WARP_SIZE_NV) << std::endl;

    std::cout << "         GPU overlap NV: ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_GPU_OVERLAP_NV)) << std::endl;

    std::cout << "         kernel exec timeout NV: ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_KERNEL_EXEC_TIMEOUT_NV)) << std::endl;

    std::cout << "         integrated memory NV: ";
    std::cout << helper::to_string(get_device_info_cl_bool(device, CL_DEVICE_INTEGRATED_MEMORY_NV)) << std::endl;
#endif
  }


  void
  print_error(cl_int code, std::string message,
              bool only_if_error, bool exit_if_error) {
    if ((code != 0) || !only_if_error) {
      if (!message.empty()) {
        message = " " + message;
      }

      std::cerr << "! OpenCL error "
                << code << ' ' << error_name(code)
                << message
                << std::endl;
      std::cerr.flush();

      if (exit_if_error) {
        exit(EXIT_FAILURE);
      }
    }
  }


  void
  print_platform(const cl::Platform &platform) {
    std::cout << std::string(30, '=') << std::endl;

    std::string platform_name;
    std::string platform_vendor;
    std::string platform_version;
    std::string platform_profile;
    std::string platform_extensions;

    platform.getInfo(CL_PLATFORM_NAME, &platform_name);
    platform.getInfo(CL_PLATFORM_VENDOR, &platform_vendor);
    platform.getInfo(CL_PLATFORM_VERSION, &platform_version);
    platform.getInfo(CL_PLATFORM_PROFILE, &platform_profile);
    platform.getInfo(CL_PLATFORM_EXTENSIONS, &platform_extensions);

    std::cout
      << "Platform name: \"" << helper::remove_last_if_null(platform_name) <<  '"' << std::endl
      << "         vendor: \"" << helper::remove_last_if_null(platform_vendor) <<  '"' << std::endl
      << "         version: \"" << helper::remove_last_if_null(platform_version) <<  '"' << std::endl
      << "         profile: \"" << helper::remove_last_if_null(platform_profile) <<  '"' << std::endl
      << "         extensions: \"" << helper::remove_last_if_null(platform_extensions) <<  '"' << std::endl
      << std::endl;

    std::vector<cl::Device> devices;

    // https://www.khronos.org/registry/OpenCL/sdk/1.1/docs/man/xhtml/clGetDeviceIDs.html
    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
    for (cl::Device device : devices) {
      print_device(device);
    }
  }


  void
  print_platforms() {
    std::vector<cl::Platform> platforms;

    // https://www.khronos.org/registry/OpenCL/sdk/1.1/docs/man/xhtml/clGetPlatformIDs.html
    cl::Platform::get(&platforms);
    for (cl::Platform platform : platforms) {
      print_platform(platform);
    }
  }

}  // namespace opencl
