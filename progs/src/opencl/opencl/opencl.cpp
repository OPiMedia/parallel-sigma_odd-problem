/* -*- coding: latin-1 -*- */
/** \file opencl/opencl/opencl.cpp (February 6, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cassert>

#include <algorithm>
// \endcond

#include "../../common/helper/helper.hpp"
#include "../../sequential/sequential/sequential.hpp"

#include "opencl.hpp"



// #define BUILD_LOG



namespace opencl {
  /* ***********
   * Functions *
   *************/

  std::set<nat_type>
  opencl_check_gentle_varsigma_odd(nat_type first_n, nat_type last_n,
                                   unsigned int opencl_nb_number,
                                   bool print_bad, bool print_time) {
    assert(sigmaodd::is_odd(first_n));
    assert(3 <= first_n);
    assert(first_n <= last_n);
    assert(last_n <= sigmaodd::MAX_POSSIBLE_N);

#ifdef PRIME16
    assert(n < 65536);
#endif

    assert(sizeof(nat_type) == 8);
    assert(sizeof(nat_type) == sizeof(cl_ulong));

    assert(sizeof(prime_type) == 4);
    assert(sizeof(prime_type) == sizeof(cl_uint));

    cl_ulong cumulative_time = 0;

    const std::string kernel_src(helper::file_to_string("opencl_src/sigmaodd.cl"));


    // Init OpenCL
    cl_int error;
    const cl::Device device = opencl::get_first_device_gpu();
    const VECTOR_CLASS<cl::Device> devices = {device};
    const cl::Context context{device};


    // Init OpenCL buffer
    size_t primes_byte_size = sigmaodd::odd_primes_table_nb()*sizeof(prime_type);
    size_t ns_byte_size = opencl_nb_number*sizeof(nat_type);
    size_t results_byte_size = opencl_nb_number*sizeof(cl_uchar);

    cl::Buffer primes_buffer{context, CL_MEM_READ_ONLY, primes_byte_size};
    cl::Buffer ns_buffer{context, CL_MEM_READ_ONLY, ns_byte_size};
    cl::Buffer results_buffer{context, CL_MEM_WRITE_ONLY, results_byte_size};


    // Command queue
    const cl::CommandQueue queue{context, device, CL_QUEUE_PROFILING_ENABLE};

    error = queue.enqueueWriteBuffer(primes_buffer, CL_TRUE, 0, primes_byte_size,
                                     sigmaodd::odd_primes_table_ptr());
    print_error(error, "clEnqueueWriteBuffer");


    // Program
    cl::Program program{context, kernel_src};
    error = program.build(devices, "-DONLY_MAIN_KERNEL -cl-single-precision-constant -cl-denorms-are-zero -cl-mad-enable -cl-no-signed-zeros -cl-unsafe-math-optimizations -cl-finite-math-only -cl-fast-relaxed-math"
#ifdef BUILD_LOG
                          " -cl-nv-verbose"
#endif
                          );
    print_error(error, "clBuildProgram");

#ifdef BUILD_LOG
    {
      std::string log;

      error = program.getBuildInfo(device, CL_PROGRAM_BUILD_LOG, &log);
      print_error(error, "clGetProgramBuildInfo");
      std::cout << log << std::endl;
    }
#endif

    // cl::Kernel kernel{program, "check_ns"};
    cl::Kernel kernel{program, "check_ns__optimized"};
    // cl::Kernel kernel{program, "check_ns__simplified"};

#ifdef BUILD_LOG
    {
      size_t work_group_size;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_WORK_GROUP_SIZE, &work_group_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_WORK_GROUP_SIZE)");

      size_t compile_work_group_size[3];

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_COMPILE_WORK_GROUP_SIZE, &compile_work_group_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_COMPILE_WORK_GROUP_SIZE)", &compile_work_group_size);

      cl_ulong local_mem_size;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_LOCAL_MEM_SIZE, &local_mem_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_LOCAL_MEM_SIZE)");

      size_t preferred_work_group_size_multiple;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, &preferred_work_group_size_multiple);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE)");

      cl_ulong private_mem_size;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_PRIVATE_MEM_SIZE, &private_mem_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_PRIVATE_MEM_SIZE)");
    }
#endif

    error = kernel.setArg(0, primes_buffer);
    print_error(error, "clSetKernelArg(0, primes_buffer)");

    error = kernel.setArg(1, ns_buffer);
    print_error(error, "clSetKernelArg(1, ns_buffer)");

    error = kernel.setArg(3, results_buffer);
    print_error(error, "clSetKernelArg(3, results_buffer)");

    std::vector<nat_type> ns(opencl_nb_number);
    std::vector<unsigned char> results(opencl_nb_number);
    cl::Event event;

    std::set<nat_type> bad_table;
    unsigned int nb = 0;

    for (nat_type n = first_n; n <= last_n; n += 2) {
      if (!sigmaodd::is_first_mersenne_prime_unitary_divide_or_square(n)) {
        ns[nb] = n;
        ++nb;
      }

      if ((nb == opencl_nb_number) || (n == last_n)) {
        error = kernel.setArg(2, nb);
        print_error(error, "clSetKernelArg(2, nb)");

        const size_t nb_byte_size = nb*sizeof(nat_type);

        error = queue.enqueueWriteBuffer(ns_buffer, CL_TRUE, 0, nb_byte_size, ns.data());
        print_error(error, "clEnqueueWriteBuffer");

        error = queue.enqueueNDRangeKernel(kernel,
                                           cl::NullRange,
                                           cl::NDRange(opencl_nb_number),
                                           cl::NullRange,
                                           nullptr,
                                           &event);
        print_error(error, "clEnqueueNDRangeKernel");

        cl_ulong start;
        cl_ulong stop;

        error = event.wait();
        print_error(error, "clWaitForEvents");

        error = event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
        print_error(error, "getProfilingInfo");
        error = event.getProfilingInfo(CL_PROFILING_COMMAND_END, &stop);
        print_error(error, "getProfilingInfo");

        cumulative_time += stop - start;

        error = queue.enqueueReadBuffer(results_buffer, CL_TRUE, 0, results_byte_size, results.data());
        print_error(error, "clEnqueueReadBuffer");

        for (unsigned int i = 0; i < nb; ++i) {
          if (!results[i]) {
            const nat_type bad = ns[i];

            bad_table.insert(bad);
            if (print_bad) {
              std::cout << bad << std::endl;
            }
          }
        }
        if (print_bad) {
          std::cout.flush();
        }

        nb = 0;
      }
    }

    if (print_bad) {
      std::cout << "# bad numbers: " << bad_table.size() << std::endl;
    }

    if (print_time) {
      std::cout << "Total OpenCL duration:\t"
                << helper::duration_ms_to_string(static_cast<double>(cumulative_time)/(1000*1000))
                << std::endl;
    }

    return bad_table;
  }


  std::set<nat_type>
  opencl_check_gentle_varsigma_odd__parallelize_factorization(nat_type first_n, nat_type last_n,
                                                              unsigned int opencl_nb,
                                                              bool print_bad, bool print_time) {
    assert(sigmaodd::is_odd(first_n));
    assert(3 <= first_n);
    assert(first_n <= last_n);
    assert(last_n <= sigmaodd::MAX_POSSIBLE_N);

    assert(opencl_nb >= 2);
    assert(sigmaodd::divide_until_odd(opencl_nb) == 1);  // power of 2

#ifdef PRIME16
    assert(n < 65536);
#endif

    assert(sizeof(nat_type) == 8);
    assert(sizeof(nat_type) == sizeof(cl_ulong));

    assert(sizeof(prime_type) == 4);
    assert(sizeof(prime_type) == sizeof(cl_uint));

    cl_ulong cumulative_time = 0;

    std::string kernel_src(helper::file_to_string("opencl_src/sigmaodd.cl"));


    // Init OpenCL
    cl_int error;
    const cl::Device device = opencl::get_first_device_gpu();
    const VECTOR_CLASS<cl::Device> devices = {device};
    const cl::Context context{device};


    // Init OpenCL buffer
    size_t primes_byte_size = sigmaodd::odd_primes_table_nb()*sizeof(prime_type);
    size_t factors_byte_size = opencl_nb*sizeof(nat_type);

    cl::Buffer primes_buffer{context, CL_MEM_READ_ONLY, primes_byte_size};
    cl::Buffer factors_buffer{context, CL_MEM_READ_WRITE, factors_byte_size};


    // Command queue
    const cl::CommandQueue queue{context, device, CL_QUEUE_PROFILING_ENABLE};

    error = queue.enqueueWriteBuffer(primes_buffer, CL_TRUE, 0, primes_byte_size,
                                     sigmaodd::odd_primes_table_ptr());
    print_error(error, "clEnqueueWriteBuffer");


    // Program
    cl::Program program{context, kernel_src};
    error = program.build(devices, "-DONLY_MAIN_KERNEL -cl-single-precision-constant -cl-denorms-are-zero -cl-mad-enable -cl-no-signed-zeros -cl-unsafe-math-optimizations -cl-finite-math-only -cl-fast-relaxed-math"
#ifdef BUILD_LOG
                          " -cl-nv-verbose"
#endif
                          );
    print_error(error, "clBuildProgram");

#ifdef BUILD_LOG
    {
      std::string log;

      error = program.getBuildInfo(device, CL_PROGRAM_BUILD_LOG, &log);
      print_error(error, "clGetProgramBuildInfo");
      std::cout << log << std::endl;
    }
#endif

    cl::Kernel kernel{program, "compute_partial_sigma_odd"};

#ifdef BUILD_LOG
    {
      size_t work_group_size;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_WORK_GROUP_SIZE, &work_group_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_WORK_GROUP_SIZE)");

      size_t compile_work_group_size[3];

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_COMPILE_WORK_GROUP_SIZE, &compile_work_group_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_COMPILE_WORK_GROUP_SIZE)", &compile_work_group_size);

      cl_ulong local_mem_size;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_LOCAL_MEM_SIZE, &local_mem_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_LOCAL_MEM_SIZE)");

      size_t preferred_work_group_size_multiple;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, &preferred_work_group_size_multiple);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE)");

      cl_ulong private_mem_size;

      error = kernel.getWorkGroupInfo(device, CL_KERNEL_PRIVATE_MEM_SIZE, &private_mem_size);
      print_error(error, "clGetKernelWorkGroupInfo(CL_KERNEL_PRIVATE_MEM_SIZE)");
    }
#endif

    error = kernel.setArg(0, primes_buffer);
    print_error(error, "clSetKernelArg(0, primes_buffer)");

    error = kernel.setArg(2, opencl_nb);
    print_error(error, "clSetKernelArg(2, opencl_nb)");

    error = kernel.setArg(4, factors_buffer);
    print_error(error, "clSetKernelArg(4, factors_buffer)");

    cl::Event event;

    std::set<nat_type> bad_table;

    for (nat_type n = first_n; n <= last_n; n += 2) {
      if (!sigmaodd::is_first_mersenne_prime_unitary_divide_or_square(n)) {
        unsigned int prime_offset = 0;
        nat_type sqrt_n_divided = sigmaodd::floor_square_root(n);
        nat_type n_divided = n;
        nat_type varsigma_odd = 1;

        do {
          error = kernel.setArg(1, prime_offset);
#ifndef NDEBUG
          print_error(error, "clSetKernelArg(1, prime_offset)");
#endif

          assert(n_divided != 0);

          error = kernel.setArg(3, n_divided);
#ifndef NDEBUG
          print_error(error, "clSetKernelArg(3, n_divided)");
#endif

          error = queue.enqueueNDRangeKernel(kernel,
                                             cl::NullRange,
                                             cl::NDRange(opencl_nb),
                                             cl::NullRange,
                                             nullptr,
                                             &event);
#ifndef NDEBUG
          print_error(error, "clEnqueueNDRangeKernel");
#endif

          cl_ulong start;
          cl_ulong stop;

          error = event.wait();
#ifndef NDEBUG
          print_error(error, "clWaitForEvents");
#endif

          error = event.getProfilingInfo(CL_PROFILING_COMMAND_START, &start);
#ifndef NDEBUG
          print_error(error, "getProfilingInfo");
#endif
          error = event.getProfilingInfo(CL_PROFILING_COMMAND_END, &stop);
#ifndef NDEBUG
          print_error(error, "getProfilingInfo");
#endif

          cumulative_time += stop - start;

          nat_type results[2];

          error = queue.enqueueReadBuffer(factors_buffer, CL_TRUE, 0, sizeof(nat_type)*2, &results);
#ifndef NDEBUG
          print_error(error, "clEnqueueReadBuffer");
#endif

          assert(results[0] != 0);
          assert(results[0] <= n_divided);

          if (results[0] > 1) {  // at least one prime factor found
            assert(sigmaodd::is_divide(results[0], n_divided));

            n_divided /= results[0];
            sqrt_n_divided = sigmaodd::floor_square_root(n_divided);

            assert(results[1] != 0);

            varsigma_odd *= sigmaodd::divide_until_odd(results[1]);

            if (varsigma_odd
                * sequential::sequential_sigma_odd_upper_bound_with_sqrt(n_divided, bad_table, 0,
                                                                         sqrt_n_divided) < n) {
              n_divided = 1;
            }
          }
          prime_offset = std::min(prime_offset + opencl_nb,
                                  sigmaodd::odd_primes_table_nb() - opencl_nb);
        } while ((n_divided > 1)
                 && (*(sigmaodd::odd_primes_table_ptr() + prime_offset) <= sqrt_n_divided));

        if (n_divided > 1) {  // remain n_divided is prime
          varsigma_odd *= sigmaodd::divide_until_odd(n_divided + 1);
        }

        if (varsigma_odd > n) {  // bad number
          bad_table.insert(n);
          if (print_bad) {
            std::cout << n << std::endl;
            std::cout.flush();
          }
        }
      }
    }

    if (print_bad) {
      std::cout << "# bad numbers: " << bad_table.size() << std::endl;
    }

    if (print_time) {
      std::cout << "Total OpenCL duration:\t"
                << helper::duration_ms_to_string(static_cast<double>(cumulative_time)/(1000*1000))
                << std::endl;
    }

    return bad_table;
  }


  std::vector<nat_type>
  opencl_check_ns(const std::vector<nat_type> &ns) {
    assert(sizeof(nat_type) == sizeof(cl_ulong));
    assert(sizeof(prime_type) == sizeof(cl_uint));

    const std::string kernel_src(helper::file_to_string("opencl_src/sigmaodd.cl"));

    // Init OpenCL
    cl_int error;
    const cl::Device device = opencl::get_first_device_gpu();
    const VECTOR_CLASS<cl::Device> devices = {device};
    const cl::Context context{device};


    // Init OpenCL buffer
    size_t primes_byte_size = sigmaodd::odd_primes_table_nb()*sizeof(prime_type);
    size_t ns_byte_size = ns.size()*sizeof(nat_type);

    cl::Buffer primes_buffer{context, CL_MEM_READ_ONLY, primes_byte_size};
    cl::Buffer ns_buffer{context, CL_MEM_READ_ONLY, ns_byte_size};
    cl::Buffer results_buffer{context, CL_MEM_WRITE_ONLY, ns_byte_size};


    // Command queue
    const cl::CommandQueue queue{context, device, CL_QUEUE_PROFILING_ENABLE};

    queue.enqueueWriteBuffer(primes_buffer, CL_TRUE, 0, primes_byte_size,
                             sigmaodd::odd_primes_table_ptr());


    // Program
    cl::Program program{context, kernel_src};

    error = program.build(devices);
    print_error(error, "clBuildProgram");

    cl::Kernel kernel{program, "check_ns"};

    error = kernel.setArg(0, primes_buffer);
    print_error(error, "clSetKernelArg(0, primes_buffer)");

    error = kernel.setArg(1, ns_buffer);
    print_error(error, "clSetKernelArg(1, ns_buffer)");

    error = kernel.setArg(2, results_buffer);
    print_error(error, "clSetKernelArg(2, results_buffer)");

    cl::Event event;

    queue.enqueueWriteBuffer(ns_buffer, CL_TRUE, 0, ns_byte_size, ns.data());
    error = queue.enqueueNDRangeKernel(kernel,
                                       cl::NullRange,
                                       cl::NDRange(ns.size()),
                                       cl::NullRange,
                                       nullptr,
                                       &event);
    print_error(error, "clEnqueueNDRangeKernel");

    event.wait();

    std::vector<nat_type> results(ns.size());

    queue.enqueueReadBuffer(results_buffer, CL_TRUE, 0, ns_byte_size, results.data());

    std::vector<nat_type> bads;

    for (unsigned int i = 0; i < results.size(); ++i) {
      if (!results[i]) {
        bads.push_back(ns[i]);
      }
    }

    return bads;
  }


  std::vector<nat_type>
  opencl_run_program_on_ns(const std::string &filename, const std::string &kernal_name,
                           const std::vector<nat_type> &ns) {
    assert(sizeof(nat_type) == sizeof(cl_ulong));
    assert(sizeof(prime_type) == sizeof(cl_uint));

    const std::string kernel_src(helper::file_to_string(filename));

    // Init OpenCL
    const cl::Device device = opencl::get_first_device_gpu();
    const VECTOR_CLASS<cl::Device> devices = {device};
    const cl::Context context{device};


    // Init OpenCL buffer
    size_t primes_byte_size = sigmaodd::odd_primes_table_nb()*sizeof(prime_type);
    size_t ns_byte_size = ns.size()*sizeof(nat_type);

    cl::Buffer primes_buffer{context, CL_MEM_READ_ONLY, primes_byte_size};
    cl::Buffer ns_buffer{context, CL_MEM_READ_ONLY, ns_byte_size};
    cl::Buffer results_buffer{context, CL_MEM_WRITE_ONLY, ns_byte_size};


    // Command queue
    const cl::CommandQueue queue{context, device};

    queue.enqueueWriteBuffer(primes_buffer, CL_TRUE, 0, primes_byte_size,
                             sigmaodd::odd_primes_table_ptr());
    queue.enqueueWriteBuffer(ns_buffer, CL_TRUE, 0, ns_byte_size, ns.data());


    // Program
    cl::Program program{context, kernel_src};

    program.build(devices);

    cl::Kernel kernel{program, kernal_name.c_str()};

    kernel.setArg(0, primes_buffer);
    kernel.setArg(1, ns_buffer);
    kernel.setArg(2, results_buffer);

    queue.enqueueNDRangeKernel(kernel,
                               cl::NullRange,
                               cl::NDRange(ns.size()),
                               cl::NullRange,
                               nullptr,
                               nullptr);

    std::vector<nat_type> results(ns.size());

    queue.enqueueReadBuffer(results_buffer, CL_TRUE, 0, ns_byte_size, results.data());

    return results;
  }

}  // namespace opencl
