/* -*- coding: latin-1 -*- */
/** \file opencl/opencl/opencl.hpp (February 6, 2018)
 * \brief
 * Implementation of the OpenCL parallel algorithms presented in the report.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_OPENCL_OPENCL_OPENCL_HPP_
#define PROGS_SRC_OPENCL_OPENCL_OPENCL_HPP_

// \cond
#include <set>
#include <string>
#include <vector>
// \endcond

#include "helper.hpp"


namespace opencl {

  /* ************
   * Prototypes *
   **************/

  /** \brief
   * Check in the order all odd gentle numbers between first_n and last_n,
   * and if print_bad
   *     then print all bad numbers between first_n and last_n (included).
   * The consequence of the result is that:
   * if (all numbers < first_n respect the conjecture)
   *     and (all perfect squares < last_n respect the conjecture)
   *     and (all bad numbers < last_n respect the conjecture)
   * then all numbers < last_n respect the conjecture.
   *
   * If prime_time
   * then print the calculation time computed by OpenCL.
   *
   * sigmaodd::primes.cpp must be compiled without the macro PRIME16.
   *
   * @param first_n 3 <= odd <= last_n
   * @param last_n <= MAX_POSSIBLE_N
   * @param opencl_nb_number number of n transmitted to OpenCL in one step
   * @param print_bad
   * @param print_time
   *
   * @return the set of all bad numbers between first_n and last_n (included)
   */
  std::set<nat_type>
  opencl_check_gentle_varsigma_odd(nat_type first_n, nat_type last_n,
                                   unsigned int opencl_nb_number = 65536,
                                   bool print_bad = true, bool print_time = true);


  /** \brief
   * Like opencl_check_gentle_varsigma_odd()
   * but instead parallelize on group of opencl_nb numbers,
   * compute separately for each number and parallelize the factorization.
   *
   * sigmaodd::primes.cpp must be compiled without the macro PRIME16.
   *
   * @param first_n 3 <= odd <= last_n
   * @param last_n <= MAX_POSSIBLE_N
   * @param opencl_nb number of OpenCL units (must be a power of 2, at least 2)
   * @param print_bad
   * @param print_time
   *
   * @return the set of all bad numbers between first_n and last_n (included)
   */
  std::set<nat_type>
  opencl_check_gentle_varsigma_odd__parallelize_factorization(nat_type first_n, nat_type last_n,
                                                              unsigned int opencl_nb = 1024,
                                                              bool print_bad = true, bool print_time = true);


  /** \brief
   * Check all numbers in ns
   * and return vector of all bad numbers found.
   *
   * @param ns vector of odds
   */
  std::vector<nat_type>
  opencl_check_ns(const std::vector<nat_type> &ns);


  /** \brief
   * Run the OpenCL program from filename on ns.
   * Used for tests.
   */
  std::vector<nat_type>
  opencl_run_program_on_ns(const std::string &filename, const std::string &kernal_name,
                           const std::vector<nat_type> &ns);

}  // namespace opencl

#endif  // PROGS_SRC_OPENCL_OPENCL_OPENCL_HPP_
