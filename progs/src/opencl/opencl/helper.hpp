/* -*- coding: latin-1 -*- */
/** \file opencl/opencl/helper.hpp (December 17, 2017)
 * \brief
 * Helper functions for the OpenCL.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_OPENCL_OPENCL_HELPER_HPP_
#define PROGS_SRC_OPENCL_OPENCL_HELPER_HPP_

// \cond
// #define __CL_ENABLE_EXCEPTIONS
#include <CL/cl.hpp>

#include <iostream>
#include <string>
#include <map>
// \endcond

#include "../../common/sigmaodd/helper.hpp"
#include "../../common/sigmaodd/primes.hpp"


namespace opencl {

  using nat_type = sigmaodd::nat_type;
  using prime_type = sigmaodd::prime_type;


  /* **********
   * Constant *
   ************/

  /** \brief
   * Table associating error codes to error names.
   *
   * https://www.khronos.org/registry/OpenCL/sdk/1.0/docs/man/xhtml/errors.html
   */
  extern const std::map<int32_t, std::string> errors_map;



  /* ************
   * Prototypes *
   **************/

  /** \brief
   * Return the corresponding information about the device.
   */
  cl_bool
  get_device_info_cl_bool(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  cl_device_fp_config
  get_device_info_cl_device_fp_config(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  cl_device_local_mem_type
  get_device_info_cl_device_local_mem_type(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  cl_device_mem_cache_type
  get_device_info_cl_device_mem_cache_type(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  cl_device_type
  get_device_info_cl_device_type(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  cl_uint
  get_device_info_cl_uint(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  cl_ulong
  get_device_info_cl_ulong(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  size_t
  get_device_info_size_t(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  std::string
  get_device_info_string(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the corresponding information about the device.
   */
  VECTOR_CLASS<::size_t>
  get_device_info_vector_size_t(const cl::Device &device, cl_device_info info);


  /** \brief
   * Return the first GPU device found.
   * If not found then print an error message and exit.
   */
  cl::Device
  get_first_device_gpu();


  /** \brief
   * Return the error name corresponding to the error code.
   */
  std::string
  error_name(cl_int code);


  /** \brief
   * Print information on this device.
   */
  void
  print_device(const cl::Device &device);


  /** \brief
   * Print an error message corresponding to the error code.
   *
   * If only_if_error
   * then print only if != 0,
   * else print same if code == 0.
   *
   * If exit_if_error and (code != 0)
   * then exit.
   */
  void
  print_error(cl_int code, std::string message = "",
              bool only_if_error = true, bool exit_if_error = true);


  /** \brief
   * Print information on this platform.
   */
  void
  print_platform(const cl::Platform &platform);


  /** \brief
   * Print information on all platforms.
   */
  void
  print_platforms();

}  // namespace opencl

#endif  // PROGS_SRC_OPENCL_OPENCL_HELPER_HPP_
