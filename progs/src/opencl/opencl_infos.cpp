/* -*- coding: latin-1 -*- */
/** \file opencl/opencl_infos.cpp (December 16, 2017)
 * \brief
 * Print infos about OpenCL platforms and devices.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <iostream>
// \endcond

#include "opencl/opencl.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: opencl_infos [option]" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --first-gpu   only the first GPU device found" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/
int
main(int argc, const char* const argv[]) {
  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--first-gpu") {
      const cl::Device device = opencl::get_first_device_gpu();

      std::cout << "OpenCL first GPU device found:" << std::endl;
      opencl::print_device(device);

      exit(EXIT_SUCCESS);
    }
    else {
      help_and_exit();
    }
  }

  std::cout << "OpenCL platforms and devices:" << std::endl;
  opencl::print_platforms();

  return EXIT_SUCCESS;
}
