/* -*- coding: latin-1 -*- */

/** \file opencl/tests/test__opencl_src__functions.hpp (February 6, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <vector>

#include "../../common/tests/test_helper.hpp"

#include "../../common/sigmaodd/helper.hpp"
#include "../../sequential/sequential/sequential.hpp"

#include "../opencl/opencl.hpp"


using namespace opencl;



// All bad numbers <= 4000000
const std::vector<nat_type>
bad_table_to_4000000_list{2205, 19845, 108045, 143325, 178605, 187425,
    236925, 266805, 319725, 353925, 372645, 407925, 452025, 462825, 584325,
    637245, 646425, 658125, 672525, 789525, 796005, 804825, 845325,
    920205, 972405, 981225, 1007325, 1055925, 1069425,
    1102725, 1113525, 1116225, 1166445, 1201725, 1245825, 1289925, 1378125, 1380825,
    1442925, 1510425, 1547325, 1573605, 1607445, 1642725, 1660725, 1686825, 1730925,
    1854405, 1907325, 1965645, 1995525, 2119005, 2127825, 2132325, 2171925, 2401245,
    2524725, 2568825, 2657025, 2833425, 2877525, 2965725, 3018645, 3053925, 3098025,
    3185325, 3230325, 3353805, 3450825, 3494925, 3583125, 3671325, 3706605, 3715425,
    3847725, 3891825};

const std::set<nat_type> bad_table_to_4000000(bad_table_to_4000000_list.cbegin(),
                                              bad_table_to_4000000_list.cend());


std::vector<nat_type> ns_10000;


class Test__sigmaodd__sigmaodd : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    helper::print_intern_config_compiler();


    TS_ASSERT(sigmaodd::read_primes_table());

    TS_ASSERT_DIFFERS(sigmaodd::array_odd_primes_,nullptr);


    for (unsigned int i = 0; i < 10000; ++i) {
      ns_10000.push_back(i);
    }
  }


  void test__divide_until_odd() {
    SHOW_FUNC_NAME;

    const std::vector<nat_type> ns(ns_10000.cbegin() + 1, ns_10000.cend());
    const std::vector<nat_type> results
      = opencl_run_program_on_ns("opencl_src/sigmaodd.cl", "test__divide_until_odd", ns);

    TS_ASSERT_EQUALS(results.size(), ns.size());

    for (unsigned int i = 0; i < results.size(); ++i) {
      TS_ASSERT_EQUALS(ns[i], i + 1);
      TS_ASSERT_EQUALS(results[i], sigmaodd::divide_until_odd(ns[i]));
    }
  }


  void test__floor_square_root() {
    SHOW_FUNC_NAME;

    const std::vector<nat_type> ns(ns_10000);
    const std::vector<nat_type> results
      = opencl_run_program_on_ns("opencl_src/sigmaodd.cl", "test__floor_square_root", ns);

    TS_ASSERT_EQUALS(results.size(), ns.size());

    for (unsigned int i = 0; i < results.size(); ++i) {
      TS_ASSERT_EQUALS(ns[i], i);
      TS_ASSERT_LESS_THAN_EQUALS(sigmaodd::square(results[i]), ns[i]);
      TS_ASSERT_LESS_THAN(ns[i], sigmaodd::square(results[i] + 1));
    }
  }


  void test__opencl_check_gentle_varsigma_odd() {
    SHOW_FUNC_NAME;

    opencl_check_gentle_varsigma_odd(3, 108045);

    for (unsigned int nb : {256, 65536, 65536*2, 65536*4}) {
      const std::set<nat_type> results = opencl_check_gentle_varsigma_odd(3, 4000000,
                                                                          nb,
                                                                          false, true);

      TS_ASSERT_EQUALS(results.size(), bad_table_to_4000000.size());
      TS_ASSERT_EQUALS(results, bad_table_to_4000000);
    }
  }


  void test__opencl_check_gentle_varsigma_odd__parallelize_factorization() {
    SHOW_FUNC_NAME;

    opencl_check_gentle_varsigma_odd__parallelize_factorization(3, 108045);

    std::set<nat_type> bad_table_to_100000;

    for (nat_type n : bad_table_to_4000000) {
      if (n <= 100000) {
        bad_table_to_100000.insert(n);
      }
    }

    for (unsigned int nb : {32, 64, 128, 256, 512, 1024}) {
      const std::set<nat_type> results
        = opencl_check_gentle_varsigma_odd__parallelize_factorization(3, 100000,
                                                                      nb,
                                                                      false, true);

      TS_ASSERT_EQUALS(results.size(), bad_table_to_100000.size());
      TS_ASSERT_EQUALS(results, bad_table_to_100000);
    }
  }


  void test__pow_nat() {
    SHOW_FUNC_NAME;

    const std::vector<nat_type> ns(ns_10000);
    const std::vector<nat_type> results
      = opencl_run_program_on_ns("opencl_src/sigmaodd.cl", "test__pow_nat", ns);

    TS_ASSERT_EQUALS(results.size(), ns.size());

    for (unsigned int i = 0; i < results.size(); ++i) {
      TS_ASSERT_EQUALS(ns[i], i);
      TS_ASSERT_EQUALS(results[i], sigmaodd::pow_nat(ns[i], i % 5));
    }
  }


  void test__sum_geometric_progression_strict() {
    SHOW_FUNC_NAME;

    const std::vector<nat_type> ns(ns_10000.cbegin() + 2, ns_10000.cend());
    const std::vector<nat_type> results
      = opencl_run_program_on_ns("opencl_src/sigmaodd.cl",
                                 "test__sum_geometric_progression_strict", ns);

    TS_ASSERT_EQUALS(results.size(), ns.size());

    for (unsigned int i = 0; i < results.size(); ++i) {
      TS_ASSERT_EQUALS(ns[i], i + 2);
      TS_ASSERT_EQUALS(results[i], sigmaodd::sum_geometric_progression(ns[i], i % 16));
    }
  }


  void test__varsigma_odd() {
    SHOW_FUNC_NAME;

    const std::vector<nat_type> ns(ns_10000.cbegin() + 2, ns_10000.cend());
    const std::vector<nat_type> results
      = opencl_run_program_on_ns("opencl_src/sigmaodd.cl", "test__varsigma_odd", ns);

    TS_ASSERT_EQUALS(results.size(), ns.size());

    for (unsigned int i = 0; i < results.size(); ++i) {
      TS_ASSERT_EQUALS(ns[i], i + 2);
      if (sigmaodd::is_odd(ns[i])) {
        TS_ASSERT_EQUALS(results[i],
                         sequential::sequential_varsigma_odd(ns[i]));
      }
    }
  }
};
