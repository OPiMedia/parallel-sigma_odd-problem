/* -*- coding: latin-1 -*- */
/** \file opencl/start_opencl.cpp (January 8, 2018)
 * \brief
 * Run an OpenCL program on some little numbers without printing,
 * only to "start" OpenCL capabilities.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
// \endcond

#include "../common/helper/helper.hpp"

#include "opencl/opencl.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: start_opencl" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  // Read command line parameters
  if (argc > 1) {
    help_and_exit();
  }


  // Main calculation
  opencl::opencl_check_gentle_varsigma_odd(3, 1000001, 65536, false);

  return EXIT_SUCCESS;
}
