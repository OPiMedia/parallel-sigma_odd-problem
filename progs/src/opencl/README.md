# sigma\_odd — `progs/src/opencl/`

Disable cache during development phase!

`export CUDA_CACHE_DISABLE=1`
