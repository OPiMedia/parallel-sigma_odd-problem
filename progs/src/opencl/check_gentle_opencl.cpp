/* -*- coding: latin-1 -*- */
/** \file opencl/check_gentle_opencl.cpp (February 6, 2018)
 * \brief
 * Check odd gentle numbers for the varsigma_odd problem
 * and print bad numbers.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
// \endcond

#include "../common/helper/helper.hpp"

#include "opencl/opencl.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: check_gentle [options]   (OpenCL version)" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --first n           first odd number to check (3 by default)" << std::endl
            << "  --infos-first-gpu   print infos about the first GPU found and exit" << std::endl
            << "  --infos-opencl      print infos about OpenCL and exit" << std::endl
            << "  --last n            last odd number to check <= 7927*7927 (1000001 by default)" << std::endl
            << "  --nb n              number of odd numbers to check" << std::endl
            << "  --nb-opencl n       number of OpenCL units (65536 default, must be changed with --par-factor)" << std::endl
            << "  --no-print          do not print bad numbers" << std::endl
            << "  --par-factor        parallelize the factorization instead numbers" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/
int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  sigmaodd::nat_type first = 3;
  sigmaodd::nat_type last = 1000001;
  unsigned int nb_opencl = 65536;
  bool par_factor = false;
  bool print_bad = true;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--first") {
      first = std::max(3ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
      if (sigmaodd::is_even(first)) {
        ++first;
      }
    }
    else if (param == "--infos-first-gpu") {
      const cl::Device device = opencl::get_first_device_gpu();

      std::cout << "OpenCL first GPU device found:" << std::endl;
      opencl::print_device(device);

      exit(EXIT_SUCCESS);
    }
    else if (param == "--infos-opencl") {
      std::cout << "OpenCL platforms and devices:" << std::endl;
      opencl::print_platforms();

      exit(EXIT_SUCCESS);
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
      if (sigmaodd::is_even(last)) {
        --last;
      }
    }
    else if (param == "--nb") {
      last = first + helper::get_ulong(argc, argv, ++i, &help_and_exit)*2 - 1;
    }
    else if (param == "--nb-opencl") {
      nb_opencl = helper::get_uint(argc, argv, ++i, &help_and_exit);
    }
    else if (param == "--no-print") {
      print_bad = false;
    }
    else if (param == "--par-factor") {
      par_factor = true;
    }
    else {
      help_and_exit();
    }
  }


  // Print intern configuration
  helper::print_intern_config_compiler();

  // Print parameters
  std::cout << "opencl/check_gentle"
            << "\tFirst: " << first
            << "\tLast: " << last
            << "\tNb: " << (first <= last
                            ? (last - first)/2 + 1
                            : 0)
            << "\tNb OpenCL: " << nb_opencl
            << std::endl;

  // Print table legend
  std::cout << std::endl
            << 'n' << std::endl;
  std::cout.flush();


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  if (par_factor) {
    if ((nb_opencl < 2) || (sigmaodd::divide_until_odd(nb_opencl) != 1)) {
      help_and_exit();
    }
    opencl::opencl_check_gentle_varsigma_odd__parallelize_factorization(first, last, nb_opencl, print_bad);
  }
  else {
    opencl::opencl_check_gentle_varsigma_odd(first, last, nb_opencl, print_bad);
  }

  // End
  std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

  std::cout << "Total duration:       \t" << helper::duration_to_string(duration)
            << std::endl;

  return EXIT_SUCCESS;
}
