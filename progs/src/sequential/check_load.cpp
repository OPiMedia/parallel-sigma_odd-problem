/* -*- coding: latin-1 -*- */
/** \file sequential/check_load.cpp (January 6, 2018)
 * \brief
 * Check numbers loaded from a file for the varsigma_odd problem
 * and print bad numbers or all, with or without path.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
#include <vector>
// \endcond

#include "../common/helper/helper.hpp"
#include "sequential/sequential.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: check_load [options] filename" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --complete    check complete path until 1 (by default check only partial path until < n)" << std::endl
            << "  --print-all   print all checked number (by default only bad number are printed)" << std::endl
            << "  --useless     check all odd numbers (only with --complete option, by default avoid exactly divisible by 3, 7, 31 or 127)" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  bool complete = false;
  std::string filename;
  bool print_all = false;
  bool useless = false;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--complete") {
      complete = true;
    }
    else if (param == "--print-all") {
      print_all = true;
    }
    else if (param == "--useless") {
      useless = true;
    }
    else if (i < static_cast<unsigned int>(argc)) {
      filename = param;
    }
    else {
      help_and_exit();
    }
  }

  if (filename.empty()) {
      help_and_exit();
  }


  // Load numbers
  const std::vector<sigmaodd::nat_type> list = sigmaodd::load_numbers(filename);


  // Print intern configuration
  helper::print_intern_config_compiler();


  // Print parameters
  std::cout << "sequential/check_load"
            << "\tFilename: " << filename
            << "\tNb: " << list.size()
            << "\tComplete?: " << helper::to_string(complete)
            << "\tPrint all?: " << helper::to_string(print_all);
  if (complete) {
    std::cout << "\tUseless?: " << helper::to_string(useless);
  }
  std::cout << std::endl;

  // Print table legend
  std::cout << std::endl
            << "n\tLower\tPartial length\tPartial path";
  if (complete) {
    std::cout << "\tLength\tPath";
  }
  std::cout << std::endl;
  std::cout.flush();


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  for (sigmaodd::nat_type n : list) {
    if (complete) {
      sequential::sequential_check_varsigma_odd_complete(n, n, useless, true, print_all);
    }
    else {
      sequential::sequential_check_varsigma_odd(n, n, true, print_all);
    }
  }

  // End
  std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

  std::cout << "Total duration: " << helper::duration_to_string(duration)
            << std::endl;

  return EXIT_SUCCESS;
}
