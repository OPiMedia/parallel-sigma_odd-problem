/* -*- coding: latin-1 -*- */

/** \file sequential/tests/test__sequential__sequential.hpp (January 17, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>
#include <sstream>
#include <vector>

#include "../../common/tests/test_helper.hpp"

#include "../sequential/sequential.hpp"

#include "../../common/helper/helper.hpp"
#include "../../common/sigmaodd/primes.hpp"
#include "../../common/sigmaodd/sigmaodd.hpp"


using namespace sequential;



// All bad numbers <= 4000000
const std::vector<nat_type>
bad_table_to_4000000_list{2205, 19845, 108045, 143325, 178605, 187425,
    236925, 266805, 319725, 353925, 372645, 407925, 452025, 462825, 584325,
    637245, 646425, 658125, 672525, 789525, 796005, 804825, 845325,
    920205, 972405, 981225, 1007325, 1055925, 1069425,
    1102725, 1113525, 1116225, 1166445, 1201725, 1245825, 1289925, 1378125, 1380825,
    1442925, 1510425, 1547325, 1573605, 1607445, 1642725, 1660725, 1686825, 1730925,
    1854405, 1907325, 1965645, 1995525, 2119005, 2127825, 2132325, 2171925, 2401245,
    2524725, 2568825, 2657025, 2833425, 2877525, 2965725, 3018645, 3053925, 3098025,
    3185325, 3230325, 3353805, 3450825, 3494925, 3583125, 3671325, 3706605, 3715425,
    3847725, 3891825};

const std::set<nat_type> bad_table_to_4000000(bad_table_to_4000000_list.cbegin(),
                                              bad_table_to_4000000_list.cend());



class Test__sigmaodd__sigmaodd : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    helper::print_intern_config_compiler();


    TS_ASSERT(sigmaodd::read_primes_table());

    TS_ASSERT_DIFFERS(sigmaodd::array_odd_primes_,nullptr);

    TS_ASSERT_EQUALS(bad_table_to_4000000_list.size(), 76);
    TS_ASSERT_EQUALS(bad_table_to_4000000.size(), 76);

    for (nat_type n : bad_table_to_4000000_list) {
      // Empirical observations
      TS_ASSERT(sigmaodd::is_divide(9, n));  // seems always true
      TS_ASSERT(sigmaodd::is_divide(5, n));  // only always true for bad numbers of this little table
    }
  }



  void test__sequential_check_gentle_varsigma_odd() {
    SHOW_FUNC_NAME;

    sequential_check_gentle_varsigma_odd(3, bad_table_to_4000000_list[0] - 1);
    sequential_check_gentle_varsigma_odd(3, bad_table_to_4000000_list[3] - 1);

    const std::set<nat_type> bad_table1
      = sequential_check_gentle_varsigma_odd(3, 4000000,
                                             false);
    const std::set<nat_type> bad_table2
      = sequential_check_gentle_varsigma_odd(bad_table_to_4000000_list[62] - 2, 4000000,
                                             false);
    const std::set<nat_type> bad_table3
      = sequential_check_gentle_varsigma_odd(bad_table_to_4000000_list[62], 4000000,
                                             false);

    TS_ASSERT_EQUALS(bad_table1, bad_table_to_4000000);
    TS_ASSERT_EQUALS(bad_table2, bad_table3);
    TS_ASSERT_EQUALS(bad_table_to_4000000_list[62], 3018645);

    TS_ASSERT_LESS_THAN(bad_table2.size(), bad_table1.size());
    for (nat_type n : bad_table1) {
      if (n < bad_table_to_4000000_list[62]) {
        TS_ASSERT_EQUALS(bad_table2.find(n), bad_table2.cend());
      }
      else {
        TS_ASSERT_DIFFERS(bad_table2.find(n), bad_table2.cend());
      }
    }
  }


  void test__sequential_check_gentle_varsigma_odd__bad_table() {
    SHOW_FUNC_NAME;

    sequential_check_gentle_varsigma_odd(3, bad_table_to_4000000_list[0] - 1,
                                         bad_table_to_4000000,
                                         0, 0,
                                         false);
    sequential_check_gentle_varsigma_odd(3, bad_table_to_4000000_list[0] - 1,
                                         bad_table_to_4000000,
                                         0, bad_table_to_4000000_list.back(),
                                         false);
    sequential_check_gentle_varsigma_odd(3, bad_table_to_4000000_list[3] - 1,
                                         bad_table_to_4000000,
                                         0, 0,
                                         false);

    const std::set<nat_type> bad_table1
      = sequential_check_gentle_varsigma_odd(3, 4000000,
                                             bad_table_to_4000000,
                                             0, bad_table_to_4000000_list.back(),
                                             false);
    const std::set<nat_type> bad_table2
      = sequential_check_gentle_varsigma_odd(bad_table_to_4000000_list[62] - 2, 4000000,
                                             bad_table_to_4000000,
                                             0, bad_table_to_4000000_list.back(),
                                             false);
    const std::set<nat_type> bad_table3
      = sequential_check_gentle_varsigma_odd(bad_table_to_4000000_list[62], 4000000,
                                             bad_table_to_4000000,
                                             0, bad_table_to_4000000_list.back(),
                                             false);

    TS_ASSERT_EQUALS(bad_table1, bad_table_to_4000000);
    TS_ASSERT_EQUALS(bad_table2, bad_table3);
    TS_ASSERT_EQUALS(bad_table_to_4000000_list[62], 3018645);

    TS_ASSERT_LESS_THAN(bad_table2.size(), bad_table1.size());
    for (nat_type n : bad_table1) {
      if (n < bad_table_to_4000000_list[62]) {
        TS_ASSERT_EQUALS(bad_table2.find(n), bad_table2.cend());
      }
      else {
        TS_ASSERT_DIFFERS(bad_table2.find(n), bad_table2.cend());
      }
    }
  }


  void test__sequential_check_varsigma_odd() {
    SHOW_FUNC_NAME;

    sequential_check_varsigma_odd(3, bad_table_to_4000000_list[0] - 1);
    sequential_check_varsigma_odd(3, bad_table_to_4000000_list[3] - 1);

    // Redirect cout to ss
    std::stringstream ss;
    auto old_buf = std::cout.rdbuf(ss.rdbuf());

    sequential_check_varsigma_odd(3, 4000000, true, false,
                                  false, false, false, false);

    // Reset cout
    std::cout.rdbuf(old_buf);

    std::string s;

    for (nat_type n : bad_table_to_4000000_list) {
      s += std::to_string(n) + '\n';
    }

    TS_ASSERT_EQUALS(ss.str(), s);
  }


  void test__sequential_check_varsigma_odd_perfect_square() {
    SHOW_FUNC_NAME;

    sequential_check_varsigma_odd_perfect_square(9);
    sequential_check_varsigma_odd_perfect_square(25);

    // Same results that general implementation
    for (nat_type i = 3; i < 10000; i += 2) {
      const nat_type n = sigmaodd::square(i);

      // Redirect cout to ss
      std::stringstream ss;
      auto old_buf = std::cout.rdbuf(ss.rdbuf());

      sequential_check_varsigma_odd_perfect_square(n);

      // Redirect cout to ss2
      std::stringstream ss2;
      std::cout.rdbuf(ss2.rdbuf());
      sequential_check_varsigma_odd(n, n, false, true,
                                    false, true);

      // Reset cout
      std::cout.rdbuf(old_buf);

      TS_ASSERT(!ss.str().empty());
      TS_ASSERT_EQUALS(ss.str(), ss2.str());
    }
  }


  void test__sequential_check_varsigma_odd_complete() {
    SHOW_FUNC_NAME;

    sequential_check_varsigma_odd_complete(3, bad_table_to_4000000_list[0] - 1);
    sequential_check_varsigma_odd_complete(3, bad_table_to_4000000_list[3] - 1);

    // Redirect cout to ss
    std::stringstream ss;
    auto old_buf = std::cout.rdbuf(ss.rdbuf());

    sequential_check_varsigma_odd_complete(3, 4000000, true, true, false,
                                           false, false, false, false);

    // Reset cout
    std::cout.rdbuf(old_buf);

    std::string s;

    for (nat_type n : bad_table_to_4000000_list) {
      s += std::to_string(n) + '\n';
    }

    TS_ASSERT_EQUALS(ss.str(), s);
  }


  void test__sequential_check_varsigma_odd_perfect_square_complete() {
    SHOW_FUNC_NAME;

    sequential_check_varsigma_odd_perfect_square_complete(9);
    sequential_check_varsigma_odd_perfect_square_complete(25);

    // Same results that general implementation
    for (nat_type i = 3; i < 10000; i += 2) {
      const nat_type n = sigmaodd::square(i);

      // Redirect cout to ss
      std::stringstream ss;
      auto old_buf = std::cout.rdbuf(ss.rdbuf());

      sequential_check_varsigma_odd_perfect_square_complete(n);

      // Redirect cout to ss2
      std::stringstream ss2;
      std::cout.rdbuf(ss2.rdbuf());
      sequential_check_varsigma_odd_complete(n, n, false, true, true,
                                             false);

      // Reset cout
      std::cout.rdbuf(old_buf);

      TS_ASSERT(!ss.str().empty());
      TS_ASSERT_EQUALS(ss.str(), ss2.str());
    }
  }


  void test__sequential_is_varsigma_odd_lower__bad_table() {
    SHOW_FUNC_NAME;

    const nat_type last_n = 4000000 - 1;

    for (nat_type n = 3; n <= last_n; n += 2) {
      const bool is_bad = (bad_table_to_4000000.find(n) != bad_table_to_4000000.cend());
      const bool is_square = sigmaodd::is_square(n);

      const bool is_lower1 = sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, n + 1, n + 1);
      const bool is_lower2 = sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, 1, n - 1);

      if (is_bad) {
        TS_ASSERT(!is_square);
      }
      TS_ASSERT_EQUALS(is_lower1, is_lower2);
      TS_ASSERT_DIFFERS(is_lower1, is_bad || is_square);

      TS_ASSERT_EQUALS(is_lower1,
                       sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, 1));
    }

    // Same that sequential implementation
    for (nat_type n = 3; n < 1000000; n += 2) {
      TS_ASSERT_EQUALS(sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, 1, n - 1),
                       sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, 1));
    }
  }


  void test__sequential_iterate_varsigma_odd_until_1() {
    SHOW_FUNC_NAME;

    for (nat_type n = 3; n < 100000; n += 2) {
      const std::vector<nat_type> complete_path = sequential_iterate_varsigma_odd_until_1(n);

      TS_ASSERT_LESS_THAN_EQUALS(2, complete_path.size());
      TS_ASSERT_EQUALS(complete_path[0], n);
      TS_ASSERT_EQUALS(complete_path.back(), 1);

      std::vector<nat_type> path;
      nat_type new_n = n;

      do {
        const std::vector<nat_type> partial_path
          = sequential_iterate_varsigma_odd_until_lower(new_n);

        new_n = partial_path.back();
        path.insert(path.end(), partial_path.cbegin() + (path.empty()
                                                         ? 0
                                                         :1), partial_path.cend());
      } while (new_n > 1);

      TS_ASSERT_EQUALS(complete_path, path);
    }
  }


  void test__sequential_iterate_varsigma_odd_perfect_square_until_1() {
    SHOW_FUNC_NAME;

    // Same results that general implementation
    for (nat_type i = 3; i < 100000; i += 2) {
      const nat_type n = sigmaodd::square(i);
      const std::vector<nat_type> path = sequential_iterate_varsigma_odd_perfect_square_until_1(n);
      const std::vector<nat_type> general_path = sequential_iterate_varsigma_odd_until_1(n);

      TS_ASSERT_EQUALS(path.size(), general_path.size());
      TS_ASSERT_EQUALS(path, general_path);
    }
  }


  void test__sequential_iterate_varsigma_odd_until_lower() {
    SHOW_FUNC_NAME;

    for (nat_type n : {3u, 5u, 7u, 11u, 15u, 17u, 19u}) {
      const std::vector<nat_type> path = sequential_iterate_varsigma_odd_until_lower(n);

      TS_ASSERT_EQUALS(path.size(), 2);
      TS_ASSERT_EQUALS(path[0], n);
      TS_ASSERT_LESS_THAN(path[1], n);
      TS_ASSERT_EQUALS(path[1], sequential_varsigma_odd(n));
    }

    for (nat_type i = 3; i < 100; i += 2) {
      const nat_type n = sigmaodd::square(i);
      const std::vector<nat_type> path = sequential_iterate_varsigma_odd_until_lower(n);
      nat_type reached_n = n;

      for (unsigned int j = 0; j < path.size() - 1; ++j) {
        reached_n = sequential_varsigma_odd(reached_n);
      }

      TS_ASSERT_LESS_THAN_EQUALS(3, path.size());
      TS_ASSERT_EQUALS(path[0], n);
      TS_ASSERT_LESS_THAN(n, path[1]);
      if (path.size() > 3) {
        TS_ASSERT_LESS_THAN(n, path[2]);
      }
      TS_ASSERT_LESS_THAN(path.back(), n);
      TS_ASSERT_EQUALS(path.back(), reached_n);
    }

    for (nat_type n : bad_table_to_4000000_list) {
      const std::vector<nat_type> path = sequential_iterate_varsigma_odd_until_lower(n);

      TS_ASSERT_EQUALS(path.size(), 3);
      TS_ASSERT_EQUALS(path[0], n);
      TS_ASSERT_LESS_THAN(n, path[1]);
      TS_ASSERT_LESS_THAN(path[2], n);
      TS_ASSERT_EQUALS(path[2], sequential_varsigma_odd(sequential_varsigma_odd(n)));
    }
  }


  void test__sequential_iterate_varsigma_odd_perfect_square_until_lower() {
    SHOW_FUNC_NAME;

    // Same results that general implementation
    for (nat_type i = 3; i < 100000; i += 2) {
      const nat_type n = sigmaodd::square(i);
      const std::vector<nat_type> path
        = sequential_iterate_varsigma_odd_perfect_square_until_lower(n);
      const std::vector<nat_type> general_path = sequential_iterate_varsigma_odd_until_lower(n);

      TS_ASSERT_EQUALS(path.size(), general_path.size());
      TS_ASSERT_EQUALS(path, general_path);
    }
  }


  void test__sequential_min_array() {
    SHOW_FUNC_NAME;

    {
      const nat_type as[1] = {42};

      TS_ASSERT_EQUALS(sequential_min_array(as, 1), 42);
    }

    {
      const nat_type as[2] = {42, 666};

      TS_ASSERT_EQUALS(sequential_min_array(as, 1), 42);
      TS_ASSERT_EQUALS(sequential_min_array(as, 2), 42);
    }

    {
      const nat_type as[4] = {666, 42, 5, 9};

      TS_ASSERT_EQUALS(sequential_min_array(as, 1), 666);
      TS_ASSERT_EQUALS(sequential_min_array(as, 2), 42);
      TS_ASSERT_EQUALS(sequential_min_array(as, 3), 5);
      TS_ASSERT_EQUALS(sequential_min_array(as, 4), 5);
    }
  }


  void test__sequential_print_in_order() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__sequential_varsigma_odd() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sequential_varsigma_odd( 1),  1);
    TS_ASSERT_EQUALS(sequential_varsigma_odd( 3),  1);
    TS_ASSERT_EQUALS(sequential_varsigma_odd( 5),  3);
    TS_ASSERT_EQUALS(sequential_varsigma_odd( 7),  1);
    TS_ASSERT_EQUALS(sequential_varsigma_odd( 9), 13);
    TS_ASSERT_EQUALS(sequential_varsigma_odd(11),  3);
    TS_ASSERT_EQUALS(sequential_varsigma_odd(13),  7);
    TS_ASSERT_EQUALS(sequential_varsigma_odd(15),  3);
    TS_ASSERT_EQUALS(sequential_varsigma_odd(17),  9);
    TS_ASSERT_EQUALS(sequential_varsigma_odd(19),  5);

    for (nat_type n : bad_table_to_4000000_list) {
      TS_ASSERT_LESS_THAN(n, sequential_varsigma_odd(n));
    }

    // Same results that previous implementation
    for (nat_type n = 1; n < 1000000; n += 2) {
      TS_ASSERT_EQUALS(sequential_varsigma_odd(n),
                       sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n));
    }

    for (nat_type n = 10000001; n < 10010001; n += 2) {
      TS_ASSERT_EQUALS(sequential_varsigma_odd(n),
                       sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n));
    }

    // Special value on prime numbers
    for (unsigned int i = 0; i < 1000; ++i) {
      const nat_type p = sigmaodd::odd_primes_table_by_index(i);

      TS_ASSERT_DIFFERS(p, 0);

      const nat_type p2 = sigmaodd::square(p);
      const nat_type p3 = p2*p;
      const nat_type p4 = sigmaodd::square(p2);

      TS_ASSERT_EQUALS(sequential_varsigma_odd(p),  sigmaodd::divide_until_odd(1 + p));
      TS_ASSERT_EQUALS(sequential_varsigma_odd(p3), sigmaodd::divide_until_odd(1 + p + p2 + p3));

      TS_ASSERT_EQUALS(sequential_varsigma_odd(p2), 1 + p + p2);
      TS_ASSERT_EQUALS(sequential_varsigma_odd(p4), 1 + p + p2 + p3 + p4);
    }
  }


  void test__sequential_varsigma_odd_perfect_square() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sequential_varsigma_odd_perfect_square( 1),  1);
    TS_ASSERT_EQUALS(sequential_varsigma_odd_perfect_square( 9), 13);
    TS_ASSERT_EQUALS(sequential_varsigma_odd_perfect_square(25), 31);

    // Same results that general implementation
    for (nat_type i = 1; i < 100000; i += 2) {
      const nat_type n = sigmaodd::square(i);

      TS_ASSERT_EQUALS(sequential_varsigma_odd_perfect_square(n), sequential_varsigma_odd(n));
    }
  }


  void test__sequential_varsigma_odd_greater_set() {
    SHOW_FUNC_NAME;

    // Same that sequential_is_varsigma_odd_lower() one by one
    for (nat_type n = 3; n < 1000000; n += 2) {
      const std::set<nat_type> is_greaters_set
        = sequential_varsigma_odd_greater_set({n}, bad_table_to_4000000, 1, n - 1);
      const std::vector<nat_type> is_greaters(is_greaters_set.cbegin(), is_greaters_set.cend());

      if (sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, 1, n - 1)) {
        TS_ASSERT_EQUALS(is_greaters.size(), 0);
      }
      else {
        TS_ASSERT_EQUALS(is_greaters.size(), 1);
        TS_ASSERT_EQUALS(is_greaters[0], n);
      }
    }

    // Same that sequential_is_varsigma_odd_lower() in only one call
    std::vector<nat_type> ns;

    for (nat_type n = 3; n < 1000000; n += 2) {
      ns.push_back(n);
    }

    const std::set<nat_type> is_greaters
      = sequential_varsigma_odd_greater_set(ns, bad_table_to_4000000, 1, 666);

    for (nat_type n = 3; n < 1000000; n += 2) {
      TS_ASSERT_EQUALS(is_greaters.find(n) == is_greaters.cend(),
                       sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, 1, n - 1));
    }
  }


  void test__sequential_is_varsigma_odd_lower() {
    SHOW_FUNC_NAME;

    const nat_type last_n = 4000000 - 1;

    for (nat_type n = 3; n <= last_n; n += 2) {
      const bool is_bad = (bad_table_to_4000000.find(n) != bad_table_to_4000000.cend());
      const bool is_square = sigmaodd::is_square(n);

      const bool is_lower1 = sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, n + 1);
      const bool is_lower2 = sequential_is_varsigma_odd_lower(n, bad_table_to_4000000, 1);

      if (is_bad) {
        TS_ASSERT(!is_square);
      }
      TS_ASSERT_EQUALS(is_lower1, is_lower2);
      TS_ASSERT_DIFFERS(is_lower1, is_bad || is_square);
      TS_ASSERT_EQUALS(is_lower1, sequential_varsigma_odd(n) < n);
    }
  }


  void test__sequential_sigma_odd_upper_bound() {
    SHOW_FUNC_NAME;

    const nat_type last_n = 4000000 - 1;

    std::set<nat_type> bad_table;

    TS_ASSERT_EQUALS(sequential_sigma_odd_upper_bound(1, bad_table, 0), 1);

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd,
                                 sequential_sigma_odd_upper_bound(n, bad_table, n + 1));

      if ((varsigmaodd >= n) && (!sigmaodd::is_square(n))) {
        bad_table.insert(n);
      }
    }

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);
      const nat_type upper_bound_1  = sequential_sigma_odd_upper_bound(n, bad_table, 1);
      const nat_type upper_bound_n  = sequential_sigma_odd_upper_bound(n, bad_table, n);
      const nat_type upper_bound_n1 = sequential_sigma_odd_upper_bound(n, bad_table, n + 1);

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_1);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n1);

      TS_ASSERT_EQUALS(upper_bound_1, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(upper_bound_n, upper_bound_n1);

    }

    const nat_type nb = last_n/2 + 1;

    std::cout << bad_table.size() << " bads / " << nb
              << " = " << static_cast<double>(bad_table.size()*100)/nb << '%' << std::endl;

    TS_ASSERT_EQUALS(bad_table.size(), 76);
    TS_ASSERT_EQUALS(bad_table, bad_table_to_4000000);
  }


  void test__sequential_sigma_odd_upper_bound__bad_table() {
    SHOW_FUNC_NAME;

    const nat_type last_n = 4000000 - 1;

    std::set<nat_type> bad_table;

    TS_ASSERT_EQUALS(sequential_sigma_odd_upper_bound(1, bad_table, 0, 0), 1);

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd,
                                 sequential_sigma_odd_upper_bound(n, bad_table, n + 1, n + 1));

      if ((varsigmaodd >= n) && (!sigmaodd::is_square(n))) {
        bad_table.insert(n);
      }
    }

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);
      const nat_type upper_bound_1  = sequential_sigma_odd_upper_bound(n, bad_table, 1, n);
      const nat_type upper_bound_n  = sequential_sigma_odd_upper_bound(n, bad_table, n, n);
      const nat_type upper_bound_n1 = sequential_sigma_odd_upper_bound(n, bad_table, n + 1, n + 1);

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_1);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n1);

      TS_ASSERT_EQUALS(upper_bound_1, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(upper_bound_n, upper_bound_n1);

    }

    const nat_type nb = last_n/2 + 1;

    std::cout << bad_table.size() << " bads / " << nb
              << " = " << static_cast<double>(bad_table.size()*100)/nb << '%' << std::endl;

    TS_ASSERT_EQUALS(bad_table.size(), 76);
    TS_ASSERT_EQUALS(bad_table, bad_table_to_4000000);

    // Same that function without the bad_last_n param
    for (nat_type n = 1; n < 1000000; n += 2) {
      TS_ASSERT_EQUALS(sequential_sigma_odd_upper_bound(n, bad_table, 1, n),
                       sequential_sigma_odd_upper_bound(n, bad_table, 1));
    }
  }


  void test__sequential_sigma_odd_upper_bound_with_sqrt() {
    SHOW_FUNC_NAME;

    const nat_type last_n = 4000000 - 1;

    std::set<nat_type> bad_table;

    TS_ASSERT_EQUALS(sequential_sigma_odd_upper_bound_with_sqrt(1, bad_table, 0,
                                                                1), 1);

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd,
                                 sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, n + 1,
                                                                            sigmaodd::floor_square_root(n)));

      if ((varsigmaodd >= n) && (!sigmaodd::is_square(n))) {
        bad_table.insert(n);
      }
    }

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);
      const nat_type upper_bound_1
        = sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, 1,
                                                     sigmaodd::floor_square_root(n));
      const nat_type upper_bound_n
        = sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, n,
                                                     sigmaodd::floor_square_root(n));
      const nat_type upper_bound_n1
        = sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, n + 1,
                                                     sigmaodd::floor_square_root(n));

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_1);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n1);

      TS_ASSERT_EQUALS(upper_bound_1, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(upper_bound_n, upper_bound_n1);

    }

    const nat_type nb = last_n/2 + 1;

    std::cout << bad_table.size() << " bads / " << nb
              << " = " << static_cast<double>(bad_table.size()*100)/nb << '%' << std::endl;

    TS_ASSERT_EQUALS(bad_table.size(), 76);
    TS_ASSERT_EQUALS(bad_table, bad_table_to_4000000);

    // Same that function without the sqrt_n param
    for (nat_type n = 1; n < 1000000; n += 2) {
      TS_ASSERT_EQUALS(sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, 1,
                                                                  sigmaodd::floor_square_root(n)),
                       sequential_sigma_odd_upper_bound(n, bad_table, 1));
    }
  }


  void test__sequential_sigma_odd_upper_bound_with_sqrt__bad_table() {
    SHOW_FUNC_NAME;

    const nat_type last_n = 4000000 - 1;

    std::set<nat_type> bad_table;

    TS_ASSERT_EQUALS(sequential_sigma_odd_upper_bound_with_sqrt(1, bad_table, 0, 0,
                                                                1), 1);

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd,
                                 sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, n + 1, n + 1,
                                                                            sigmaodd::floor_square_root(n)));

      if ((varsigmaodd >= n) && (!sigmaodd::is_square(n))) {
        bad_table.insert(n);
      }
    }

    for (nat_type n = 1; n <= last_n; n += 2) {
      const nat_type varsigmaodd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);
      const nat_type upper_bound_1
        = sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, 1, n,
                                                     sigmaodd::floor_square_root(n));
      const nat_type upper_bound_n
        = sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, n, n,
                                                     sigmaodd::floor_square_root(n));
      const nat_type upper_bound_n1
        = sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, n + 1, n + 1,
                                                     sigmaodd::floor_square_root(n));

      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_1);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(varsigmaodd, upper_bound_n1);

      TS_ASSERT_EQUALS(upper_bound_1, upper_bound_n);
      TS_ASSERT_LESS_THAN_EQUALS(upper_bound_n, upper_bound_n1);

    }

    const nat_type nb = last_n/2 + 1;

    std::cout << bad_table.size() << " bads / " << nb
              << " = " << static_cast<double>(bad_table.size()*100)/nb << '%' << std::endl;

    TS_ASSERT_EQUALS(bad_table.size(), 76);
    TS_ASSERT_EQUALS(bad_table, bad_table_to_4000000);

    // Same that function without the bad_last_n param
    for (nat_type n = 1; n < 1000000; n += 2) {
      TS_ASSERT_EQUALS(sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, 1, n,
                                                                  sigmaodd::floor_square_root(n)),
                       sequential_sigma_odd_upper_bound_with_sqrt(n, bad_table, 1,
                                                                  sigmaodd::floor_square_root(n)));
    }
  }

};
