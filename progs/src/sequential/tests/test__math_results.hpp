/* -*- coding: latin-1 -*- */

/** \file sequential/tests/test__math_results.hpp (August 30, 2020)
 *
 * GPLv3 --- Copyright (C) 2017, 2018, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <iostream>
#include <sstream>
#include <vector>

#include "../../common/tests/test_helper.hpp"

#include "../sequential/sequential.hpp"


using namespace sequential;



// All bad numbers <= 4000000
const std::vector<nat_type>
bad_table_to_4000000_list{2205, 19845, 108045, 143325, 178605, 187425,
    236925, 266805, 319725, 353925, 372645, 407925, 452025, 462825, 584325,
    637245, 646425, 658125, 672525, 789525, 796005, 804825, 845325,
    920205, 972405, 981225, 1007325, 1055925, 1069425,
    1102725, 1113525, 1116225, 1166445, 1201725, 1245825, 1289925, 1378125, 1380825,
    1442925, 1510425, 1547325, 1573605, 1607445, 1642725, 1660725, 1686825, 1730925,
    1854405, 1907325, 1965645, 1995525, 2119005, 2127825, 2132325, 2171925, 2401245,
    2524725, 2568825, 2657025, 2833425, 2877525, 2965725, 3018645, 3053925, 3098025,
    3185325, 3230325, 3353805, 3450825, 3494925, 3583125, 3671325, 3706605, 3715425,
    3847725, 3891825};

const std::set<nat_type> bad_table_to_4000000(bad_table_to_4000000_list.cbegin(),
                                              bad_table_to_4000000_list.cend());



class Test__sigmaodd__sigmaodd : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    helper::print_intern_config_compiler();


    TS_ASSERT(sigmaodd::read_primes_table());

    TS_ASSERT_DIFFERS(sigmaodd::array_odd_primes_,nullptr);

    TS_ASSERT_EQUALS(bad_table_to_4000000_list.size(), 76);
    TS_ASSERT_EQUALS(bad_table_to_4000000.size(), 76);

    for (nat_type n : bad_table_to_4000000_list) {
      TS_ASSERT(!sigmaodd::is_square(n));
      TS_ASSERT_LESS_THAN(n, sequential_varsigma_odd(n));

      // Empirical observations
      TS_ASSERT(sigmaodd::is_divide(9, n));  // seems always true
      TS_ASSERT(sigmaodd::is_divide(5, n));  // true on this little table
    }
  }


  void test__bad_3_5000000001() {
    SHOW_FUNC_NAME;

    const std::vector<sigmaodd::nat_type> bads
      = sigmaodd::load_numbers("../../tables/tsv/sequential_check_bad_3_5000000001.tsv");

    TS_ASSERT_EQUALS(bads.size(), 29517);

    for (unsigned int i = 0; i < bad_table_to_4000000_list.size(); ++i) {
      TS_ASSERT_EQUALS(bads[i], bad_table_to_4000000_list[i]);
    }

    unsigned int nb_not_5_divide = 0;
    unsigned int nb_not_7_divide = 0;
    unsigned int nb_not_11_divide = 0;
    unsigned int nb_not_13_divide = 0;
    unsigned int nb_not_17_divide = 0;
    unsigned int nb_not_19_divide = 0;

    unsigned int nb_not_27_divide = 0;

    for (nat_type n : bads) {
      TS_ASSERT(!sigmaodd::is_square(n));
      TS_ASSERT_LESS_THAN(n, sequential_varsigma_odd(n));

      TS_ASSERT(sigmaodd::is_divide(9, n));  // empirical observations, seems always true

      if (!sigmaodd::is_divide(5, n)) {
        ++nb_not_5_divide;
      }
      if (!sigmaodd::is_divide(7, n)) {
        ++nb_not_7_divide;
      }
      if (!sigmaodd::is_divide(11, n)) {
        ++nb_not_11_divide;
      }
      if (!sigmaodd::is_divide(13, n)) {
        ++nb_not_13_divide;
      }
      if (!sigmaodd::is_divide(17, n)) {
        ++nb_not_17_divide;
      }
      if (!sigmaodd::is_divide(19, n)) {
        ++nb_not_19_divide;
      }

      if (!sigmaodd::is_divide(27, n)) {
        ++nb_not_27_divide;
      }
    }

    std::cout << "27 not divide n\t"
              << nb_not_27_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_27_divide*100)/static_cast<double>(bads.size())
              << std::endl;

    std::cout << "5 not divide n\t"
              << nb_not_5_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_5_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "7 not divide n\t"
              << nb_not_7_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_7_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "11 not divide n\t"
              << nb_not_11_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_11_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "13 not divide n\t"
              << nb_not_13_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_13_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "17 not divide n\t"
              << nb_not_17_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_17_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "19 not divide n\t"
              << nb_not_19_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_19_divide*100)/static_cast<double>(bads.size())
              << std::endl;
  }


  void test__bad_5000000001_10000000001() {
    SHOW_FUNC_NAME;

    const std::vector<sigmaodd::nat_type> bads
      = sigmaodd::load_numbers("../../tables/tsv/sequential_check_bad_5000000001_10000000001.tsv");

    TS_ASSERT_EQUALS(bads.size(), 25618);

    unsigned int nb_not_5_divide = 0;
    unsigned int nb_not_7_divide = 0;
    unsigned int nb_not_11_divide = 0;
    unsigned int nb_not_13_divide = 0;
    unsigned int nb_not_17_divide = 0;
    unsigned int nb_not_19_divide = 0;

    unsigned int nb_not_27_divide = 0;

    for (nat_type n : bads) {
      TS_ASSERT(!sigmaodd::is_square(n));
      TS_ASSERT_LESS_THAN(n, sequential_varsigma_odd(n));

      TS_ASSERT(sigmaodd::is_divide(9, n));  // empirical observations, seems always true

      if (!sigmaodd::is_divide(5, n)) {
        ++nb_not_5_divide;
      }
      if (!sigmaodd::is_divide(7, n)) {
        ++nb_not_7_divide;
      }
      if (!sigmaodd::is_divide(11, n)) {
        ++nb_not_11_divide;
      }
      if (!sigmaodd::is_divide(13, n)) {
        ++nb_not_13_divide;
      }
      if (!sigmaodd::is_divide(17, n)) {
        ++nb_not_17_divide;
      }
      if (!sigmaodd::is_divide(19, n)) {
        ++nb_not_19_divide;
      }

      if (!sigmaodd::is_divide(27, n)) {
        ++nb_not_27_divide;
      }
    }

    std::cout << "27 not divide n\t"
              << nb_not_27_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_27_divide*100)/static_cast<double>(bads.size())
              << std::endl;

    std::cout << "5 not divide n\t"
              << nb_not_5_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_5_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "7 not divide n\t"
              << nb_not_7_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_7_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "11 not divide n\t"
              << nb_not_11_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_11_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "13 not divide n\t"
              << nb_not_13_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_13_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "17 not divide n\t"
              << nb_not_17_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_17_divide*100)/static_cast<double>(bads.size())
              << std::endl;
    std::cout << "19 not divide n\t"
              << nb_not_19_divide << "\t/ " << bads.size() << " ~=\t"
              << static_cast<double>(nb_not_19_divide*100)/static_cast<double>(bads.size())
              << std::endl;
  }

};
