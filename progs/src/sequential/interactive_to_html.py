#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
interactive_to_html.py (January 17, 2018)

Parse the output of ./interactive
and return an HTML format.

GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
http://www.opimedia.be/
"""

import html
import re
import sys


#
# Main
######
def main():
    """
    Main
    """
    skip = 0

    i = 0
    while i < len(sys.argv):
        arg = sys.argv[i]

        if arg == '--skip':
            i += 1
            try:
                skip = int(sys.argv[i])
            except:
                pass

        i += 1

    print("""<!DOCTYPE html>
<html>
<head>
  <title>sigma_odd problem</title>
  <style>
table { border-collapse: collapse; }

table tr.bad { color: red; }
table tr.even { color: silver; }
table tr.gentle {}
table tr.square { color: orange; }

table tr.sep { border-bottom: black 3px solid; }
table tr.sep td { display: none; }

table tr td {
    border: black 1px solid;
    padding-left: 0.5ex;
    white-space: nowrap;
}
  </style>
</head>
<body>""")

    # Skip first lines
    for _ in range(skip):
        sys.stdin.readline()

    print("""  <table>
    <tbody>""")

    sys.stdout.flush()

    kind = ''

    for line in sys.stdin:
        line = line.strip()

        if line.startswith('---'):
            print('<tr class="sep"><td></td></tr>')
        elif line != '':
            for kind_name in ('bad', 'even', 'gentle', 'square'):
                if line.find(kind_name) != -1:
                    kind = kind_name

                    break

            line = html.escape(line)

            line = re.sub(r'\^(\d+)', r'<sup>\1</sup>', line)
            line = line.replace('varsigma_odd(', '&sigmaf;<sub>odd</sub>(')
            line = line.replace('sigma_odd(', '&sigma;<sub>odd</sub>(')
            line = line.replace('sigma(', '&sigma;(')
            line = line.replace('nu_odd(', '&nu;<sub>odd</sub>(')
            line = line.replace('nu(', '&nu;(')

            line = line.replace('*', '<mark>*</mark>')

            line = ('<tr class="{}">\n  <td>{}</td>\n</tr>'
                    .format(kind, line.replace('\t', '</td>\n  <td>')))
            print(line)

        sys.stdout.flush()

    print("""      </tbody>
    </table>
    &rarr; <a href="https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem">&sigma;<sub>odd</sub> problem</a>
  </body>
</html>""")


if __name__ == '__main__':
    main()
