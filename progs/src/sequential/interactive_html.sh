#!/bin/sh

# You can redirect this output to a file,
# open it with Firefox,
# and with the ReloadEvery plugin see automatically each new result.
# https://addons.mozilla.org/en-US/firefox/addon/reloadevery/

./interactive --no-prompt | ./interactive_to_html.py --skip 2
