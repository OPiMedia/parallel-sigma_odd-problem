/* -*- coding: latin-1 -*- */
/** \file sequential/print_path.cpp (January 16, 2018)
 * \brief
 * Check odd gentle numbers for the varsigma_odd problem
 * and print bad numbers.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
// \endcond

#include "../common/helper/helper.hpp"
#include "sequential/sequential.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: print_path [options]" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --complete   check complete path until 1 (by default check only partial path until < n)" << std::endl
            << "  --first n    first odd number to check (3 by default)" << std::endl
            << "  --last  n    last odd number to check (1000001 by default)" << std::endl
            << "  --nb    n    number of odd numbers to check" << std::endl
            << "  --useless    check all odd numbers (by default avoid exactly divisible by 3, 7, 31 or 127)" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  sigmaodd::nat_type first = 3;
  sigmaodd::nat_type last = 1000001;
  bool complete = false;
  bool useless = false;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--complete") {
      complete = true;
    }
    else if (param == "--first") {
      first = std::max(3ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
      if (sigmaodd::is_even(first)) {
        ++first;
      }
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
      if (sigmaodd::is_even(last)) {
        --last;
      }
    }
    else if (param == "--nb") {
      last = first + helper::get_ulong(argc, argv, ++i, &help_and_exit)*2 - 1;
    }
    else if (param == "--useless") {
      useless = true;
    }
    else {
      help_and_exit();
    }
  }


  // Print intern configuration
  helper::print_intern_config_compiler();

  // Print parameters
  std::cout << "sequential/print_path"
            << "\tFirst: " << first
            << "\tLast: " << last
            << "\tNb: " << (first <= last
                            ? (last - first)/2 + 1
                            : 0)
            << "\tComplete?: " << helper::to_string(complete)
            << "\tUseless?: " << helper::to_string(useless)
            << std::endl;

  // Print table legend
  std::cout << std::endl
            << "n\tLower\tPartial length\tPartial path";
  if (complete) {
    std::cout << "\tLength\tPath";
  }
  std::cout << std::endl;
  std::cout.flush();


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  for (sigmaodd::nat_type n = first; n <= last; n += 2) {
    if (complete) {
      sequential::sequential_check_varsigma_odd_complete(n, n, useless, false, true);
    }
    else {
      sequential::sequential_check_varsigma_odd(n, n, false, true);
    }
  }

  // End
  std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

  std::cout << "Total duration: " << helper::duration_to_string(duration)
            << std::endl;

  return EXIT_SUCCESS;
}
