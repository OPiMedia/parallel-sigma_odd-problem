/* -*- coding: latin-1 -*- */
/** \file sequential/check_square.cpp (January 6, 2018)
 * \brief
 * Check odd square numbers for the varsigma_odd problem
 * and print some numbers to show progression.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
// \endcond

#include "../common/helper/helper.hpp"
#include "sequential/sequential.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: check_square [options]" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --complete   check complete path until 1 (by default check only partial path until < n)" << std::endl
            << "  --first n    square of first odd number to check (9 by default)" << std::endl
            << "  --last  n    square of last odd number to check (10000000000 by default)" << std::endl
            << "  --print      print partial or complete path for each number checked (by default only some numbers are printed to show progression)" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  sigmaodd::nat_type first = 9;
  sigmaodd::nat_type last = 10000000000;  // = 10^10 ~= 2^33
  bool complete = false;
  bool print = false;

  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--complete") {
      complete = true;
    }
    else if (param == "--first") {
      first = std::max(9ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
      if (sigmaodd::is_even(first)) {
        ++first;
      }
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
      if (sigmaodd::is_even(last)) {
        --last;
      }
    }
    else if (param == "--print") {
      print = true;
    }
    else {
      help_and_exit();
    }
  }

  const sigmaodd::nat_type sqrt_first
    = (sigmaodd::is_even(sigmaodd::ceil_square_root(first))
       ? sigmaodd::ceil_square_root(first) + 1
       : sigmaodd::ceil_square_root(first));
  const sigmaodd::nat_type sqrt_last
    = (sigmaodd::is_even(sigmaodd::floor_square_root(last))
       ? sigmaodd::floor_square_root(last) - 1
       : sigmaodd::floor_square_root(last));


  // Print intern configuration
  helper::print_intern_config_compiler();

  // Print parameters
  std::cout << "sequential/check_square"
            << "\tFirst: " << first
            << "\tLast: " << last
            << "\tSqrt first: " << sqrt_first
            << "\tSqrt last: " << sqrt_last
            << "\tComplete?: " << helper::to_string(complete)
            << "\tPrint?: " << helper::to_string(print) << std::endl;

  // Print table legend
  std::cout << std::endl
            << "i\ti^2\tLower\tPartial length\tPartial path";
  if (complete) {
    std::cout << "\tLength\tPath";
  }
  std::cout << std::endl;
  std::cout.flush();


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  // Print always the first tried
  {
    const sigmaodd::nat_type n = sigmaodd::square(sqrt_first);

    std::cout << sqrt_first << '\t';
    if (complete) {
      sequential::sequential_check_varsigma_odd_perfect_square_complete(n);
    }
    else {
      sequential::sequential_check_varsigma_odd_perfect_square(n);
    }
  }

  // Check next numbers but print all only if option enabled
  for (sigmaodd::nat_type i = sqrt_first + 2; i <= sqrt_last; i += 2) {
    const sigmaodd::nat_type n = sigmaodd::square(i);

    const bool print_this = print || ((i & 0xFFF) == 1);
    if (print_this) {
      std::cout << i << '\t';
    }

    if (complete) {
      sequential::sequential_check_varsigma_odd_perfect_square_complete(n, print_this);
    }
    else {
      sequential::sequential_check_varsigma_odd_perfect_square(n, print_this);
    }
  }

  // End
  std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

  std::cout << "Total duration: " << helper::duration_to_string(duration)
            << std::endl;

  return EXIT_SUCCESS;
}
