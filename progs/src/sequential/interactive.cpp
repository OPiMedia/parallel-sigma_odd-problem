/* -*- coding: latin-1 -*- */
/** \file sequential/interactive.cpp (January 6, 2018)
 * \brief
 * Read number from stdin
 * and print factorization, number of divisors, sum of divisors
 * and complete path of the varsigma_odd problem.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <algorithm>
#include <iostream>
#include <set>
// \endcond

#include "../common/helper/helper.hpp"
#include "sequential/sequential.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: interactive [options]" << std::endl
            << std::endl
            << "  --no-prompt      do not print the prompt" << std::endl
            << "  --no-separator   do not print the separator" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  bool prompt = true;
  bool separator = true;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--no-prompt") {
      prompt = false;
    }
    else if (param == "--no-separator") {
      separator = false;
    }
    else {
      help_and_exit();
    }
  }


  // Print intern configuration
  helper::print_intern_config_compiler();


  std::cout << "0 or CTRL-D for exit" << std::endl;
  std::cout.flush();


  // Interactive loop
  while (true) {
    // Input
    std::string s;

    if (prompt) {
      std::cout << ">>> n = ";
    }
    std::cin >> s;

    if (std::cin.eof()) {
      break;
    }

    sigmaodd::nat_type n;

    if (!std::all_of(s.cbegin(), s.cend(),
                     [] (int c) { return (('0' <= c) && (c <= '9')); })) {  // invalid character
      continue;
    }

    try {
      n = std::stoull(s);
    }
    catch (...) {
      continue;
    }

    if ((std::to_string(n) != s) || (n > sigmaodd::MAX_POSSIBLE_N)) {
      std::cout << "Too big!" << std::endl;

      continue;
    }

    if (n == 0) {
      break;
    }


    // Factorize and compute
    const sigmaodd::nat_type n_odd = sigmaodd::divide_until_odd(n);
    const std::vector<sigmaodd::FactorExp> prime_exps = sigmaodd::factorize(n);
    const sigmaodd::nat_type nu = sigmaodd::factorization_to_nu(prime_exps);
    const sigmaodd::nat_type nu_odd = sigmaodd::factorization_to_nu_odd(prime_exps);
    const sigmaodd::nat_type sigma = sigmaodd::factorization_to_sigma(prime_exps);
    const sigmaodd::nat_type sigma_odd = sigmaodd::factorization_to_sigma_odd(prime_exps);
    const sigmaodd::nat_type varsigma_odd = sigmaodd::divide_until_odd(sigma_odd);

    const std::set<sigmaodd::nat_type> empty_bad_table;
    const sigmaodd::nat_type sigma_odd_upper_bound
      = sequential::sequential_sigma_odd_upper_bound(n_odd, empty_bad_table, 0);

    // Output
    std::cout << "n = " << n;
    if ((prime_exps.size() > 1)
        || ((prime_exps.size() == 1) && (prime_exps[0].exp > 1))) {
      // Factorization(s)
      if (sigmaodd::is_even(n) && ((prime_exps.size() > 2)
                                   || ((prime_exps.size() == 2) && (prime_exps[1].exp > 1)))) {
        std::cout << " = " << prime_exps[0] << ' ' << n_odd;
      }

      std::cout << " =";
      for (sigmaodd::FactorExp prime_exp : prime_exps) {
        std::cout << ' ' << prime_exp;
      }
    }

    std::cout << '\t';
    if (sigmaodd::is_even(n)) {
      std::cout << "even";
    }
    else if (n == 1) {
      std::cout << "fixed point";
    }
    else if (sigmaodd::is_square(n)) {
      std::cout << "square";
    }
    else if (varsigma_odd > n) {
      std::cout << "bad";
    }
    else {
      std::cout << "gentle";
    }


    // Number of divisors
    std::cout << "\tnu_odd(n) = " << nu_odd;
    if (sigmaodd::is_even(n)) {
      std::cout << " < nu(n) = " << nu;
    }


    // Sum(s) of divisors
    std::cout << "\tvarsigma_odd(n) = " << varsigma_odd
              << ' ' << (varsigma_odd == sigma_odd
                         ? '='
                         : '<')<< " sigma_odd(n) = " << sigma_odd;
    if (sigmaodd::is_even(n)) {
      std::cout << " < sigma(n) = " << sigma;
    }

    std::cout << "\t sigma_odd(n) upper bound = " << sigma_odd_upper_bound
              << std::endl;

    assert(sigma == sigmaodd::sum_divisors__factorize(n));
    assert(sigma_odd == sigmaodd::sum_odd_divisors__factorize(n));

    // Paths
    if (n_odd != 1) {
      sequential::sequential_check_varsigma_odd_complete(n_odd, n_odd, true, true, true);
    }

    if (separator) {
      std::cout << std::string(50, '-') << std::endl;
    }
    std::cout.flush();
  }

  std::cout << std::endl;

  return EXIT_SUCCESS;
}
