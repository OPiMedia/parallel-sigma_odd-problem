/* -*- coding: latin-1 -*- */
/** \file sequential/check_gentle_sequential.cpp (January 6, 2018)
 * \brief
 * Check odd gentle numbers for the varsigma_odd problem
 * and print bad numbers.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <algorithm>
#include <chrono>
#include <iostream>
// \endcond

#include "../common/helper/helper.hpp"
#include "sequential/sequential.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: check_gentle [options]   (sequential version)" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --bad-table filename   load bad numbers from this file" << std::endl
            << "      (If load bad numbers then must be contains all bad numbers < first." << std::endl
            << "       Can load them with several files.)" << std::endl
            << "  --first n              first odd number to check (3 by default)" << std::endl
            << "  --last  n              last odd number to check (1000001 by default)" << std::endl
            << "  --nb    n              number of odd numbers to check" << std::endl
            << "  --no-print             do not print bad numbers" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  std::vector<sigmaodd::nat_type> bad_table;
  sigmaodd::nat_type first = 3;
  sigmaodd::nat_type last = 1000001;
  bool print_bad = true;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--bad-table") {
      std::string filename = helper::get_string(argc, argv, ++i, &help_and_exit);

      if (filename.empty() || !helper::is_file_exists(filename)) {
        std::cerr << "! Failed reading file \"" << filename << '"' << std::endl;
        help_and_exit();
      }

      bad_table = sigmaodd::load_bad_table(filename, bad_table);
      if (bad_table.empty()) {
        std::cerr << "! Wrong format for the bad table file \"" << filename << '"' << std::endl;
        help_and_exit();
      }
    }
    else if (param == "--first") {
      first = std::max(3ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
      if (sigmaodd::is_even(first)) {
        ++first;
      }
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
      if (sigmaodd::is_even(last)) {
        --last;
      }
    }
    else if (param == "--nb") {
      last = first + helper::get_ulong(argc, argv, ++i, &help_and_exit)*2 - 1;
    }
    else if (param == "--no-print") {
      print_bad = false;
    }
    else {
      help_and_exit();
    }
  }

  const std::set<sigmaodd::nat_type> bad_table_set(bad_table.cbegin(), bad_table.cend());

  bad_table.clear();

  const auto minmax = std::minmax_element(bad_table_set.cbegin(), bad_table_set.cend());


  // Print intern configuration
  helper::print_intern_config_compiler();

  // Print parameters
  std::cout << "sequential/check_gentle"
            << "\tFirst: " << first
            << "\tLast: " << last
            << "\tNb: " << (first <= last
                            ? (last - first)/2 + 1
                            : 0);
  if (!bad_table_set.empty()) {
    std::cout << "\t# bad numbers loaded: " << bad_table_set.size()
              << "\tBetween " << *minmax.first
              << "\tand " << *minmax.second << std::endl;
  }
  std::cout << std::endl;

  // Print table legend
  std::cout << std::endl
            << 'n' << std::endl;
  std::cout.flush();


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  if (bad_table_set.empty()) {
    sequential::sequential_check_gentle_varsigma_odd(first, last,
                                                     print_bad);
  }
  else {
    sequential::sequential_check_gentle_varsigma_odd(first, last,
                                                     bad_table_set,
                                                     0, std::max(first - 1, *minmax.second),
                                                     print_bad);
  }

  // End
  const std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

  std::cout << "Total duration: " << helper::duration_to_string(duration)
            << std::endl;

  return EXIT_SUCCESS;
}
