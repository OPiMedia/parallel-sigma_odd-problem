/* -*- coding: latin-1 -*- */
/** \file sequential/check_multiplicativity.cpp (October 6, 2018)
 * \brief
 * varsigma_odd is multiplicative,
 * this program check if there exists some (a, b) *not* coprimes such that there is equality.
 *
 * GPLv3 --- Copyright (C) 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
// \endcond

#include "../common/helper/helper.hpp"
#include "../common/sigmaodd/divisors.hpp"
#include "sequential/sequential.hpp"



/* ***********
 * Constants *
 *************/

const sigmaodd::nat_type first_default = 3;
const sigmaodd::nat_type last_default = 100001;



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: check_multiplicativity [options]" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --first n    first odd number to check (" << first_default << " by default)" << std::endl
            << "  --last  n    last odd number to check (" << last_default << " by default)" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  sigmaodd::nat_type first = first_default;
  sigmaodd::nat_type last = last_default;

  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--first") {
      first = std::max(9ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
      if (sigmaodd::is_even(first)) {
        ++first;
      }
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
      if (sigmaodd::is_even(last)) {
        --last;
      }
    }
    else {
      help_and_exit();
    }
  }


  // Print intern configuration
  helper::print_intern_config_compiler();

  // Print parameters
  std::cout << "sequential/check_multiplicativity"
            << "\tFirst: " << first
            << "\tLast: " << last << std::endl;
  std::cout.flush();


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  // Check if there exists some (a, b) *not* coprimes such that there is equality
  for (sigmaodd::nat_type a = first; a <= last; a += 2) {
    if (a % 1001 == 0) {
      std::cout << a << std::endl;
      std::cout.flush();
    }

    const sigmaodd::nat_type varsigma_odd_a = sequential::sequential_varsigma_odd(a);

    for (sigmaodd::nat_type b = a; b <= last; b += 2) {
      const sigmaodd::nat_type d = sigmaodd::gcd(a, b);

      if (d != 1) {  // a and b *not* coprimes
        const sigmaodd::nat_type varsigma_odd_b = sequential::sequential_varsigma_odd(b);
        const sigmaodd::nat_type varsigma_odd_ab = sequential::sequential_varsigma_odd(a*b);

        if (varsigma_odd_ab == varsigma_odd_a*varsigma_odd_b) {
          std::cout << "Exception found! a b: " << a*b << '=' << a << '*' << b << '\t'
                    << d << '\t'
                    << "varsigma_odd: " << varsigma_odd_ab
                    << '=' << varsigma_odd_a << '*' << varsigma_odd_b << std::endl;
        }
      }
    }
  }

  // End
  std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

  std::cout << "Total duration: " << helper::duration_to_string(duration)
            << std::endl;

  return EXIT_SUCCESS;
}
