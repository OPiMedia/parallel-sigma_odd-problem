/* -*- coding: latin-1 -*- */
/** \file sequential/sequential/sequential__inline.hpp (January 8, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_SEQUENTIAL_SEQUENTIAL_SEQUENTIAL__INLINE_HPP_
#define PROGS_SRC_SEQUENTIAL_SEQUENTIAL_SEQUENTIAL__INLINE_HPP_

// \cond
#include <cassert>

#include <algorithm>
#include <set>
// \endcond

#include "../../common/sigmaodd/helper.hpp"


namespace sequential {

  /* ********************
   * constexpr function *
   **********************/

  constexpr
  nat_type
  sequential_min_array(const nat_type ns[], size_t size) {
    assert(size != 0);

    nat_type min = ns[0];

    for (unsigned int i = 1; i < size; ++i) {
      min = std::min(min, ns[i]);
    }

    return min;
  }


  constexpr
  nat_type
  sequential_sigma_odd_upper_bound(nat_type n,
                                   const std::set<nat_type> &bad_table,
                                   nat_type bad_first_n) {
    assert(sigmaodd::is_odd(n));

    return (n == 1
            ? 1
            : ((bad_first_n <= n) && !sigmaodd::is_square(n)
               && (bad_table.find(n) == bad_table.cend())
               ? n - 1
               : ((n*((sigmaodd::ceil_eighth_root(n - 1) << 1) + 1)) >> 1)));
  }


  constexpr
  nat_type
  sequential_sigma_odd_upper_bound(nat_type n,
                                   const std::set<nat_type> &bad_table,
                                   nat_type bad_first_n, nat_type bad_last_n) {
    assert(sigmaodd::is_odd(n));

    return (n == 1
            ? 1
            : ((bad_first_n <= n) && (n <= bad_last_n) && !sigmaodd::is_square(n)
               && (bad_table.find(n) == bad_table.cend())
               ? n - 1
               : ((n*((sigmaodd::ceil_eighth_root(n - 1) << 1) + 1)) >> 1)));
  }


  constexpr
  nat_type
  sequential_sigma_odd_upper_bound_with_sqrt(nat_type n,
                                             const std::set<nat_type> &bad_table,
                                             nat_type bad_first_n,
                                             nat_type sqrt_n) {
    assert(sigmaodd::is_odd(n));
    // assert(sqrt_n == sigmaodd::floor_square_root(n));

    return (n == 1
            ? 1
            : ((bad_first_n <= n) && (sigmaodd::square(sqrt_n) != n)
               && (bad_table.find(n) == bad_table.cend())
               ? n - 1
               : ((n*((sigmaodd::ceil_eighth_root(n - 1) << 1) + 1)) >> 1)));
  }


  constexpr
  nat_type
  sequential_sigma_odd_upper_bound_with_sqrt(nat_type n,
                                             const std::set<nat_type> &bad_table,
                                             nat_type bad_first_n, nat_type bad_last_n,
                                             nat_type sqrt_n) {
    assert(sigmaodd::is_odd(n));
    // assert(sqrt_n == sigmaodd::floor_square_root(n));

    return (n == 1
            ? 1
            : ((bad_first_n <= n) && (n <= bad_last_n) && (sigmaodd::square(sqrt_n) != n)
               && (bad_table.find(n) == bad_table.cend())
               ? n - 1
               : ((n*((sigmaodd::ceil_eighth_root(n - 1) << 1) + 1)) >> 1)));
  }

}  // namespace sequential

#endif  // PROGS_SRC_SEQUENTIAL_SEQUENTIAL_SEQUENTIAL__INLINE_HPP_
