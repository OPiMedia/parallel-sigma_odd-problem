/* -*- coding: latin-1 -*- */
/** \file sequential/sequential/sequential.cpp (February 2nd, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <iostream>
// \endcond

#include "../../common/sigmaodd/divisors.hpp"
#include "../../common/sigmaodd/sigmaodd.hpp"

#include "sequential.hpp"


namespace sequential {

  /* ***********
   * Functions *
   *************/

  std::set<nat_type>
  sequential_check_gentle_varsigma_odd(nat_type first_n, nat_type last_n,
                                       bool print_bad) {
    assert(sigmaodd::is_odd(first_n));
    assert(3 <= first_n);
    assert(first_n <= last_n);
    assert(last_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(last_n < 65536);
#endif

    std::set<nat_type> bad_table;

    for (nat_type n = first_n; n <= last_n; n += 2) {
      if (!sigmaodd::is_first_mersenne_prime_unitary_divide_or_square(n)
          && !sequential_is_varsigma_odd_lower(n,
                                               bad_table,
                                               first_n)) {  // bad number identified
        bad_table.insert(n);
        if (print_bad) {
          std::cout << n << std::endl;
        }
      }
    }
    if (print_bad) {
      std::cout.flush();
    }

    return bad_table;
  }


  std::set<nat_type>
  sequential_check_gentle_varsigma_odd(nat_type first_n, nat_type last_n,
                                       const std::set<nat_type> &bad_table,
                                       nat_type bad_first_n, nat_type bad_last_n,
                                       bool print_bad) {
    assert(sigmaodd::is_odd(first_n));
    assert(3 <= first_n);
    assert(first_n <= last_n);
    assert(last_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(last_n < 65536);
#endif

    std::set<nat_type> new_bad_table;

    for (nat_type n = first_n; n <= last_n; n += 2) {
      if (!sigmaodd::is_first_mersenne_prime_unitary_divide_or_square(n)
          && !sequential_is_varsigma_odd_lower(n,
                                               bad_table,
                                               bad_first_n, bad_last_n)) {  // bad number identified
        new_bad_table.insert(n);
        if (print_bad) {
          std::cout << n << std::endl;
        }
      }
    }
    if (print_bad) {
      std::cout.flush();
    }

    return new_bad_table;
  }


  void
  sequential_check_varsigma_odd(nat_type first_n, nat_type last_n,
                                bool print_bad,
                                bool print_all,
                                bool print_category,
                                bool print_lower,
                                bool print_length,
                                bool print_path) {
    assert(sigmaodd::is_odd(first_n));
    assert(3 <= first_n);
    assert(first_n <= last_n);
    assert(last_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(last_n < 65536);
#endif

    for (nat_type n = first_n; n <= last_n; n += 2) {
      if (!sigmaodd::is_first_mersenne_prime_unitary_divide(n)) {  // not useless to check
        const std::vector<nat_type> path = sequential_iterate_varsigma_odd_until_lower(n);

        sigmaodd::print_path_infos(path,
                                   print_bad, print_all,
                                   print_category,
                                   print_lower, print_length, print_path);
      }
    }
  }


  void
  sequential_check_varsigma_odd_perfect_square(nat_type n,
                                               bool print,
                                               bool print_lower,
                                               bool print_length,
                                               bool print_path) {
    assert(sigmaodd::is_odd(n));
    assert(sigmaodd::is_square(n));
    assert(3 <= n);
    assert(n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(n < 65536);
#endif

    const std::vector<nat_type> path
      = sequential_iterate_varsigma_odd_perfect_square_until_lower(n);

    sigmaodd::print_path_infos(path,
                               print, print,
                               false,
                               print_lower, print_length, print_path);
  }


  void
  sequential_check_varsigma_odd_complete(nat_type first_n, nat_type last_n,
                                         bool check_useless,
                                         bool print_bad,
                                         bool print_all,
                                         bool print_category,
                                         bool print_lower,
                                         bool print_length,
                                         bool print_path) {
    assert(sigmaodd::is_odd(first_n));
    assert(3 <= first_n);
    assert(first_n <= last_n);
    assert(last_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(last_n < 65536);
#endif

    for (nat_type n = first_n; n <= last_n; n += 2) {
      if (check_useless ||
          !sigmaodd::is_first_mersenne_prime_unitary_divide(n)) {  // not useless to check
        const std::vector<nat_type> path = sequential_iterate_varsigma_odd_until_1(n);

        sigmaodd::print_path_infos(path,
                                   print_bad, print_all,
                                   print_category,
                                   print_lower, print_length, print_path);
      }
    }
  }


  void
  sequential_check_varsigma_odd_perfect_square_complete(nat_type n,
                                                        bool print,
                                                        bool print_lower,
                                                        bool print_length,
                                                        bool print_path) {
    assert(sigmaodd::is_odd(n));
    assert(sigmaodd::is_square(n));
    assert(3 <= n);
    assert(n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(n < 65536);
#endif

    const std::vector<nat_type> path = sequential_iterate_varsigma_odd_perfect_square_until_1(n);

    sigmaodd::print_path_infos(path,
                               print, print,
                               false,
                               print_lower, print_length, print_path);
  }


  bool
  sequential_is_varsigma_odd_lower(nat_type n,
                                   const std::set<nat_type> &bad_table,
                                   nat_type bad_first_n) {
    assert(sigmaodd::is_odd(n));
    assert(3 <= n);
    assert(n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(n < 65536);
#endif

    assert(sigmaodd::array_odd_primes_ != nullptr);

    nat_type n_divided = n;
    nat_type sqrt_n_divided = sigmaodd::floor_square_root(n_divided);
    nat_type varsigma_odd = 1;
    const prime_type* prime_ptr = sigmaodd::odd_primes_table_ptr() - 1;
    nat_type prime;

    while ((prime = *(++prime_ptr)) <= sqrt_n_divided) {
      assert(prime != 0);

      unsigned int alpha = 0;

      while (sigmaodd::is_divide(prime, n_divided)) {
        n_divided /= prime;
        ++alpha;
      }

      if (alpha > 0) {
        varsigma_odd
          *= sigmaodd::divide_until_odd(sigmaodd::sum_geometric_progression_strict(prime, alpha));

        sqrt_n_divided = sigmaodd::floor_square_root(n_divided);
        if (varsigma_odd
            * sequential_sigma_odd_upper_bound_with_sqrt(n_divided, bad_table, bad_first_n,
                                                         sqrt_n_divided) < n) {
          return true;
        }
      }
    }

    // Remain n_divided is prime
    if (n_divided > 1) {
      varsigma_odd *= sigmaodd::divide_until_odd(n_divided + 1);
    }

    return (varsigma_odd < n);
  }


  bool
  sequential_is_varsigma_odd_lower(nat_type n,
                                   const std::set<nat_type> &bad_table,
                                   nat_type bad_first_n, nat_type bad_last_n) {
    assert(sigmaodd::is_odd(n));
    assert(3 <= n);
    assert(n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(n < 65536);
#endif

    assert(sigmaodd::array_odd_primes_ != nullptr);

    nat_type n_divided = n;
    nat_type sqrt_n_divided = sigmaodd::floor_square_root(n_divided);
    nat_type varsigma_odd = 1;
    const prime_type* prime_ptr = sigmaodd::odd_primes_table_ptr() - 1;
    nat_type prime;

    while ((prime = *(++prime_ptr)) <= sqrt_n_divided) {
      assert(prime != 0);

      unsigned int alpha = 0;

      while (sigmaodd::is_divide(prime, n_divided)) {
        n_divided /= prime;
        ++alpha;
      }

      if (alpha > 0) {
        varsigma_odd
          *= sigmaodd::divide_until_odd(sigmaodd::sum_geometric_progression_strict(prime, alpha));

        sqrt_n_divided = sigmaodd::floor_square_root(n_divided);
        if (varsigma_odd
            * sequential_sigma_odd_upper_bound_with_sqrt(n_divided, bad_table, bad_first_n, bad_last_n,
                                                         sqrt_n_divided) < n) {
          return true;
        }
      }
    }

    // Remain n_divided is prime
    if (n_divided > 1) {
      varsigma_odd *= sigmaodd::divide_until_odd(n_divided + 1);
    }

    return (varsigma_odd < n);
  }


  std::vector<nat_type>
  sequential_iterate_varsigma_odd_until_1(nat_type start_n) {
    assert(sigmaodd::is_odd(start_n));
    assert(3 <= start_n);
    assert(start_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(start_n < 65536);
#endif

    nat_type n = start_n;
    std::set<nat_type> already;
    std::vector<nat_type> path;

    already.insert(n);
    path.push_back(n);

    do {
      n = sequential_varsigma_odd(n);
      if (already.find(n) != already.end()) {
        std::cout << "! Found not trivial cycle ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }
      else if (n > sigmaodd::MAX_POSSIBLE_N) {
        std::cout << "! Impossible to check ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }

      already.insert(n);
      path.push_back(n);
    } while (n > 1);

    return path;
  }


  std::vector<nat_type>
  sequential_iterate_varsigma_odd_perfect_square_until_1(nat_type start_n) {
    assert(sigmaodd::is_odd(start_n));
    assert(sigmaodd::is_square(start_n));
    assert(3 <= start_n);
    assert(start_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(start_n < 65536);
#endif

    nat_type n = start_n;
    std::set<nat_type> already;
    std::vector<nat_type> path;

    already.insert(n);
    path.push_back(n);

    n = sequential_varsigma_odd_perfect_square(n);
    already.insert(n);
    path.push_back(n);

    do {
      if (n > sigmaodd::MAX_POSSIBLE_N) {
        std::cout << "! Impossible to check ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }

      n = sequential_varsigma_odd(n);
      if (already.find(n) != already.end()) {
        std::cout << "! Found not trivial cycle ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }

      already.insert(n);
      path.push_back(n);
    } while (n > 1);

    return path;
  }


  std::vector<nat_type>
  sequential_iterate_varsigma_odd_until_lower(nat_type start_n) {
    assert(sigmaodd::is_odd(start_n));
    assert(3 <= start_n);
    assert(start_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(start_n < 65536);
#endif

    nat_type n = start_n;
    std::set<nat_type> already;
    std::vector<nat_type> path;

    already.insert(n);
    path.push_back(n);

    do {
      n = sequential_varsigma_odd(n);
      if (already.find(n) != already.end()) {
        std::cout << "! Found not trivial cycle ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }
      else if (n > sigmaodd::MAX_POSSIBLE_N) {
        std::cout << "! Impossible to check ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }

      already.insert(n);
      path.push_back(n);
    } while (n > start_n);

    return path;
  }


  std::vector<nat_type>
  sequential_iterate_varsigma_odd_perfect_square_until_lower(nat_type start_n) {
    assert(sigmaodd::is_odd(start_n));
    assert(sigmaodd::is_square(start_n));
    assert(3 <= start_n);
    assert(start_n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(start_n < 65536);
#endif

    nat_type n = start_n;
    std::set<nat_type> already;
    std::vector<nat_type> path;

    already.insert(n);
    path.push_back(n);

    n = sequential_varsigma_odd_perfect_square(n);
    already.insert(n);
    path.push_back(n);

    do {
      if (n > sigmaodd::MAX_POSSIBLE_N) {
        std::cout << "! Impossible to check ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }

      n = sequential_varsigma_odd(n);
      if (already.find(n) != already.end()) {
        std::cout << "! Found not trivial cycle ";
        sigmaodd::print_path(path);
        std::cout << std::endl;

        exit(EXIT_FAILURE);
      }

      already.insert(n);
      path.push_back(n);
    } while (n > start_n);

    return path;
  }


  std::vector<nat_type>
  sequential_print_in_order(const std::set<nat_type> &ns) {
    std::vector<nat_type> list;

    list.insert(list.begin(), ns.cbegin(), ns.cend());
    sort(list.begin(), list.end());
    for (nat_type n : ns) {
      std::cout << n << std::endl;
    }

    return list;
  }


  nat_type
  sequential_varsigma_odd(nat_type n) {
    assert(sigmaodd::is_odd(n));
    assert(n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(n < 65536);
#endif

    assert(sigmaodd::array_odd_primes_ != nullptr);

    nat_type n_divided = n;
    nat_type sqrt_n_divided = sigmaodd::floor_square_root(n_divided);
    nat_type varsigma_odd = 1;
    const prime_type* prime_ptr = sigmaodd::odd_primes_table_ptr() - 1;
    nat_type prime;

    while ((prime = *(++prime_ptr)) <= sqrt_n_divided) {
      assert(prime != 0);

      unsigned int alpha = 0;

      while (sigmaodd::is_divide(prime, n_divided)) {
        n_divided /= prime;
        ++alpha;
      }

      if (alpha > 0) {
        varsigma_odd
          *= sigmaodd::divide_until_odd(sigmaodd::sum_geometric_progression_strict(prime, alpha));
        sqrt_n_divided = sigmaodd::floor_square_root(n_divided);
      }
    }

    // Remain n_divided is prime
    if (n_divided > 1) {
      varsigma_odd *= sigmaodd::divide_until_odd(n_divided + 1);
    }

    return varsigma_odd;
  }


  nat_type
  sequential_varsigma_odd_perfect_square(nat_type n) {
    assert(sigmaodd::is_odd(n));
    assert(sigmaodd::is_square(n));
    assert(n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
    assert(n < 65536);
#endif

    assert(sigmaodd::array_odd_primes_ != nullptr);

    nat_type n_divided = n;
    nat_type sqrt4_n = sigmaodd::floor_fourth_root(n_divided);
    nat_type varsigma_odd = 1;
    const prime_type* p = sigmaodd::odd_primes_table_ptr() - 1;

    while (*(++p) <= sqrt4_n) {
      assert(*p != 0);

      unsigned int alpha = 0;

      while (sigmaodd::is_divide(*p, n_divided)) {
        assert(sigmaodd::is_divide((*p)*(*p), n_divided));

        n_divided /= sigmaodd::square(static_cast<nat_type>(*p));
        alpha += 2;
      }

      if (alpha > 0) {
        sqrt4_n = sigmaodd::floor_fourth_root(n);
        varsigma_odd
          *= sigmaodd::divide_until_odd(sigmaodd::sum_geometric_progression_strict(*p, alpha));
      }
    }

    // Remain n_divided is an even power of a prime
    if (n_divided > 1) {
      unsigned int alpha = 0;

      do {
        alpha += 2;
        n_divided = sigmaodd::floor_square_root(n_divided);
      } while (sigmaodd::is_square(n_divided));

      varsigma_odd
        *= sigmaodd::divide_until_odd(sigmaodd::sum_geometric_progression_strict(n_divided, alpha));
    }

    return varsigma_odd;
  }


  std::set<nat_type>
  sequential_varsigma_odd_greater_set(const std::vector<nat_type> &ns,
                                      const std::set<nat_type> &bad_table,
                                      nat_type bad_first_n, nat_type bad_last_n) {
    assert(sigmaodd::array_odd_primes_ != nullptr);
    std::set<nat_type> new_bad_table;

    for (nat_type n : ns) {
      assert(sigmaodd::is_odd(n));
      assert(3 <= n);
      assert(n <= sigmaodd::MAX_POSSIBLE_N);
#ifdef PRIME16
      assert(n < 65536);
#endif

      if (!sequential_is_varsigma_odd_lower(n, bad_table, bad_first_n, bad_last_n)) {
        new_bad_table.insert(n);
      }
    }

    return new_bad_table;
  }

}  // namespace sequential
