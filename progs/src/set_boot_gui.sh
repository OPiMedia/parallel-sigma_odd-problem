#!/bin/sh

echo "Set configuration system to boot to the normal GUI mode."
echo "Require systemd and admin authentication."
echo "Only tested on Debian GNU/Linux 9 (Stretch)"

systemctl set-default graphical.target
