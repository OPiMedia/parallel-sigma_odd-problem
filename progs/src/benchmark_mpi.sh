#!/bin/sh

LAST=$1


cd mpi

echo === MPI/one by one ===
for i in 1 2 3 4 5 6
do
    mpirun -n $i ./check_gentle_mpi --last $LAST --no-print --one-by-one
    echo ------
done

echo === MPI/dynamic ===
for i in 1 2 3 4 5 6
do
    mpirun -n $i ./check_gentle_mpi --last $LAST --no-print --dynamic
    echo ------
done

cd ..
