# sigma\_odd — `progs/src/common/`

`>>> make clang all`

To compile library and programs in debug mode.
Specify `clang` to use the [clang](https://clang.llvm.org/) compiler.
By default [GCC](https://gcc.gnu.org/) is used.



(Not exhaustive) options:

  - `>>> make distclean`

    To remove programs and compiled files.

  - `>>> make ndebug clang all`

    To compile library and programs without assertions and with optimizations.

  - `>>> ./sigmaoddprob --help`

    To print options for this program.

The code of functions marked constexpr or inline are in the \*\_\_inline.hpp files.
