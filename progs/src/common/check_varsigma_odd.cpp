/* -*- coding: latin-1 -*- */
/** \file common/check_varsigma_odd.cpp (January 5, 2018)
 * \brief
 * Check odd numbers for the varsigma_odd problem
 * and print bad and perfect square numbers.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
// \endcond

#include "helper/helper.hpp"
#include "sigmaodd/primes.hpp"
#include "sigmaodd/sigmaodd.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: check_varsigma_odd [options]" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --first n   first odd number to check (3 by default)" << std::endl
            << "  --last  n   last odd number to check (101 by default)" << std::endl
            << "  --nb    n   number of odd numbers to check" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  sigmaodd::nat_type first = 3;
  sigmaodd::nat_type last = 101;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--first") {
      first = std::max(3ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
      if (sigmaodd::is_even(first)) {
        ++first;
      }
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
      if (sigmaodd::is_even(last)) {
        --last;
      }
    }
    else if (param == "--nb") {
      last = first + helper::get_ulong(argc, argv, ++i, &help_and_exit)*2 - 1;
    }
    else {
      help_and_exit();
    }
  }


  // Print intern configuration
  helper::print_intern_config_compiler();

  // Print parameters
  std::cout << "First: " << first
            << "\tLast: " << last
            << "\tNb: " << (first <= last
                            ? (last - first)/2 + 1
                            : 0)
            << std::endl;

  // Print table legend
  std::cout << std::endl
            << "n\tLower\tLength" << std::endl;
  std::cout.flush();


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  sigmaodd::check_varsigma_odd(first, last);

  // End
  std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

  std::cout << "Total duration: " << helper::duration_to_string(duration)
            << std::endl;

  return EXIT_SUCCESS;
}
