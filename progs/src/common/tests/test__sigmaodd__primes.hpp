/* -*- coding: latin-1 -*- */
/** \file common/tests/test__sigmaodd__primes.hpp (January 5, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <algorithm>

#include "test_helper.hpp"

#include "../sigmaodd/primes.hpp"
#include "../sigmaodd/divisors.hpp"


using namespace sigmaodd;


class Test__sigmaodd__primes : public CxxTest::TestSuite {
public:
  void test__read_primes_table() {
    SHOW_FUNC_NAME;

    // The prime table contains numbers on 32 bits stored in little-endian format
    {
      uint32_t n = (1
                    + 3*(static_cast<uint32_t>(1) << 8)
                    + 5*(static_cast<uint32_t>(1) << 16)
                    + 7*(static_cast<uint32_t>(1) << 24));
      unsigned char* ptr = reinterpret_cast<unsigned char*>(&n);

      TS_ASSERT_EQUALS(n, 117768961u);

      TS_ASSERT_EQUALS(ptr[0], 1);
      TS_ASSERT_EQUALS(ptr[1], 3);
      TS_ASSERT_EQUALS(ptr[2], 5);
      TS_ASSERT_EQUALS(ptr[3], 7);
    }

    TS_ASSERT(read_primes_table());

    TS_ASSERT_DIFFERS(array_odd_primes_, nullptr);
  }


  void test__factorize() {
    SHOW_FUNC_NAME;

    std::vector<FactorExp> list = factorize(1);

    TS_ASSERT(list.empty());

    list = factorize(2*2*2*13*127*127);
    TS_ASSERT_EQUALS(list.size(), 3);
    TS_ASSERT_EQUALS(list[0].factor, 2);
    TS_ASSERT_EQUALS(list[0].exp, 3);
    TS_ASSERT_EQUALS(list[1].factor, 13);
    TS_ASSERT_EQUALS(list[1].exp, 1);
    TS_ASSERT_EQUALS(list[2].factor, 127);
    TS_ASSERT_EQUALS(list[2].exp, 2);

    for (unsigned int i = 0; i < 10000; ++i) {
      const nat_type prime = odd_primes_table_by_index(i);

      list = factorize(prime);
      TS_ASSERT_EQUALS(list.size(), 1);
      TS_ASSERT_EQUALS(list[0].factor, prime);
      TS_ASSERT_EQUALS(list[0].exp, 1);
    }

    for (unsigned int n = 1; n < 1000000; ++n) {
      nat_type prod = 1;
      nat_type sum = 1;

      list = factorize(n);
      for (FactorExp p_exp : list) {
        prod *= pow_nat(p_exp.factor, p_exp.exp);
        sum *= sum_geometric_progression(p_exp.factor, p_exp.exp);
      }

      TS_ASSERT_EQUALS(prod, n);
      TS_ASSERT_EQUALS(sum, sum_divisors__factorize(n));
    }
  }


  void test__factorization_to_n() {
    SHOW_FUNC_NAME;

    for (unsigned int n = 1; n < 1000000; ++n) {
      std::vector<FactorExp> list = factorize(n);

      TS_ASSERT_EQUALS(factorization_to_n(list), n);
    }
  }


  void test__factorization_to_nu() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__factorization_to_nu_odd() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__factorization_to_sigma() {
    SHOW_FUNC_NAME;

    for (unsigned int n = 1; n < 1000000; ++n) {
      std::vector<FactorExp> list = factorize(n);

      TS_ASSERT_EQUALS(factorization_to_sigma(list), sum_divisors__factorize(n));
    }
  }


  void test__factorization_to_sigma_odd() {
    SHOW_FUNC_NAME;

    for (unsigned int n = 1; n < 1000000; ++n) {
      std::vector<FactorExp> list = factorize(n);

      TS_ASSERT_EQUALS(factorization_to_sigma_odd(list), sum_odd_divisors__factorize(n));
    }
  }


  void test__is_prime() {
    SHOW_FUNC_NAME;

    TS_ASSERT(is_prime(2));
    TS_ASSERT(is_prime(3));
    TS_ASSERT(is_prime(5));
    TS_ASSERT(is_prime(7));
    TS_ASSERT(is_prime(11));
    TS_ASSERT(is_prime(13));
    TS_ASSERT(is_prime(65537));
    TS_ASSERT(is_prime(65539));
    TS_ASSERT(is_prime(65543));
    TS_ASSERT(is_prime(65551));
    TS_ASSERT(is_prime(65557));
    TS_ASSERT(is_prime(65563));
    TS_ASSERT(is_prime(65579));
    TS_ASSERT(is_prime(65581));

    TS_ASSERT(is_prime(4294967197));
    TS_ASSERT(is_prime(4294967231));
    TS_ASSERT(is_prime(4294967279));
    TS_ASSERT(is_prime(4294967291));
    TS_ASSERT(is_prime(4294967311));

    TS_ASSERT(is_prime(odd_primes_table_last()));

    TS_ASSERT(!is_prime(0));
    TS_ASSERT(!is_prime(1));
    TS_ASSERT(!is_prime(4));
    TS_ASSERT(!is_prime(6));
    TS_ASSERT(!is_prime(8));
    TS_ASSERT(!is_prime(9));
    TS_ASSERT(!is_prime(10));
    TS_ASSERT(!is_prime(12));
    TS_ASSERT(!is_prime(14));

    TS_ASSERT(!is_prime(65535));
    TS_ASSERT(!is_prime(65541));
    TS_ASSERT(!is_prime(65545));
    TS_ASSERT(!is_prime(65547));
    TS_ASSERT(!is_prime(65549));
    TS_ASSERT(!is_prime(65553));
    TS_ASSERT(!is_prime(65555));
    TS_ASSERT(!is_prime(65559));
    TS_ASSERT(!is_prime(65561));
    TS_ASSERT(!is_prime(65565));
    TS_ASSERT(!is_prime(65567));
    TS_ASSERT(!is_prime(65569));
    TS_ASSERT(!is_prime(65571));
    TS_ASSERT(!is_prime(65573));
    TS_ASSERT(!is_prime(65575));
    TS_ASSERT(!is_prime(65577));
    TS_ASSERT(!is_prime(65583));

#ifdef PRIME16
    for (unsigned int i = 0; i < odd_primes_table_nb(); ++i) {
#else
    for (unsigned int i = 0; i < odd_primes_table_nb()/1000; ++i) {
#endif
      const nat_type prime = odd_primes_table_by_index(i);

      TS_ASSERT(is_prime(prime));
#ifdef PRIME16
      for (nat_type n = prime + prime; n < 10000000; n += prime) {
        TS_ASSERT(!is_prime(n));
      }
#endif
    }

    for (nat_type n = 8; n < 1000000; ++n) {
      if (is_even(n) || is_divide(3, n) || is_divide(5, n) || is_divide(7, n)) {
        TS_ASSERT(!is_prime(n));
      }

      TS_ASSERT_EQUALS(is_prime(n), (first_divisor(n) == n));
    }
  }


  void test__is_prime_in_odd_primes_table() {
    SHOW_FUNC_NAME;

    TS_ASSERT(is_prime_in_odd_primes_table(3));
    TS_ASSERT(is_prime_in_odd_primes_table(5));
    TS_ASSERT(is_prime_in_odd_primes_table(7));
    TS_ASSERT(is_prime_in_odd_primes_table(11));
    TS_ASSERT(is_prime_in_odd_primes_table(13));
    TS_ASSERT(is_prime_in_odd_primes_table(odd_primes_table_last()));

    TS_ASSERT(!is_prime_in_odd_primes_table(2));

#ifdef PRIME16
    TS_ASSERT(!is_prime_in_odd_primes_table(65537));
    TS_ASSERT(!is_prime_in_odd_primes_table(65539));
    TS_ASSERT(!is_prime_in_odd_primes_table(65543));
    TS_ASSERT(!is_prime_in_odd_primes_table(65551));
    TS_ASSERT(!is_prime_in_odd_primes_table(65557));
    TS_ASSERT(!is_prime_in_odd_primes_table(65563));
    TS_ASSERT(!is_prime_in_odd_primes_table(65579));
    TS_ASSERT(!is_prime_in_odd_primes_table(65581));
#else
    TS_ASSERT(is_prime_in_odd_primes_table(65537));
    TS_ASSERT(is_prime_in_odd_primes_table(65539));
    TS_ASSERT(is_prime_in_odd_primes_table(65543));
    TS_ASSERT(is_prime_in_odd_primes_table(65551));
    TS_ASSERT(is_prime_in_odd_primes_table(65557));
    TS_ASSERT(is_prime_in_odd_primes_table(65563));
    TS_ASSERT(is_prime_in_odd_primes_table(65579));
    TS_ASSERT(is_prime_in_odd_primes_table(65581));

    TS_ASSERT(is_prime_in_odd_primes_table(268435367u));
    TS_ASSERT(is_prime_in_odd_primes_table(268435399u));

    if (odd_primes_table_nb() >= 14630843u) {
      TS_ASSERT(is_prime_in_odd_primes_table(268435459u));

      TS_ASSERT(is_prime_in_odd_primes_table(4294967197u));
      TS_ASSERT(is_prime_in_odd_primes_table(4294967231u));
      TS_ASSERT(is_prime_in_odd_primes_table(4294967279u));
      TS_ASSERT(is_prime_in_odd_primes_table(4294967291u));
    }
    else {
      TS_ASSERT(!is_prime_in_odd_primes_table(268435459));

      TS_ASSERT(!is_prime_in_odd_primes_table(4294967197u));
      TS_ASSERT(!is_prime_in_odd_primes_table(4294967231u));
      TS_ASSERT(!is_prime_in_odd_primes_table(4294967279u));
      TS_ASSERT(!is_prime_in_odd_primes_table(4294967291u));
    }
    TS_ASSERT(!is_prime_in_odd_primes_table(4294967311u));
#endif

    TS_ASSERT(!is_prime_in_odd_primes_table(0));
    TS_ASSERT(!is_prime_in_odd_primes_table(1));
    TS_ASSERT(!is_prime_in_odd_primes_table(4));
    TS_ASSERT(!is_prime_in_odd_primes_table(6));
    TS_ASSERT(!is_prime_in_odd_primes_table(8));
    TS_ASSERT(!is_prime_in_odd_primes_table(9));
    TS_ASSERT(!is_prime_in_odd_primes_table(10));
    TS_ASSERT(!is_prime_in_odd_primes_table(12));
    TS_ASSERT(!is_prime_in_odd_primes_table(14));

    TS_ASSERT(!is_prime_in_odd_primes_table(65535));
    TS_ASSERT(!is_prime_in_odd_primes_table(65541));
    TS_ASSERT(!is_prime_in_odd_primes_table(65545));
    TS_ASSERT(!is_prime_in_odd_primes_table(65547));
    TS_ASSERT(!is_prime_in_odd_primes_table(65549));
    TS_ASSERT(!is_prime_in_odd_primes_table(65553));
    TS_ASSERT(!is_prime_in_odd_primes_table(65555));
    TS_ASSERT(!is_prime_in_odd_primes_table(65559));
    TS_ASSERT(!is_prime_in_odd_primes_table(65561));
    TS_ASSERT(!is_prime_in_odd_primes_table(65565));
    TS_ASSERT(!is_prime_in_odd_primes_table(65567));
    TS_ASSERT(!is_prime_in_odd_primes_table(65569));
    TS_ASSERT(!is_prime_in_odd_primes_table(65571));
    TS_ASSERT(!is_prime_in_odd_primes_table(65573));
    TS_ASSERT(!is_prime_in_odd_primes_table(65575));
    TS_ASSERT(!is_prime_in_odd_primes_table(65577));
    TS_ASSERT(!is_prime_in_odd_primes_table(65583));

    TS_ASSERT(!is_prime_in_odd_primes_table(268435365u));
    TS_ASSERT(!is_prime_in_odd_primes_table(268435369u));

    TS_ASSERT(!is_prime_in_odd_primes_table(268435397u));
    TS_ASSERT(!is_prime_in_odd_primes_table(268435401u));

    TS_ASSERT(!is_prime_in_odd_primes_table(268435457u));
    TS_ASSERT(!is_prime_in_odd_primes_table(268435461u));

#ifdef PRIME16
    for (unsigned int i = 0; i < odd_primes_table_nb(); ++i) {
#else
    for (unsigned int i = 0; i < odd_primes_table_nb()/1000; ++i) {
#endif
      const nat_type prime = odd_primes_table_by_index(i);

      TS_ASSERT(is_prime_in_odd_primes_table(prime));
#ifdef PRIME16
      for (nat_type n = prime + prime; n < 10000000; n += prime) {
        TS_ASSERT(!is_prime_in_odd_primes_table(n));
      }
#endif
    }

    for (nat_type n = 8; n < 1000000; ++n) {
      if (is_even(n) || is_divide(3, n) || is_divide(5, n) || is_divide(7, n)) {
        TS_ASSERT(!is_prime_in_odd_primes_table(n));
      }

      if (n <= odd_primes_table_last()) {
        TS_ASSERT_EQUALS(is_prime_in_odd_primes_table(n), (first_divisor(n) == n));
      }
      else {
        TS_ASSERT(!is_prime_in_odd_primes_table(n));
      }
    }
  }


  void test__odd_primes_table_by_index() {
    SHOW_FUNC_NAME;

    // First odd prime numbers
    TS_ASSERT_EQUALS(odd_primes_table_by_index(0), 3);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(1), 5);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(2), 7);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(3), 11);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(4), 13);

    // Last odd prime numbers < 2^16
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6538), 65497);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6539), 65519);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6540), 65521);

#ifndef PRIME16
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6541), 65537);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6542), 65539);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6543), 65543);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6544), 65551);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6545), 65557);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6546), 65563);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6547), 65579);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(6548), 65581);

    if (odd_primes_table_nb() >= 14630843u) {
      // Last odd prime numbers < 2^32
      TS_ASSERT_EQUALS(odd_primes_table_by_index(203280217u), 4294967231);
      TS_ASSERT_EQUALS(odd_primes_table_by_index(203280218u), 4294967279);
      TS_ASSERT_EQUALS(odd_primes_table_by_index(203280219u), 4294967291);
    }
    else {
      // Last odd prime numbers < 2^28
      TS_ASSERT_EQUALS(odd_primes_table_by_index(14630839u), 268435361);
      TS_ASSERT_EQUALS(odd_primes_table_by_index(14630840u), 268435367);
      TS_ASSERT_EQUALS(odd_primes_table_by_index(14630841u), 268435399);
    }
#endif

    for (unsigned int i = 1; i < odd_primes_table_nb(); ++i) {
      const nat_type p = odd_primes_table_by_index(i);

      TS_ASSERT(is_odd(p));  // odd

      TS_ASSERT_LESS_THAN(odd_primes_table_by_index(i - 1), p);  // strictly increase

#ifdef PRIME16
      // Each is prime
      for (nat_type d = 2; d < odd_primes_table_by_index(i); ++d) {
        TS_ASSERT(!is_divide(d, odd_primes_table_by_index(i)));
      }
#endif

      // Each is prime
      const nat_type until = floor_square_root(p);

      for (unsigned int j = 0; odd_primes_table_by_index(j) <= until; ++j) {
        TS_ASSERT(!is_divide(odd_primes_table_by_index(j), p));
      }
    }
  }


  void test__odd_primes_table_last() {
    SHOW_FUNC_NAME;

    // Last prime number, https://primes.utm.edu/nthprime/
#ifdef PRIME16
    TS_ASSERT_EQUALS(odd_primes_table_last(), 65521);  // last on 16 bits
#else
    TS_ASSERT_EQUALS(odd_primes_table_last(), 268435399u);  // last on 28 bits
    // TS_ASSERT_EQUALS(odd_primes_table_last(), 4294967291u);  // last on 32 bits
#endif
    TS_ASSERT_EQUALS(odd_primes_table_last(),
                     odd_primes_table_by_index(odd_primes_table_nb() - 1));
  }


  void test__odd_primes_table_ptr() {
    SHOW_FUNC_NAME;

    TS_ASSERT_DIFFERS(odd_primes_table_ptr(), nullptr);
    TS_ASSERT_EQUALS(odd_primes_table_ptr(), array_odd_primes_);
  }


  void test__odd_primes_table_nb() {
    SHOW_FUNC_NAME;

    // Number of all odd prime numbers, https://primes.utm.edu/nthprime/
#ifdef PRIME16
    TS_ASSERT_EQUALS(odd_primes_table_nb(), 6541);  //  # odd primes on 16 bits
    TS_ASSERT_EQUALS(odd_primes_table_nb() + 1,
                     sizeof(array_odd_primes_)/sizeof(array_odd_primes_[0]));
#else
    TS_ASSERT_EQUALS(odd_primes_table_nb(), 14630842u);  //  # odd primes on 28 bits
    // TS_ASSERT_EQUALS(odd_primes_table_nb(), 203280220u);  //  # odd primes on 32 bits
#endif
  }


#ifdef PRIME16
  void ZZZtest__potential_prime_offsets_table_by_index() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(odd_primes_table_by_index(0), 3);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(1), 5);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(2), 7);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(3), 11);

    // http://primes.utm.edu/nthprime/index.php#nth (be careful, only odd)
    TS_ASSERT_EQUALS(odd_primes_table_by_index( 99),  547);
    TS_ASSERT_EQUALS(odd_primes_table_by_index(199), 1229);
  }
#endif


#ifdef PRIME16
  void ZZZtest__potential_prime_offsets_table_modulo() {
    SHOW_FUNC_NAME;
    // The modulo must be less than the last prime in the table
    TS_ASSERT_LESS_THAN(potential_prime_offsets_table_modulo(), odd_primes_table_last());

    // Last prime in the table must be equals 1 modulo by index offset table, to make easy next
    TS_ASSERT_EQUALS(odd_primes_table_last() % potential_prime_offsets_table_modulo(), 1);

    {  // Modulo must be equals to a primorial
      nat_type prod = 2;

      for (unsigned int i = 0;
           (prod < potential_prime_offsets_table_modulo()) && (i < odd_primes_table_nb());
           ++i) {
        prod *= odd_primes_table_by_index(i);
      }

      TS_ASSERT_EQUALS(potential_prime_offsets_table_modulo(), prod);
    }

    {  // Sum of all offsets must be equals to the modulo
      nat_type sum = 0;

      for (unsigned int i = 0; i < potential_prime_offsets_table_nb(); ++i) {
        sum += potential_prime_offsets_table_by_index(i);
      }

      TS_ASSERT_EQUALS(potential_prime_offsets_table_modulo(), sum);
    }
  }
#endif


#ifdef PRIME16
  void ZZZtest__potential_prime_offsets_table_nb() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(potential_prime_offsets_table_nb(),
                     sizeof(array_potential_prime_offsets_)
                     / sizeof(array_potential_prime_offsets_[0]));
  }
#endif

};
