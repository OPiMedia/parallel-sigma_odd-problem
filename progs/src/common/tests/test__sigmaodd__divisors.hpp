/* -*- coding: latin-1 -*- */
/** \file common/tests/test__sigmaodd__divisors.hpp (August 30, 2020)
 *
 * GPLv3 --- Copyright (C) 2017, 2018, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <functional>
#include <numeric>
#include <limits>

#include "test_helper.hpp"

#include "../sigmaodd/divisors.hpp"
#include "../sigmaodd/primes.hpp"


using namespace sigmaodd;


class Test__sigmaodd__divisors : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    TS_ASSERT(read_primes_table());

    TS_ASSERT_DIFFERS(array_odd_primes_,nullptr);


    std::srand(666);  // to have deterministic "random" numbers
  }



  void test__coprime_factor_exps() {
    SHOW_FUNC_NAME;

    unsigned int nb = 0;
    unsigned int total = 0;

    for (nat_type a = 2; a < 2000; ++a) {
      for (nat_type b = a; b < 5000; ++b, ++total) {
        const std::vector<FactorExp> factor_exps = coprime_factor_exps(a, b);

        TS_ASSERT_EQUALS(factor_exps, coprime_factor_exps(b, a));

        if (is_coprime(a, b)) {
          TS_ASSERT_EQUALS(factor_exps.size(), 2);
          TS_ASSERT_EQUALS(factor_exps[0].exp, 1);
          TS_ASSERT_EQUALS(factor_exps[1].exp, 1);
          if (a <= b) {
            TS_ASSERT_EQUALS(factor_exps[0].factor, a);
            TS_ASSERT_EQUALS(factor_exps[1].factor, b);
          }
          else {
            TS_ASSERT_EQUALS(factor_exps[0].factor, b);
            TS_ASSERT_EQUALS(factor_exps[1].factor, a);
          }
        }

        if (factor_exps.empty()) {  // heuristic failed
          continue;
        }
        ++nb;

        const nat_type n = a*b;

        if (is_prime(n)) {
          TS_ASSERT_EQUALS(factor_exps.size(), 1);
          TS_ASSERT_EQUALS(factor_exps[0].factor, n);
          TS_ASSERT_EQUALS(factor_exps[0].exp, 1);
        }
        else {
          TS_ASSERT_LESS_THAN_EQUALS(1, factor_exps.size());

          // All are coprime ?
          for (auto it1 = factor_exps.cbegin(); it1 < factor_exps.cend(); ++it1) {
            TS_ASSERT_LESS_THAN(1, it1->factor);
            TS_ASSERT_LESS_THAN(it1->factor, n);

            for (auto it2 = it1 + 1; it2 < factor_exps.cend(); ++it2) {
              TS_ASSERT(is_coprime(it1->factor, it2->factor));
            }
          }
        }

        // The product of all factors == a*b
        nat_type product = 1;

        for (FactorExp factor_exp : factor_exps) {
          product *= pow_nat(factor_exp.factor, factor_exp.exp);
        }

        TS_ASSERT_EQUALS(product, n);
      }
    }

    std::cout << "Find " << nb << " / " << total << " ~= " << static_cast<double>(nb*100)/total << '%' << std::endl;
  }


  void test__divide_until_nb() {
    SHOW_FUNC_NAME;

    for (nat_type d = 2; d < 1000; ++d) {
      const std::pair<nat_type, unsigned int> result_alpha = divide_until_nb(1, d);

      TS_ASSERT_EQUALS(result_alpha.first, 1);
      TS_ASSERT_EQUALS(result_alpha.second, 0);
    }

    for (nat_type n = 1; n < 1000000; ++n) {
      const std::pair<nat_type, unsigned int> result_alpha = divide_until_nb(n, 2);

      TS_ASSERT_EQUALS(result_alpha.first, divide_until_odd(n));
      if (is_even(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1, result_alpha.second);
      }
      else {
        TS_ASSERT_EQUALS(result_alpha.second, 0);
        TS_ASSERT_EQUALS(result_alpha.first*pow_nat(2, result_alpha.second), n);
      }
    }

    for (nat_type n = 1; n < 10000; ++n) {
      for (nat_type d = 2; d < 1000; ++d) {
        const std::pair<nat_type, unsigned int> result_alpha = divide_until_nb(n, d);

        if (is_divide(d, n)) {
          TS_ASSERT_LESS_THAN_EQUALS(1, result_alpha.first);
          TS_ASSERT_LESS_THAN(result_alpha.first, n);
          TS_ASSERT_LESS_THAN_EQUALS(1, result_alpha.second);
        }
        else {
          TS_ASSERT_EQUALS(result_alpha.first, n);
          TS_ASSERT_EQUALS(result_alpha.second, 0);
        }
        TS_ASSERT_EQUALS(result_alpha.first*pow_nat(d, result_alpha.second), n);
      }
    }
  }


  void test__divide_until_odd() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sizeof(long int), sizeof(nat_type));

    TS_ASSERT_EQUALS(divide_until_odd(1), 1);
    TS_ASSERT_EQUALS(divide_until_odd(2), 1);
    TS_ASSERT_EQUALS(divide_until_odd(3), 3);
    TS_ASSERT_EQUALS(divide_until_odd(4), 1);
    TS_ASSERT_EQUALS(divide_until_odd(5), 5);
    TS_ASSERT_EQUALS(divide_until_odd(6), 3);

    for (unsigned int i = 0; i < sizeof(nat_type)*8; ++i) {
      TS_ASSERT_EQUALS(divide_until_odd(static_cast<nat_type>(1) << i), 1);
    }

    for (nat_type n = 1; n < 1000000; ++n) {
      if (is_odd(n)) {
        TS_ASSERT_EQUALS(divide_until_odd(n), n);
      }
      else {
        const nat_type odd = divide_until_odd(n);

        TS_ASSERT(is_odd(odd));
        TS_ASSERT(is_divide(odd, n));
      }
    }

    for (unsigned int i = 0; i < 1000000; ++i) {
      const nat_type n = std::numeric_limits<nat_type>::max() - i;

      if (is_odd(n)) {
        TS_ASSERT_EQUALS(divide_until_odd(n), n);
      }
      else {
        const nat_type odd = divide_until_odd(n);

        TS_ASSERT(is_odd(odd));
        TS_ASSERT(is_divide(odd, n));
      }
    }
  }


  void test__divide_until_odd_nb() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(sizeof(long int), sizeof(nat_type));

    {
      const std::pair<nat_type, unsigned int> result_alpha = divide_until_odd_nb(1);

      TS_ASSERT_EQUALS(result_alpha.first, 1);
      TS_ASSERT_EQUALS(result_alpha.second, 0);
    }

    for (nat_type n = 1; n < 1000000; ++n) {
      const std::pair<nat_type, unsigned int> result_alpha = divide_until_odd_nb(n);

      TS_ASSERT_EQUALS(result_alpha.first, divide_until_odd(n));
      if (is_even(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1, result_alpha.second);
      }
      else {
        TS_ASSERT_EQUALS(result_alpha.second, 0);
        TS_ASSERT_EQUALS(result_alpha.first*pow_nat(2, result_alpha.second), n);
      }
    }
  }


  void test__first_divisor() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(first_divisor(0), 1);
    TS_ASSERT_EQUALS(first_divisor(1), 1);
    TS_ASSERT_EQUALS(first_divisor(2), 2);
    TS_ASSERT_EQUALS(first_divisor(3), 3);
    TS_ASSERT_EQUALS(first_divisor(4), 2);
    TS_ASSERT_EQUALS(first_divisor(5), 5);
    TS_ASSERT_EQUALS(first_divisor(6), 2);
    TS_ASSERT_EQUALS(first_divisor(7), 7);
    TS_ASSERT_EQUALS(first_divisor(8), 2);
    TS_ASSERT_EQUALS(first_divisor(9), 3);
    TS_ASSERT_EQUALS(first_divisor(10), 2);

    TS_ASSERT_EQUALS(first_divisor(31), 31);

    TS_ASSERT_EQUALS(first_divisor(35), 5);
    TS_ASSERT_EQUALS(first_divisor(42), 2);
    TS_ASSERT_EQUALS(first_divisor(49), 7);
    TS_ASSERT_EQUALS(first_divisor(56), 2);
    TS_ASSERT_EQUALS(first_divisor(63), 3);
    TS_ASSERT_EQUALS(first_divisor(77), 7);

    for (nat_type n = 1; n < 100000; ++n) {
      const nat_type first = first_divisor(n);

      TS_ASSERT_LESS_THAN_EQUALS(first, n);

      TS_ASSERT(is_divide(first, n));
      if (is_even(n)) {
        TS_ASSERT_EQUALS(first, 2);
      }
      else {
        for (nat_type d = 3; d < first; d += 2) {
          TS_ASSERT(!is_divide(d, n));
        }
      }
    }

    TS_ASSERT_EQUALS(first_divisor(65521), 65521);
    TS_ASSERT_EQUALS(first_divisor(65537), 65537);
    TS_ASSERT_EQUALS(first_divisor(65539), 65539);
    TS_ASSERT_EQUALS(first_divisor(65543), 65543);
    TS_ASSERT_EQUALS(first_divisor(65551), 65551);
    TS_ASSERT_EQUALS(first_divisor(65557), 65557);
    TS_ASSERT_EQUALS(first_divisor(65563), 65563);
    TS_ASSERT_EQUALS(first_divisor(65579), 65579);
    TS_ASSERT_EQUALS(first_divisor(65581), 65581);

    TS_ASSERT_EQUALS(first_divisor(65563ul*65579), 65563);
    TS_ASSERT_EQUALS(first_divisor(65579ul*65579), 65579);
    TS_ASSERT_EQUALS(first_divisor(65579ul*65581), 65579);

    for (nat_type n = 10000000000; n < 10000000000 + 10000; ++n) {
      const nat_type first = first_divisor(n);

      TS_ASSERT_LESS_THAN_EQUALS(first, n);

      TS_ASSERT(is_divide(first, n));
      if (is_even(n)) {
        TS_ASSERT_EQUALS(first, 2);
      }
      else if (first < n) {
        for (nat_type d = 3; d < first; d += 2) {
          TS_ASSERT(!is_divide(d, n));
        }
      }
      else {
        const nat_type sqrt_n = floor_square_root(n);

        for (nat_type d = 3; d <= sqrt_n; d += 2) {
          TS_ASSERT(!is_divide(d, n));
        }
      }
    }

    // Check prime numbers
    for (unsigned int i = 0; i < std::min(10000u, odd_primes_table_nb()); ++i) {
      const nat_type prime = odd_primes_table_by_index(i);

      TS_ASSERT_EQUALS(first_divisor(prime), prime);
    }

    // http://primes.utm.edu/nthprime/index.php
    TS_ASSERT_EQUALS(first_divisor(15485861), 17);
    TS_ASSERT_EQUALS(first_divisor(15485863), 15485863);
    TS_ASSERT_EQUALS(first_divisor(15485865), 3);
    TS_ASSERT_EQUALS(first_divisor(22801763487), 3);
    TS_ASSERT_EQUALS(first_divisor(22801763489), 22801763489);
    TS_ASSERT_EQUALS(first_divisor(22801763491), 13);
  }


  void test__gcd() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(gcd(0, 0), 0);
    TS_ASSERT_EQUALS(gcd(1, 0), 1);

    TS_ASSERT_EQUALS(gcd(6, 10), 2);
    TS_ASSERT_EQUALS(gcd(21, 42), 21);
    TS_ASSERT_EQUALS(gcd(42, 42), 42);
    TS_ASSERT_EQUALS(gcd(42, 13*5), 1);
    TS_ASSERT_EQUALS(gcd(42, 666), 6);

    for (nat_type a = 0; a < 1000; ++a) {
      TS_ASSERT_EQUALS(gcd(a, 0), a);

      for (nat_type b = 0; b < 1000; ++b) {
        const nat_type d = gcd(a, b);

        if (d == 0) {
          TS_ASSERT_EQUALS(a, 0);
          TS_ASSERT_EQUALS(b, 0);
        }
        else {
          TS_ASSERT(is_divide(d, a));
          TS_ASSERT(is_divide(d, b));

          TS_ASSERT_EQUALS(gcd(a/d, b/d), 1);
        }

        if (a == b) {
          TS_ASSERT_EQUALS(d, a);
        }

        TS_ASSERT_EQUALS(gcd(b, a), d);
      }

      for (unsigned int i = 0; i < std::min(10000u, odd_primes_table_nb()); ++i) {
        const nat_type prime = odd_primes_table_by_index(i);
        const nat_type d = gcd(a, prime);

        if (is_divide(prime, a)) {
          TS_ASSERT_EQUALS(d, prime);
        }
        else {
          TS_ASSERT_EQUALS(d, 1);
        }
      }
    }
  }


  void test__is_coprime() {
    SHOW_FUNC_NAME;

    TS_ASSERT(!is_coprime(0, 0));

    TS_ASSERT(!is_coprime(6, 10));
    TS_ASSERT(!is_coprime(21, 42));
    TS_ASSERT(!is_coprime(42, 42));
    TS_ASSERT(!is_coprime(42, 666));

    TS_ASSERT(is_coprime(1, 0));
    TS_ASSERT(is_coprime(42, 13*5));

    for (nat_type a = 0; a < 1000; ++a) {
      TS_ASSERT_EQUALS(is_coprime(a, 0), (a == 1));

      for (nat_type b = 0; b < 1000; ++b) {
        const bool c = is_coprime(a, b);

        TS_ASSERT_EQUALS(is_coprime(b, a), c);
      }

      for (unsigned int i = 0; i < std::min(10000u, odd_primes_table_nb()); ++i) {
        const nat_type prime = odd_primes_table_by_index(i);
        const bool c = is_coprime(a, prime);

        if (is_divide(prime, a)) {
          TS_ASSERT(!c);
        }
        else {
          TS_ASSERT(c);
        }
      }
    }

    for (unsigned int i = 0; i < std::min(10000u, odd_primes_table_nb()); ++i) {
      const nat_type prime1 = odd_primes_table_by_index(i);

      for (unsigned int j = 0; j < std::min(10000u, odd_primes_table_nb()); ++j) {
        const nat_type prime2 = odd_primes_table_by_index(i);

        TS_ASSERT_EQUALS(is_coprime(prime1, prime2), (prime1 != prime2));
      }
    }
  }


  void test__is_divide() {
    SHOW_FUNC_NAME;

    for (nat_type n = 0; n < 10000; ++n) {
      for (nat_type d = 1; d < 1000; ++d) {
        const nat_type quotient = n/d;

        if (is_divide(d, n)) {
          TS_ASSERT_EQUALS(n % d, 0);
          TS_ASSERT_EQUALS(quotient*d, n);
        }
        else {
          TS_ASSERT_DIFFERS(n % d, 0);
          TS_ASSERT_LESS_THAN(quotient*d, n);
        }
      }
    }
  }


  void test__is_first_mersenne_prime_unitary_divide() {
    SHOW_FUNC_NAME;

    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(0));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(1));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(2));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(4));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(5));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(8));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(9));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(10));

    TS_ASSERT(is_first_mersenne_prime_unitary_divide(3));
    TS_ASSERT(is_first_mersenne_prime_unitary_divide(6));

    TS_ASSERT(is_first_mersenne_prime_unitary_divide(3*5));
    TS_ASSERT(is_first_mersenne_prime_unitary_divide(3*5*5*11));
    TS_ASSERT(is_first_mersenne_prime_unitary_divide(3*7));
    TS_ASSERT(is_first_mersenne_prime_unitary_divide(3*7*11));
    TS_ASSERT(is_first_mersenne_prime_unitary_divide(3*7*7*11));

    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(3*3*11));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(7*7*11));

    TS_ASSERT(is_first_mersenne_prime_unitary_divide(11*127));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide(11*127*127));
  }


  void test__is_first_mersenne_prime_unitary_divide_or_square() {
    SHOW_FUNC_NAME;

    TS_ASSERT( is_first_mersenne_prime_unitary_divide_or_square(0));
    TS_ASSERT( is_first_mersenne_prime_unitary_divide_or_square(1));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide_or_square(2));
    TS_ASSERT( is_first_mersenne_prime_unitary_divide_or_square(4));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide_or_square(5));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide_or_square(8));
    TS_ASSERT( is_first_mersenne_prime_unitary_divide_or_square(9));
    TS_ASSERT(!is_first_mersenne_prime_unitary_divide_or_square(10));

    for (nat_type n = 0; n < 10000; ++n) {
      TS_ASSERT_EQUALS(is_first_mersenne_prime_unitary_divide_or_square(n),
                       is_first_mersenne_prime_unitary_divide(n) || is_square(n));
    }
  }


  void test__is_little_mersenne_prime_unitary_divide() {
    SHOW_FUNC_NAME;

    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(0));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(1));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(2));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(4));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(5));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(8));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(9));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(10));

    TS_ASSERT(is_little_mersenne_prime_unitary_divide(3));
    TS_ASSERT(is_little_mersenne_prime_unitary_divide(6));

    TS_ASSERT(is_little_mersenne_prime_unitary_divide(3*5));
    TS_ASSERT(is_little_mersenne_prime_unitary_divide(3*5*5*11));
    TS_ASSERT(is_little_mersenne_prime_unitary_divide(3*7));
    TS_ASSERT(is_little_mersenne_prime_unitary_divide(3*7*11));
    TS_ASSERT(is_little_mersenne_prime_unitary_divide(3*7*7*11));

    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(3*3*11));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(7*7*11));

    TS_ASSERT(is_little_mersenne_prime_unitary_divide(11*127));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(11*127*127));

    TS_ASSERT(is_little_mersenne_prime_unitary_divide(11*8191));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(11*8191*8191));

    TS_ASSERT(is_little_mersenne_prime_unitary_divide(11u*524287u));
    TS_ASSERT(!is_little_mersenne_prime_unitary_divide(11u*524287u*524287u));
  }


  void test__is_mersenne_prime_unitary_divide() {
    SHOW_FUNC_NAME;

    TS_ASSERT(!is_mersenne_prime_unitary_divide(0));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(1));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(2));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(4));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(5));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(8));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(9));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(10));

    TS_ASSERT(is_mersenne_prime_unitary_divide(3));
    TS_ASSERT(is_mersenne_prime_unitary_divide(6));

    TS_ASSERT(is_mersenne_prime_unitary_divide(3*5));
    TS_ASSERT(is_mersenne_prime_unitary_divide(3*5*5*11));
    TS_ASSERT(is_mersenne_prime_unitary_divide(3*7));
    TS_ASSERT(is_mersenne_prime_unitary_divide(3*7*11));
    TS_ASSERT(is_mersenne_prime_unitary_divide(3*7*7*11));

    TS_ASSERT(!is_mersenne_prime_unitary_divide(3*3*11));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(7*7*11));

    TS_ASSERT(is_mersenne_prime_unitary_divide(11*127));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(11*127*127));

    TS_ASSERT(is_mersenne_prime_unitary_divide(11*8191));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(11*8191*8191));

    TS_ASSERT(is_mersenne_prime_unitary_divide(11u*524287u));
    TS_ASSERT(!is_mersenne_prime_unitary_divide(11u*524287u*524287u));
  }


  void test__is_square() {
    SHOW_FUNC_NAME;

    TS_ASSERT(is_square(0));
    TS_ASSERT(is_square(1));
    TS_ASSERT(is_square(4));
    TS_ASSERT(is_square(9));

    TS_ASSERT(!is_square(2));
    TS_ASSERT(!is_square(3));
    TS_ASSERT(!is_square(5));
    TS_ASSERT(!is_square(6));
    TS_ASSERT(!is_square(7));
    TS_ASSERT(!is_square(8));
    TS_ASSERT(!is_square(10));

    for (nat_type n = 2; n < 10000000; ++n) {
      TS_ASSERT(is_square(square(n)));
      TS_ASSERT(!is_square(square(n) - 1));
      TS_ASSERT(!is_square(square(n) + 1));
    }

    for (nat_type n = 1000000000; n < 1000000000 + 10000; ++n) {
      TS_ASSERT(is_square(square(n)));
      TS_ASSERT(!is_square(square(n) - 1));
      TS_ASSERT(!is_square(square(n) + 1));
    }
  }


  void test__is_unitary_divide() {
    SHOW_FUNC_NAME;

    for (nat_type n = 0; n < 10000; ++n) {
      TS_ASSERT(!is_unitary_divide(1, n));

      for (nat_type d = 1; d < 1000; ++d) {
        const nat_type quotient = n/d;

        if (is_unitary_divide(d, n)) {
          TS_ASSERT(is_divide(d, n));
          TS_ASSERT(!is_divide(d, quotient));
        }
        else if (is_divide(d, n)) {
          TS_ASSERT(is_divide(d, quotient));
        }
        else {
          TS_ASSERT_DIFFERS(n % d, 0);
          TS_ASSERT_LESS_THAN(quotient*d, n);
        }
      }
    }
  }


  void test__pollard_rho__n() {
    SHOW_FUNC_NAME;

    unsigned int nb = 0;
    unsigned int total = 0;
    const unsigned int max = 10000;

    for (nat_type n = 1; n <= max; ++n, ++total) {
      const nat_type d = pollard_rho(n);

      if (d != 0) {  // heuristic success
        TS_ASSERT_LESS_THAN(1, d);
        TS_ASSERT_LESS_THAN(d, n);
        TS_ASSERT(is_divide(d, n));
        ++nb;
      }
    }

    for (nat_type n = 1000000000; n < 1000000000 + max; ++n , ++total) {
      const nat_type d = pollard_rho(n);

      if (d != 0) {  // heuristic success
        TS_ASSERT_LESS_THAN(1, d);
        TS_ASSERT_LESS_THAN(d, n);
        TS_ASSERT(is_divide(d, n));
        ++nb;
      }
    }

    std::cout << "Find " << nb << " / " << total << " ~= " << static_cast<double>(nb*100)/total << '%' << std::endl;
  }


  void test__pollard_rho__n__random__max_iteration() {
    SHOW_FUNC_NAME;

    unsigned int nb = 0;
    unsigned int total = 0;
    const unsigned int max = 10000;

    for (nat_type n = 1; n <= max; ++n, ++total) {
      const nat_type d = pollard_rho(n, 42 % n, floor_square_root(floor_square_root(n)));

      if (d != 0) {  // heuristic success
        TS_ASSERT_LESS_THAN(1, d);
        TS_ASSERT_LESS_THAN(d, n);
        TS_ASSERT(is_divide(d, n));
        ++nb;
      }
    }

    for (nat_type n = 1000000000; n < 1000000000 + max; ++n , ++total) {
      const nat_type d = pollard_rho(n, 42, floor_square_root(floor_square_root(n)));

      if (d != 0) {  // heuristic success
        TS_ASSERT_LESS_THAN(1, d);
        TS_ASSERT_LESS_THAN(d, n);
        TS_ASSERT(is_divide(d, n));
        ++nb;
      }
    }

    std::cout << "Find " << nb << " / " << total << " = " << static_cast<double>(nb*100)/total << '%' << std::endl;
  }


  void test__pollard_rho_repeat() {
    SHOW_FUNC_NAME;

    unsigned int nb = 0;
    unsigned int total = 0;
    const unsigned int max = 10000;

    for (nat_type n = 1; n <= max; ++n, ++total) {
      const nat_type d = pollard_rho_repeat(n, 2);

      if (d != 0) {  // heuristic success
        TS_ASSERT_LESS_THAN(1, d);
        TS_ASSERT_LESS_THAN(d, n);
        TS_ASSERT(is_divide(d, n));
        ++nb;
      }
    }

    for (nat_type n = 1000000000; n < 1000000000 + max; ++n , ++total) {
      const nat_type d = pollard_rho_repeat(n, 2);

      if (d != 0) {  // heuristic success
        TS_ASSERT_LESS_THAN(1, d);
        TS_ASSERT_LESS_THAN(d, n);
        TS_ASSERT(is_divide(d, n));
        ++nb;
      }
    }

    std::cout << "Find " << nb << " / " << total << " = " << static_cast<double>(nb*100)/total << '%' << std::endl;
  }


  void test__sum_divisors__factorize() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sum_divisors__factorize(1), 1);
    TS_ASSERT_EQUALS(sum_divisors__factorize(2), 1 + 2);
    TS_ASSERT_EQUALS(sum_divisors__factorize(3), 1 + 3);
    TS_ASSERT_EQUALS(sum_divisors__factorize(4), 1 + 2 + 4);
    TS_ASSERT_EQUALS(sum_divisors__factorize(5), 1 + 5);
    TS_ASSERT_EQUALS(sum_divisors__factorize(6), 1 + 2 + 3 + 6);
    TS_ASSERT_EQUALS(sum_divisors__factorize(7), 1 + 7);
    TS_ASSERT_EQUALS(sum_divisors__factorize(8), 1 + 2 + 4 + 8);
    TS_ASSERT_EQUALS(sum_divisors__factorize(9), 1 + 3 + 9);

    // https://oeis.org/A000203/b000203.txt
    TS_ASSERT_EQUALS(sum_divisors__factorize(    99),   156);
    TS_ASSERT_EQUALS(sum_divisors__factorize(   100),   217);
    TS_ASSERT_EQUALS(sum_divisors__factorize(   999),  1520);
    TS_ASSERT_EQUALS(sum_divisors__factorize(  1000),  2340);
    TS_ASSERT_EQUALS(sum_divisors__factorize(  9999),  15912);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 10000),  24211);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 10001),  10212);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 10001),  10212);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99990), 286416);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99991),  99992);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99992), 194400);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99993), 133328);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99994), 160254);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99995), 137184);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99996), 251664);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99997), 105918);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99998), 150000);
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99999), 148512);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000), 246078);

    // https://www.wolframalpha.com/input/?i=DivisorSigma%5B1,+n%5D,+%7Bn%3D999999,1000002%7D%5D
    TS_ASSERT_EQUALS(sum_divisors__factorize( 999999), 2042880);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000), 2480437);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000001), 1010004);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000002), 2000016);

    TS_ASSERT_EQUALS(sum_divisors__factorize( 9999999), 14508000);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000), 24902280);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000001), 10909104);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000002), 20426112);

    TS_ASSERT_EQUALS(sum_divisors__factorize( 99999999), 162493344);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000000), 249511591);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000001), 105882372);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000002), 210988800);

    TS_ASSERT_EQUALS(sum_divisors__factorize( 999999999), 1534205464);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000000), 2497558338);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000001), 1413350400);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000002), 2049113088);

    TS_ASSERT_EQUALS(sum_divisors__factorize( 9999999999), 16203253248);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000000), 24987792457);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000001), 10102223208);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000002), 20000000016);

    TS_ASSERT_EQUALS(sum_divisors__factorize( 99999999999), 144451398000);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000000000), 249938963820);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000000001), 114737461440);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000000002), 228719711232);

    TS_ASSERT_EQUALS(sum_divisors__factorize( 999999999999), 2063316971520);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000000000), 2499694822171);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000000001), 1021097900424);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000000002), 2000000000016);

    // 10^13 > 2^43
    TS_ASSERT_EQUALS(sum_divisors__factorize( 9999999999999), 14903272088640);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000000000), 24998474116998);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000000001), 10921790676000);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000000002), 20017814978304);

    // 10^14 > 2^46
    TS_ASSERT_EQUALS(sum_divisors__factorize( 99999999999999), 158269280832000);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000000000000), 249992370597277);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000000000001), 104844305394000);
    TS_ASSERT_EQUALS(sum_divisors__factorize(100000000000002), 203052956984640);

    // 10^15 > 2^49
    TS_ASSERT_EQUALS(sum_divisors__factorize( 999999999999999), 1614847741624320);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000000000000), 2499961853010960);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000000000001), 1355394166984704);
    TS_ASSERT_EQUALS(sum_divisors__factorize(1000000000000002), 2000000000000016);

    // 10^16 > 2^53
    TS_ASSERT_EQUALS(sum_divisors__factorize( 9999999999999999), 17205180696931968);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000000000000), 24999809265103951);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000000000001), 10073631600468000);
    TS_ASSERT_EQUALS(sum_divisors__factorize(10000000000000002), 20000015492304672);

    // 2^27 - 1
    TS_ASSERT_EQUALS(sum_divisors__factorize(268435455u), 462274560u);

    // Mersenne prime: https://oeis.org/A000668
    TS_ASSERT_EQUALS(sum_divisors__factorize(         3),          4);
    TS_ASSERT_EQUALS(sum_divisors__factorize(         7),          8);
    TS_ASSERT_EQUALS(sum_divisors__factorize(        31),         32);
    TS_ASSERT_EQUALS(sum_divisors__factorize(       127),        128);
    TS_ASSERT_EQUALS(sum_divisors__factorize(      8191),       8192);
    TS_ASSERT_EQUALS(sum_divisors__factorize(    131071),     131072);
    TS_ASSERT_EQUALS(sum_divisors__factorize(    524287),     524288);
    if (odd_primes_table_nb() >= 14630843u) {
      // 2^31 - 1
      TS_ASSERT_EQUALS(sum_divisors__factorize(2147483647u), 2147483648u);
      // 2^61 - 1
      TS_ASSERT_EQUALS(sum_divisors__factorize(2305843009213693951u), 2305843009213693952u);
    }

    for (nat_type n = 2; n < 10000; ++n) {
      const nat_type sum = sum_divisors__factorize(n);

      TS_ASSERT_LESS_THAN(1, sum);
      if (is_odd(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1 + n, sum);
      }
      TS_ASSERT_LESS_THAN_EQUALS(sum, sum_natural(n));

      TS_ASSERT_EQUALS(sum, sum_divisors__naive(n));
    }
  }


  void test__sum_divisors__naive() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sum_divisors__naive(1), 1);
    TS_ASSERT_EQUALS(sum_divisors__naive(2), 1 + 2);
    TS_ASSERT_EQUALS(sum_divisors__naive(3), 1 + 3);
    TS_ASSERT_EQUALS(sum_divisors__naive(4), 1 + 2 + 4);
    TS_ASSERT_EQUALS(sum_divisors__naive(5), 1 + 5);
    TS_ASSERT_EQUALS(sum_divisors__naive(6), 1 + 2 + 3 + 6);
    TS_ASSERT_EQUALS(sum_divisors__naive(7), 1 + 7);
    TS_ASSERT_EQUALS(sum_divisors__naive(8), 1 + 2 + 4 + 8);
    TS_ASSERT_EQUALS(sum_divisors__naive(9), 1 + 3 + 9);

    // https://oeis.org/A000203/b000203.txt
    TS_ASSERT_EQUALS(sum_divisors__naive(    99),   156);
    TS_ASSERT_EQUALS(sum_divisors__naive(   100),   217);
    TS_ASSERT_EQUALS(sum_divisors__naive(   999),  1520);
    TS_ASSERT_EQUALS(sum_divisors__naive(  1000),  2340);
    TS_ASSERT_EQUALS(sum_divisors__naive(  9999),  15912);
    TS_ASSERT_EQUALS(sum_divisors__naive( 10000),  24211);
    TS_ASSERT_EQUALS(sum_divisors__naive( 10001),  10212);
    TS_ASSERT_EQUALS(sum_divisors__naive( 10001),  10212);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99990), 286416);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99991),  99992);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99992), 194400);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99993), 133328);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99994), 160254);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99995), 137184);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99996), 251664);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99997), 105918);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99998), 150000);
    TS_ASSERT_EQUALS(sum_divisors__naive( 99999), 148512);
    TS_ASSERT_EQUALS(sum_divisors__naive(100000), 246078);

    for (nat_type n = 2; n < 10000; ++n) {
      const nat_type sum = sum_divisors__naive(n);

      TS_ASSERT_LESS_THAN(1, sum);
      if (is_odd(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1 + n, sum);
      }
      TS_ASSERT_LESS_THAN_EQUALS(sum, sum_natural(n));
    }
  }


  void test__sum_odd_divisors__factorize() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(2), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(3), 1 + 3);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(4), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(5), 1 + 5);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(6), 1 + 3);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(7), 1 + 7);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(8), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(9), 1 + 3 + 9);

    // http://www.wolframalpha.com/input/?i=DivisorSigma(123456789)
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(       89),        90);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(      789),      1056);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(     6789),      9472);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(    56789),     57420);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(   456789),    623392);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(  3456789),   5324928);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 23456789),  23456790);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(123456789), 178422816);

    // https://oeis.org/A000593/b000593.txt
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(   99),   156);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(  100),    31);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(  999),  1520);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 1000),   156);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 1001),  1344);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9990),  9120);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9991), 10192);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9992),  1250);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9993), 13328);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9994),  5280);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9995), 12000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9996),  4104);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9997), 10780);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9998),  5000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9999), 15912);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000),   781);

    // https://www.wolframalpha.com/input/?i=DivisorSigma%5B1,+n%2F(2%5EIntegerExponent%5Bn,+2%5D)%5D,+%7Bn%3D999999,1000002%7D%5D
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 999999), 2042880);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000),   19531);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000001), 1010004);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000002),  666672);

    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9999999), 14508000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000),    97656);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000001), 10909104);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000002),  6808704);

    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 99999999), 162493344);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000000),    488281);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000001), 105882372);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000002),  70329600);

    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 999999999), 1534205464);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000000),    2441406);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000001), 1413350400);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000002),  683037696);

    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9999999999), 16203253248);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000000),    12207031);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000001), 10102223208);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000002),  6666666672);

    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 99999999999), 144451398000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000000000),     61035156);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000000001), 114737461440);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000000002),  76239903744);

    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 999999999999), 2063316971520);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000000000),     305175781);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000000001), 1021097900424);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000000002),  666666666672);

    // 10^13 > 2^43
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9999999999999), 14903272088640);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000000000),     1525878906);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000000001), 10921790676000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000000002),  6672604992768);

    // 10^14 > 2^46
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 99999999999999), 158269280832000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000000000000),      7629394531);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000000000001), 104844305394000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(100000000000002),  67684318994880);

    // 10^15 > 2^49
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 999999999999999), 1614847741624320);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000000000000),      38146972656);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000000000001), 1355394166984704);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(1000000000000002),  666666666666672);

    // 10^16 > 2^53
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize( 9999999999999999), 17205180696931968);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000000000000),      190734863281);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000000000001), 10073631600468000);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(10000000000000002),  6666671830768224);

    // 2^27 - 1
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(268435455u), 462274560u);

    // Mersenne prime: https://oeis.org/A000668
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(         3),          4);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(         7),          8);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(        31),         32);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(       127),        128);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(      8191),       8192);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(    131071),     131072);
    TS_ASSERT_EQUALS(sum_odd_divisors__factorize(    524287),     524288);
    if (odd_primes_table_nb() >= 14630843u) {
      // 2^31 - 1
      TS_ASSERT_EQUALS(sum_odd_divisors__factorize(2147483647), 2147483648);
      // 2^61 - 1
      TS_ASSERT_EQUALS(sum_odd_divisors__factorize(2305843009213693951), 2305843009213693952);
    }

    // Check results for all primes in the table
    for (unsigned int i = 0; i < std::min(10000u, odd_primes_table_nb()); ++i) {
      const nat_type prime = odd_primes_table_by_index(i);

      TS_ASSERT_EQUALS(sum_odd_divisors__factorize(prime), prime + 1);
    }

    // Check that results for not prime n are > n + 1
    {
      nat_type previous = 3;

      for (unsigned int i = 1; i < std::min(10000u, odd_primes_table_nb()); ++i) {
        const nat_type prime = odd_primes_table_by_index(i);

        for (nat_type n = previous + 2; n < prime; n += 2) {
          TS_ASSERT_LESS_THAN(n + 1, sum_odd_divisors__factorize(n));
        }
        previous = prime;
      }
    }

    // Same results that naive method
    for (nat_type n = 2; n < 10000; ++n) {
      const nat_type sum = sum_odd_divisors__factorize(n);

      TS_ASSERT_EQUALS(sum, sum_odd_divisors__naive(n));
      TS_ASSERT_LESS_THAN_EQUALS(1, sum);
      if (is_odd(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1 + n, sum);
      }
      TS_ASSERT_LESS_THAN_EQUALS(sum, sum_odd(n));
    }

    for (nat_type n = 1000000; n < 1000100; ++n) {
      const nat_type sum = sum_odd_divisors__factorize(n);

      TS_ASSERT_EQUALS(sum, sum_odd_divisors__naive(n));
      TS_ASSERT_LESS_THAN(1, sum);
      if (is_odd(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1 + n, sum);
      }
      TS_ASSERT_LESS_THAN(sum, sum_odd(n));
    }

    for (nat_type n = 10000000; n < 10000010; ++n) {
      const nat_type sum = sum_odd_divisors__factorize(n);

      TS_ASSERT_EQUALS(sum, sum_odd_divisors__naive(n));
      TS_ASSERT_LESS_THAN(1, sum);
      if (is_odd(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1 + n, sum);
      }
      TS_ASSERT_LESS_THAN(sum, sum_odd(n));
    }

    // Calculate naively sum of even divisor, to compare
    for (nat_type n = 1; n < 100000; ++n) {
      const nat_type sum_odd = sum_odd_divisors__factorize(n);
      const nat_type sum_all = sum_divisors__factorize(n);

      if (is_odd(n)) {
        TS_ASSERT_EQUALS(sum_all, sum_odd);
      }
      else {
        nat_type sum_even = 0;

        for (nat_type d = 2; d <= n; d += 2) {
          if (is_divide(d, n)) {
            sum_even += d;
          }
        }

        TS_ASSERT_EQUALS(sum_odd + sum_even, sum_all);
      }
    }
  }


  void test__sum_odd_divisors__naive() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sum_odd_divisors__naive(1), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(2), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(3), 1 + 3);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(4), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(5), 1 + 5);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(6), 1 + 3);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(7), 1 + 7);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(8), 1);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(9), 1 + 3 + 9);

    // https://oeis.org/A000593/b000593.txt
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(   99),   156);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(  100),    31);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(  999),  1520);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 1000),   156);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 1001),  1344);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9990),  9120);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9991), 10192);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9992),  1250);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9993), 13328);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9994),  5280);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9995), 12000);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9996),  4104);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9997), 10780);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9998),  5000);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive( 9999), 15912);
    TS_ASSERT_EQUALS(sum_odd_divisors__naive(10000),   781);

    // Same results that with factorize method for all divisors
    for (nat_type n = 1; n < 10000; ++n) {
      if (is_odd(n)) {
        TS_ASSERT_EQUALS(sum_odd_divisors__naive(n), sum_divisors__factorize(n));
      }
      else {
        TS_ASSERT_EQUALS(sum_odd_divisors__naive(n), sum_divisors__factorize(divide_until_odd(n)));
      }
    }

    for (nat_type n = 1000000; n < 1000100; ++n) {
      if (is_odd(n)) {
        TS_ASSERT_EQUALS(sum_odd_divisors__naive(n), sum_divisors__factorize(n));
      }
      else {
        TS_ASSERT_EQUALS(sum_odd_divisors__naive(n), sum_divisors__factorize(divide_until_odd(n)));
      }
    }

    for (nat_type n = 10000000; n < 10000010; ++n) {
      if (is_odd(n)) {
        TS_ASSERT_EQUALS(sum_odd_divisors__naive(n), sum_divisors__factorize(n));
      }
      else {
        TS_ASSERT_EQUALS(sum_odd_divisors__naive(n), sum_divisors__factorize(divide_until_odd(n)));
      }
    }
  }


  void test__sum_odd_divisors_upper_bound() {
    SHOW_FUNC_NAME;

    for (nat_type n = 1; n < 20; ++n) {
      TS_ASSERT_LESS_THAN_EQUALS(sum_odd_divisors__factorize(n),
                                 sum_odd_divisors_upper_bound(n));
    }

    for (nat_type n = 10000000; n < 10010000; ++n) {
      TS_ASSERT_LESS_THAN_EQUALS(sum_odd_divisors__factorize(n),
                                 sum_odd_divisors_upper_bound(n));
    }
  }


  void test__sum_odd_divisors_upper_bound__DeTemple__n() {
    SHOW_FUNC_NAME;

    /*
      These values correspond to values obtained by ../../miscellaneous/harmonic/
      but the Wolfram|Alpha Web application give to me some different values!

      https://www.wolframalpha.com/input/?i=floor((floor(sqrt(n))%2B1)%2F2)%5E2+%2B+floor(n+(ln(+(2k%2B1)%5E2+%2F+(2(k%2B1))+)%2F2+%2B+EulerGamma%2F2+%2B+1%2F(24k%5E2)+-+1%2F(12(k%2B2)%5E2)))+where+k%3Dfloor(sqrt(n-1))

      https://www.wolframalpha.com/input/?i=floor((floor(sqrt(n))%2B1)%2F2)%5E2+%2B+floor(n+(ln(+(2k%2B1)%5E2+%2F+(2k)+)%2F2+%2B+EulerGamma%2F2+%2B+1%2F(24k%5E2)+-+1%2F(12(k%2B1)%5E2)))+where+k%3Dfloor(sqrt(n-1))
    */
    std::cout
      << "Maybe incorrect, because the Wolfram|Alpha Web application give some different values!"
      << std::endl;

    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 1),  1);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 2),  3);  // ! 2 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 3),  4);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 4),  5);  // ! 3 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 5),  6);  // ! 7 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 6),  7);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 7),  8);  // ! 9 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 8),  9);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 9), 13);  // 14 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(10), 17);  // 15 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(11), 18);

    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 99), 201);  // 201 by Maxima and Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(100), 203);  // ! 198 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(101), 205);  // ! 210 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(102), 207);

    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 999), 2621);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(1000), 2624);  // ! 2608 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(1001), 2626);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(1002), 2628);  // ! 2612 by Wolfram

    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 99999), 376265);  // ! 376423 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(100000), 376269);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(100001), 376272);   // ! 376430 by Wolfram
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(100002), 376276);

    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 999999), 4339055);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(1000000), 4339059);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(1000001), 4339063);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(1000002), 4339067);

    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 9999999), 49146170);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(10000000), 49146175);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(10000001), 49146180);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(10000002), 49146184);

    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple( 99999999), 549035155);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(100000000), 549035160);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(100000001), 549035166);
    TS_ASSERT_EQUALS(sum_odd_divisors_upper_bound__DeTemple(100000002), 549035171);

    for (nat_type n = 1; n < 1000000; ++n) {
      const nat_type bound = sum_odd_divisors_upper_bound__DeTemple(n);

      TS_ASSERT_LESS_THAN_EQUALS(sum_odd_divisors__factorize(n), bound);
    }

    for (nat_type n = 10000000; n < 10100000; ++n) {
      const nat_type bound = sum_odd_divisors_upper_bound__DeTemple(n);

      TS_ASSERT_LESS_THAN_EQUALS(sum_odd_divisors__factorize(n), bound);
    }

    for (unsigned int i = 0; i < 10000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 40) + i;
      const nat_type bound = sum_odd_divisors_upper_bound__DeTemple(n);

      TS_ASSERT_LESS_THAN_EQUALS(sum_odd_divisors__factorize(n), bound);
    }
  }


  void test__sum_odd_divisors_upper_bound__DeTemple_ceil__n() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__sum_odd_divisors_upper_bound__DeTemple__n_k() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }

};
