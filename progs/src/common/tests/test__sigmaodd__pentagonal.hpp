/* -*- coding: latin-1 -*- */
/** \file common/tests/test__sigmaodd__pentagonal.hpp (January 5, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "test_helper.hpp"

#include "../sigmaodd/pentagonal.hpp"
#include "../sigmaodd/divisors.hpp"
#include "../sigmaodd/primes.hpp"


using namespace sigmaodd;


class Test__sigmaodd__pentagonal : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    TS_ASSERT(read_primes_table());

    TS_ASSERT_DIFFERS(array_odd_primes_,nullptr);
  }


  void test__pentagonal() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(pentagonal(0), 0);
    TS_ASSERT_EQUALS(pentagonal(1), 1);
    TS_ASSERT_EQUALS(pentagonal(2), 5);
    TS_ASSERT_EQUALS(pentagonal(3), 12);
    TS_ASSERT_EQUALS(pentagonal(4), 22);
    TS_ASSERT_EQUALS(pentagonal(5), 35);

    // https://oeis.org/A000326/b000326.txt
    TS_ASSERT_EQUALS(pentagonal(99), 14652);
    TS_ASSERT_EQUALS(pentagonal(100), 14950);
    TS_ASSERT_EQUALS(pentagonal(999), 1496502);
    TS_ASSERT_EQUALS(pentagonal(1000), 1499500);
  }


  void test__pentagonal_neg() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(pentagonal_neg(0), 0);
    TS_ASSERT_EQUALS(pentagonal_neg(1), 2);
    TS_ASSERT_EQUALS(pentagonal_neg(2), 7);
    TS_ASSERT_EQUALS(pentagonal_neg(3), 15);
    TS_ASSERT_EQUALS(pentagonal_neg(4), 26);
    TS_ASSERT_EQUALS(pentagonal_neg(5), 40);

    // https://oeis.org/A005449/b005449.txt
    TS_ASSERT_EQUALS(pentagonal_neg(99), 14751);
    TS_ASSERT_EQUALS(pentagonal_neg(100), 15050);
    TS_ASSERT_EQUALS(pentagonal_neg(999), 1497501);
    TS_ASSERT_EQUALS(pentagonal_neg(1000), 1500500);
    TS_ASSERT_EQUALS(pentagonal_neg(1999), 5995001);
    TS_ASSERT_EQUALS(pentagonal_neg(2000), 6001000);
  }


  void test__sum_divisors__euler() {
    SHOW_FUNC_NAME;

    std::cout << "Table # items: " << sum_divisors__euler_table_nb() << std::endl;
    std::cout << "Cache # items: " << sum_divisors__euler_cache_nb() << std::endl;

    TS_ASSERT_EQUALS(sum_divisors__euler(1), 1);
    TS_ASSERT_EQUALS(sum_divisors__euler(2), 1 + 2);
    TS_ASSERT_EQUALS(sum_divisors__euler(3), 1 + 3);
    TS_ASSERT_EQUALS(sum_divisors__euler(4), 1 + 2 + 4);
    TS_ASSERT_EQUALS(sum_divisors__euler(5), 1 + 5);
    TS_ASSERT_EQUALS(sum_divisors__euler(6), 1 + 2 + 3 + 6);
    TS_ASSERT_EQUALS(sum_divisors__euler(7), 1 + 7);
    TS_ASSERT_EQUALS(sum_divisors__euler(8), 1 + 2 + 4 + 8);
    TS_ASSERT_EQUALS(sum_divisors__euler(9), 1 + 3 + 9);

    for (nat_type n = 2; n < 100000; ++n) {
      const nat_type sum = sum_divisors__euler(n);

      TS_ASSERT_LESS_THAN(1, sum);
      if (is_odd(n)) {
        TS_ASSERT_LESS_THAN_EQUALS(1 + n, sum);
      }
      TS_ASSERT_LESS_THAN_EQUALS(sum, sum_natural(n));

      TS_ASSERT_EQUALS(sum, sum_divisors__factorize(n));
    }

    std::cout << "Table # items: " << sum_divisors__euler_table_nb() << std::endl;
    std::cout << "Cache # items: " << sum_divisors__euler_cache_nb() << std::endl;
  }

};
