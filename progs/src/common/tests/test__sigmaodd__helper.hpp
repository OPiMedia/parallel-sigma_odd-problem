/* -*- coding: latin-1 -*- */
/** \file common/tests/test__sigmaodd__helper.hpp (August, 2020)
 *
 * GPLv3 --- Copyright (C) 2017, 2018, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <limits>

#include "test_helper.hpp"

#include "../sigmaodd/helper.hpp"
#include "../sigmaodd/divisors.hpp"


using namespace sigmaodd;


class Test__sigmaodd__helper : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    TS_ASSERT_LESS_THAN_EQUALS(sizeof(nat_type), 8);
    TS_ASSERT_LESS_THAN_EQUALS(sizeof(double), 8);
  }



  void test__ceil_eighth_root() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(ceil_eighth_root(0), 0);
    TS_ASSERT_EQUALS(ceil_eighth_root(1), 1);
    TS_ASSERT_EQUALS(ceil_eighth_root(2), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(3), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(4), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(5), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(6), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(7), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(8), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(9), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(10), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(15), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(16), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(17), 2);

    TS_ASSERT_EQUALS(ceil_eighth_root(255), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(256), 2);
    TS_ASSERT_EQUALS(ceil_eighth_root(257), 3);

    for (nat_type n = 0; n < 100000000; ++n) {
      const nat_type root = ceil_eighth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_eighth_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, pow_nat(root + 1, 8));
    }

    for (nat_type n = 100000000000; n < 100000000000 + 10000; ++n) {
      const nat_type root = ceil_eighth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_eighth_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, pow_nat(root + 1, 8));
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 63) - i;
      const nat_type root = ceil_eighth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_eighth_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, pow_nat(root + 1, 8));
    }
  }


  void test__ceil_fourth_root() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(ceil_fourth_root(0), 0);
    TS_ASSERT_EQUALS(ceil_fourth_root(1), 1);
    TS_ASSERT_EQUALS(ceil_fourth_root(2), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(3), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(4), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(5), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(6), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(7), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(8), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(9), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(10), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(15), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(16), 2);
    TS_ASSERT_EQUALS(ceil_fourth_root(17), 3);

    TS_ASSERT_EQUALS(ceil_fourth_root(255), 4);
    TS_ASSERT_EQUALS(ceil_fourth_root(256), 4);
    TS_ASSERT_EQUALS(ceil_fourth_root(257), 5);

    for (nat_type n = 0; n < 100000000; ++n) {
      const nat_type root = ceil_fourth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_fourth_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, pow_nat(root + 1, 4));
    }

    for (nat_type n = 100000000000; n < 100000000000 + 10000; ++n) {
      const nat_type root = ceil_fourth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_fourth_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, pow_nat(root + 1, 4));
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 63) - i;
      const nat_type root = ceil_fourth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_fourth_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, pow_nat(root + 1, 4));
    }
  }


  void test__ceil_square_root() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(ceil_square_root(0), 0);
    TS_ASSERT_EQUALS(ceil_square_root(1), 1);
    TS_ASSERT_EQUALS(ceil_square_root(2), 2);
    TS_ASSERT_EQUALS(ceil_square_root(3), 2);
    TS_ASSERT_EQUALS(ceil_square_root(4), 2);
    TS_ASSERT_EQUALS(ceil_square_root(5), 3);
    TS_ASSERT_EQUALS(ceil_square_root(6), 3);
    TS_ASSERT_EQUALS(ceil_square_root(7), 3);
    TS_ASSERT_EQUALS(ceil_square_root(8), 3);
    TS_ASSERT_EQUALS(ceil_square_root(9), 3);
    TS_ASSERT_EQUALS(ceil_square_root(10), 4);
    TS_ASSERT_EQUALS(ceil_square_root(15), 4);
    TS_ASSERT_EQUALS(ceil_square_root(16), 4);
    TS_ASSERT_EQUALS(ceil_square_root(17), 5);

    TS_ASSERT_EQUALS(ceil_square_root(255), 16);
    TS_ASSERT_EQUALS(ceil_square_root(256), 16);
    TS_ASSERT_EQUALS(ceil_square_root(257), 17);

    for (nat_type n = 0; n < 100000000; ++n) {
      const nat_type root = ceil_square_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_square_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, square(root + 1));
    }

    for (nat_type n = 100000000000; n < 100000000000 + 10000; ++n) {
      const nat_type root = ceil_square_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_square_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, square(root + 1));
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 63) - i;
      const nat_type root = ceil_square_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(floor_square_root(n), n);
      TS_ASSERT_LESS_THAN_EQUALS(n, square(root + 1));
    }
  }


  void test__floor_eighth_root() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(floor_eighth_root(0), 0);
    TS_ASSERT_EQUALS(floor_eighth_root(1), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(2), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(3), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(4), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(5), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(6), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(7), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(8), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(9), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(10), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(15), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(16), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(17), 1);

    TS_ASSERT_EQUALS(floor_eighth_root(255), 1);
    TS_ASSERT_EQUALS(floor_eighth_root(256), 2);
    TS_ASSERT_EQUALS(floor_eighth_root(257), 2);

    for (nat_type n = 0; n < 100000000; ++n) {
      const nat_type root = floor_eighth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(pow_nat(root, 8), n);
      TS_ASSERT_LESS_THAN(n, pow_nat(root + 1, 8));
    }

    for (nat_type n = 100000000000; n < 100000000000 + 10000; ++n) {
      const nat_type root = floor_eighth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(pow_nat(root, 8), n);
      TS_ASSERT_LESS_THAN(n, pow_nat(root + 1, 8));
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 63) - i;
      const nat_type root = floor_eighth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(pow_nat(root, 8), n);
      TS_ASSERT_LESS_THAN(n, pow_nat(root + 1, 8));
    }
  }


  void test__floor_fourth_root() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(floor_fourth_root(0), 0);
    TS_ASSERT_EQUALS(floor_fourth_root(1), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(2), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(3), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(4), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(5), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(6), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(7), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(8), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(9), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(10), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(15), 1);
    TS_ASSERT_EQUALS(floor_fourth_root(16), 2);
    TS_ASSERT_EQUALS(floor_fourth_root(17), 2);

    for (nat_type n = 0; n < 100000000; ++n) {
      const nat_type root = floor_fourth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(pow_nat(root, 4), n);
      TS_ASSERT_LESS_THAN(n, pow_nat(root + 1, 4));
    }

    for (nat_type n = 100000000000; n < 100000000000 + 10000; ++n) {
      const nat_type root = floor_fourth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(pow_nat(root, 4), n);
      TS_ASSERT_LESS_THAN(n, pow_nat(root + 1, 4));
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 63) - i;
      const nat_type root = floor_fourth_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(pow_nat(root, 4), n);
      TS_ASSERT_LESS_THAN(n, pow_nat(root + 1, 4));
    }
  }


  void test__floor_square_root() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sizeof(nat_type), 8);

    TS_ASSERT_EQUALS(floor_square_root(0), 0);
    TS_ASSERT_EQUALS(floor_square_root(1), 1);
    TS_ASSERT_EQUALS(floor_square_root(2), 1);
    TS_ASSERT_EQUALS(floor_square_root(3), 1);
    TS_ASSERT_EQUALS(floor_square_root(4), 2);
    TS_ASSERT_EQUALS(floor_square_root(5), 2);
    TS_ASSERT_EQUALS(floor_square_root(6), 2);
    TS_ASSERT_EQUALS(floor_square_root(7), 2);
    TS_ASSERT_EQUALS(floor_square_root(8), 2);
    TS_ASSERT_EQUALS(floor_square_root(9), 3);
    TS_ASSERT_EQUALS(floor_square_root(10), 3);

    for (nat_type n = 0; n < 100000000; ++n) {
      const nat_type root = floor_square_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(square(root), n);
      TS_ASSERT_LESS_THAN(n, square(root + 1));
    }

    for (nat_type n = 100000000000; n < 100000000000 + 10000; ++n) {
      const nat_type root = floor_square_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(square(root), n);
      TS_ASSERT_LESS_THAN(n, square(root + 1));
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 63) - i;
      const nat_type root = floor_square_root(n);

      TS_ASSERT_LESS_THAN_EQUALS(square(root), n);
      TS_ASSERT_LESS_THAN(n, square(root + 1));
    }
  }


  void test__is_even() {
    SHOW_FUNC_NAME;

    for (nat_type n = 0; n < 10000; n += 2) {
      TS_ASSERT(is_even(n));
    }
    for (nat_type n = 1; n < 10000; n += 2) {
      TS_ASSERT(!is_even(n));
    }
  }


  void test__is_odd() {
    SHOW_FUNC_NAME;

    for (nat_type n = 0; n < 10000; n += 2) {
      TS_ASSERT(!is_odd(n));
    }
    for (nat_type n = 1; n < 10000; n += 2) {
      TS_ASSERT(is_odd(n));
    }
  }


  void test_load_bad_table() {
    SHOW_FUNC_NAME;

    {
      const std::vector<nat_type> list
        = load_bad_table("../../tables/tsv/sequential_check_gentle_3_1000000001.tsv");

      TS_ASSERT_EQUALS(list.size(), 7070);

      TS_ASSERT_EQUALS(list.front(), 2205);
      TS_ASSERT_EQUALS(list.back(), 999934425u);

      for (unsigned int i = 1; i < list.size(); ++i) {
        TS_ASSERT(list[i] % 9 == 0);
        TS_ASSERT_LESS_THAN(list[i - 1], list[i]);
      }
    }

    {
      const std::vector<nat_type>
        list = load_bad_table("../../tables/tsv/sequential_check_gentle_3_5000000001.tsv");

      TS_ASSERT_EQUALS(list.size(), 29517);

      TS_ASSERT_EQUALS(list.front(), 2205);
      TS_ASSERT_EQUALS(list.back(), 4999407525u);

      for (unsigned int i = 1; i < list.size(); ++i) {
        TS_ASSERT(list[i] % 9 == 0);
        TS_ASSERT_LESS_THAN(list[i - 1], list[i]);
      }
    }

    {
      const std::vector<nat_type>
        list = load_bad_table("../../tables/tsv/sequential_check_gentle_5000000001_10000000001.tsv");

      TS_ASSERT_EQUALS(list.size(), 25618);

      TS_ASSERT_EQUALS(list.front(), 5000421825u);
      TS_ASSERT_EQUALS(list.back(), 9999906525u);

      for (unsigned int i = 1; i < list.size(); ++i) {
        TS_ASSERT(list[i] % 9 == 0);
        TS_ASSERT_LESS_THAN(list[i - 1], list[i]);
      }
    }

    {
      const std::vector<nat_type>
        first_list = load_bad_table("../../tables/tsv/sequential_check_gentle_3_5000000001.tsv");

      TS_ASSERT_EQUALS(first_list.size(), 29517);

      TS_ASSERT_EQUALS(first_list.front(), 2205);
      TS_ASSERT_EQUALS(first_list.back(), 4999407525u);

      const std::vector<nat_type>
        list = load_bad_table("../../tables/tsv/sequential_check_gentle_5000000001_10000000001.tsv",
                            first_list);

      TS_ASSERT_EQUALS(list.size(), 29517 + 25618);

      TS_ASSERT_EQUALS(list.front(), 2205);
      TS_ASSERT_EQUALS(list.back(), 9999906525u);

      for (unsigned int i = 1; i < list.size(); ++i) {
        TS_ASSERT(list[i] % 9 == 0);
        TS_ASSERT_LESS_THAN(list[i - 1], list[i]);
      }
    }
  }


  void test_load_numbers() {
    SHOW_FUNC_NAME;

    std::vector<nat_type> list = load_numbers("../../tables/tsv/table_3_1001.tsv");

    TS_ASSERT_EQUALS(list.size(), 500);

    for (unsigned int i = 0; i < list.size(); ++i) {
      const nat_type n = i*2 + 3;

      TS_ASSERT_EQUALS(list[i], n);
    }
  }


  void test__pow_nat() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(pow_nat(0, 0), 1);

    for (nat_type n = 0; n < 1000000; ++n) {
      TS_ASSERT_EQUALS(pow_nat(n, 0), 1);
      TS_ASSERT_EQUALS(pow_nat(n, 1), n);
      TS_ASSERT_EQUALS(pow_nat(n, 2), square(n));
      TS_ASSERT_EQUALS(pow_nat(n, 3), square(n)*n);
      TS_ASSERT_EQUALS(pow_nat(n, 4), square(square(n)));
      TS_ASSERT_EQUALS(pow_nat(n, 5), square(square(n))*n);
      TS_ASSERT_EQUALS(pow_nat(n, 6), square(square(n)*n));
      TS_ASSERT_EQUALS(pow_nat(n, 7), square(square(n)*n)*n);
      TS_ASSERT_EQUALS(pow_nat(n, 8), square(square(square(n))));
    }

    TS_ASSERT_EQUALS(pow_nat(5, 10), 48828125/5);
    TS_ASSERT_EQUALS(pow_nat(5, 11), 48828125);
    TS_ASSERT_EQUALS(pow_nat(5, 12), 48828125*5);

    for (unsigned int i = 0; i < 64; ++i) {
      TS_ASSERT_EQUALS(pow_nat(2, i), (static_cast<nat_type>(1) << i));
    }

    for (unsigned int i = 0; i < 32; ++i) {
      TS_ASSERT_EQUALS(pow_nat(4, i), (static_cast<nat_type>(1) << (i*2)));
    }
  }


  void test__square__double() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(square(0.0), 0.0);
    TS_ASSERT_EQUALS(square(1.0), 1.0);
    TS_ASSERT_EQUALS(square(2.0), 4.0);
    TS_ASSERT_EQUALS(square(3.0), 9.0);

    for (unsigned int i = 0; i < 10000000; ++i) {
      const double x = i/10.0;

      TS_ASSERT_EQUALS(square(x), x*x);

      const double sqr_i = square(static_cast<double>(i));
      TS_ASSERT_EQUALS(sqr_i, static_cast<double>(i)*static_cast<double>(i));
      TS_ASSERT_EQUALS(static_cast<nat_type>(sqr_i), square(static_cast<nat_type>(i)));
    }
  }


  void test__square__nat_type() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(square(static_cast<nat_type>(0)), static_cast<nat_type>(0));
    TS_ASSERT_EQUALS(square(static_cast<nat_type>(1)), static_cast<nat_type>(1));
    TS_ASSERT_EQUALS(square(static_cast<nat_type>(2)), static_cast<nat_type>(4));
    TS_ASSERT_EQUALS(square(static_cast<nat_type>(3)), static_cast<nat_type>(9));

    for (nat_type n = 0; n < 100000; ++n) {
      TS_ASSERT_EQUALS(square(n), static_cast<nat_type>(n)*static_cast<nat_type>(n));
    }
  }


  void test__sum_even() {
    SHOW_FUNC_NAME;

    nat_type sum = 0;

    for (nat_type n = 0; n < 10000000; ++n) {
      if (is_even(n)) {
        sum += n;
      }
      TS_ASSERT_EQUALS(sum_even(n), sum);
    }
  }


  void test__sum_geometric_progression() {
    SHOW_FUNC_NAME;

    for (nat_type r = 1; r < 50; ++r) {
      TS_ASSERT_EQUALS(sum_geometric_progression(r, 0), 1);
      TS_ASSERT_EQUALS(sum_geometric_progression(r, 1), 1 + r);
      TS_ASSERT_EQUALS(sum_geometric_progression(r, 2), 1 + r + r*r);
      TS_ASSERT_EQUALS(sum_geometric_progression(r, 3), 1 + r + r*r + r*r*r);
      TS_ASSERT_EQUALS(sum_geometric_progression(r, 4), 1 + r + r*r + r*r*r + r*r*r*r);
    }

    for (unsigned int k = 0; k <= 100; ++k) {
      TS_ASSERT_EQUALS(sum_geometric_progression(0, k), 1);
      TS_ASSERT_EQUALS(sum_geometric_progression(1, k), k + 1);
      TS_ASSERT_EQUALS(sum_geometric_progression(2, k), pow_nat(2, k + 1) - 1);

      if (k < 41) {
        nat_type sum = 0;

        for (unsigned int i = 0; i <= k; ++i) {
          sum += pow_nat(3, i);
        }

        TS_ASSERT_EQUALS(sum_geometric_progression(3, k), sum);
      }

      TS_ASSERT_EQUALS(sum_geometric_progression(4, k), (pow_nat(4, k + 1) - 1)/3);

      if (k < 28) {
        nat_type sum = 0;

        for (unsigned int i = 0; i <= k; ++i) {
          sum += pow_nat(5, i);
        }

        TS_ASSERT_EQUALS(sum_geometric_progression(5, k), sum);
      }
    }
  }


  void test__sum_geometric_progression_strict() {
    SHOW_FUNC_NAME;

    for (nat_type r = 2; r < 50; ++r) {
      TS_ASSERT_EQUALS(sum_geometric_progression_strict(r, 0), 1);
      TS_ASSERT_EQUALS(sum_geometric_progression_strict(r, 1), 1 + r);
      TS_ASSERT_EQUALS(sum_geometric_progression_strict(r, 2), 1 + r + r*r);
      TS_ASSERT_EQUALS(sum_geometric_progression_strict(r, 3), 1 + r + r*r + r*r*r);
      TS_ASSERT_EQUALS(sum_geometric_progression_strict(r, 4), 1 + r + r*r + r*r*r + r*r*r*r);
    }

    for (unsigned int k = 0; k <= 100; ++k) {
      TS_ASSERT_EQUALS(sum_geometric_progression_strict(2, k), pow_nat(2, k + 1) - 1);

      if (k < 41) {
        nat_type sum = 0;

        for (unsigned int i = 0; i <= k; ++i) {
          sum += pow_nat(3, i);
        }

        TS_ASSERT_EQUALS(sum_geometric_progression_strict(3, k), sum);
      }

      TS_ASSERT_EQUALS(sum_geometric_progression_strict(4, k), (pow_nat(4, k + 1) - 1)/3);

      if (k < 28) {
        nat_type sum = 0;

        for (unsigned int i = 0; i <= k; ++i) {
          sum += pow_nat(5, i);
        }

        TS_ASSERT_EQUALS(sum_geometric_progression_strict(5, k), sum);
      }
    }
  }


  void test__sum_natural() {
    SHOW_FUNC_NAME;

    nat_type sum = 0;

    for (nat_type n = 0; n < 10000000; ++n) {
      sum += n;
      TS_ASSERT_EQUALS(sum_natural(n), sum);
    }
  }


  void test__sum_odd() {
    SHOW_FUNC_NAME;

    nat_type sum = 0;

    for (nat_type n = 0; n < 10000000; ++n) {
      if (is_odd(n)) {
        sum += n;
      }
      TS_ASSERT_EQUALS(sum_odd(n), sum);
    }
  }
};
