/* -*- coding: latin-1 -*- */
/** \file common/tests/test__math_results.hpp (February 14, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cmath>

#include <limits>

#include "test_helper.hpp"

#include "../sigmaodd/divisors.hpp"
#include "../sigmaodd/harmonic.hpp"
#include "../sigmaodd/primes.hpp"
#include "../sigmaodd/sigmaodd.hpp"


using namespace sigmaodd;


const double delta = 1e-15;


class Test__sigmaodd__divisors : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    TS_ASSERT_LESS_THAN_EQUALS(sizeof(nat_type), 8);

    TS_ASSERT(read_primes_table());

    TS_ASSERT_DIFFERS(array_odd_primes_,nullptr);
  }



  // Upper bound for half harmonic number difference
  void test__diff_half_harmonic() {
    SHOW_FUNC_NAME;

    for (nat_type n = 1; n < 100000; ++n) {
      const double diff = harmonic(n) - harmonic(n/2)/2;
      const double bound = diff_half_harmonic_upper_bound(n);
      const double h_odd = harmonic_odd(n);

      TS_ASSERT_LESS_THAN_EQUALS(1, h_odd);
      TS_ASSERT_LESS_THAN_EQUALS(h_odd, diff + delta*1000);  // bad precision!
      TS_ASSERT_LESS_THAN_EQUALS(diff, bound + delta*100);

      if (n > 1) {
        TS_ASSERT_LESS_THAN(bound, std::sqrt(n));
      }

      TS_ASSERT_LESS_THAN(bound, std::pow(n + 2, 1.0/4));
      if (n >= 34) {
        TS_ASSERT_LESS_THAN(bound, std::pow(n, 1.0/4));
      }
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = std::numeric_limits<nat_type>::max() - i;
      const double bound = diff_half_harmonic_upper_bound(n);

      if (n > 1) {
        TS_ASSERT_LESS_THAN(bound, std::sqrt(n));
      }

      TS_ASSERT_LESS_THAN(bound, std::pow(n + 2, 1.0/4));
      if (n >= 34) {
        TS_ASSERT_LESS_THAN(bound, std::pow(n, 1.0/4));
      }
    }
  }


  // Bad upper bound for sigma_odd: sum of odd numbers
  void test__sigma_odd_bad_upper_bound() {
    SHOW_FUNC_NAME;

    for (unsigned int n = 1; n < 100000; ++n) {
      TS_ASSERT_LESS_THAN_EQUALS(sum_odd_divisors__factorize(n), sum_odd(n));
    }

    for (unsigned int i = 1; i < 100000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 32) - i;

      TS_ASSERT_LESS_THAN_EQUALS(sum_odd_divisors__factorize(n), sum_odd(n));
    }
  }


  // Global upper bounds for sigma_odd
  void test__sigma_odd_upper_bound() {
    SHOW_FUNC_NAME;

    for (nat_type n = 1; n < 1000000; ++n) {
      const nat_type sum = sum_odd_divisors__factorize(n);

      const nat_type sum_odd_sqrt = sum_odd(floor_square_root(n));
      const nat_type bound = sum_odd_sqrt + sum_floor_n_harmonic_odd(n, floor_square_root(n - 1));

      const nat_type gub
        = sum_odd_sqrt
        + static_cast<nat_type>(std::floor(harmonic_odd(floor_square_root(n - 1))
                                           * static_cast<double>(n)));
      const nat_type gub_bound = sum_odd_divisors_upper_bound__DeTemple(n);

      TS_ASSERT_LESS_THAN_EQUALS(sum, bound);
      TS_ASSERT_LESS_THAN_EQUALS(sum, gub);
      TS_ASSERT_LESS_THAN_EQUALS(sum, gub_bound);

      TS_ASSERT_LESS_THAN_EQUALS(bound, gub);
      TS_ASSERT_LESS_THAN_EQUALS(bound, gub_bound);

      TS_ASSERT_LESS_THAN_EQUALS(gub, gub_bound);

      if (n > 1) {
        if (n > 3) {
          TS_ASSERT_LESS_THAN(gub,
                              static_cast<nat_type>(std::floor(static_cast<double>(n)
                                                               * (std::pow(n - 1, 1.0/8)
                                                                  + 1.0/2.0))));
        }
        else {
          TS_ASSERT_LESS_THAN_EQUALS(gub,
                                     static_cast<nat_type>(std::floor(static_cast<double>(n)
                                                                      * (std::pow(n - 1, 1.0/8.0)
                                                                         + 1.0/2))));
        }
      }
      TS_ASSERT_LESS_THAN(gub, static_cast<nat_type>(std::floor(std::pow(n, 9.0/8)*2.0)));
    }

    for (unsigned int i = 1; i < 10000; ++i) {
      const nat_type n = (static_cast<nat_type>(1) << 40) - i;
      const nat_type sum = sum_odd_divisors__factorize(n);

      const nat_type sum_odd_sqrt = sum_odd(floor_square_root(n));
      const nat_type bound = sum_odd_sqrt + sum_floor_n_harmonic_odd(n, floor_square_root(n - 1));

      const nat_type gub = sum_odd_sqrt
        + static_cast<nat_type>(std::floor(harmonic_odd(floor_square_root(n - 1))
                                           * static_cast<double>(n)));
      const nat_type gub_bound = sum_odd_divisors_upper_bound__DeTemple(n);

      TS_ASSERT_LESS_THAN_EQUALS(sum, bound);
      TS_ASSERT_LESS_THAN_EQUALS(sum, gub);
      TS_ASSERT_LESS_THAN_EQUALS(sum, gub_bound);

      TS_ASSERT_LESS_THAN_EQUALS(bound, gub);
      TS_ASSERT_LESS_THAN_EQUALS(bound, gub_bound);

      TS_ASSERT_LESS_THAN_EQUALS(gub, gub_bound + 1);  // bad precision!

      TS_ASSERT_LESS_THAN(gub,
                          static_cast<nat_type>(std::floor(static_cast<double>(n)
                                                           * (std::pow(n - 1, 1.0/8) + 1.0/2.0))));
      TS_ASSERT_LESS_THAN(gub, static_cast<nat_type>(std::floor(std::pow(n, 9.0/8)*2)));
    }
  }


  // Sum even expressions
  void test__sum_even() {
    SHOW_FUNC_NAME;

    for (nat_type n = 0; n < 10000; ++n) {
      const nat_type sum = sum_even(n);

      {
        nat_type s = 0;

        for (nat_type i = 1; i <= n; ++i) {
          if (is_even(i)) {
            s += i;
          }
        }

        TS_ASSERT_EQUALS(s, sum);
      }

      {
        nat_type s = 0;

        if (n > 0) {
          for (nat_type i = 1; i <= n/2; ++i) {
            s += i;
          }
        }
        s *= 2;

        TS_ASSERT_EQUALS(s, sum);
      }

      TS_ASSERT_EQUALS((n/2 + 1)*(n/2), sum);
    }
  }


  // Sum odd expressions
  void test__sum_odd() {
    SHOW_FUNC_NAME;

    for (nat_type n = 0; n < 10000; ++n) {
      const nat_type sum = sum_odd(n);

      {
        nat_type s = 0;

        for (nat_type i = 1; i <= n; ++i) {
          if (is_odd(i)) {
            s += i;
          }
        }

        TS_ASSERT_EQUALS(s, sum);
      }

      {
        nat_type s = 0;

        if (n > 0) {
          for (nat_type i = 0; i <= (n - 1)/2; ++i) {
            s += i*2 + 1;
          }
        }

        TS_ASSERT_EQUALS(s, sum);
      }

      TS_ASSERT_EQUALS(square((n + 1)/2), sum);
    }
  }
};
