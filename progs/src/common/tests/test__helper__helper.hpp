/* -*- coding: latin-1 -*- */
/** \file common/tests/test__helper__helper.hpp (January 8, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "test_helper.hpp"

#include "../helper/helper.hpp"


using namespace helper;


class Test__helper__helper : public CxxTest::TestSuite {
public:
  void test__PRINT_LIST() {
    SHOW_FUNC_NAME;

    PRINT_LIST(std::vector<unsigned int>({1, 2, 3}));
  }


  void test__STR() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(__CHAR_BIT__, 8);
    TS_ASSERT_EQUALS(STR(__CHAR_BIT__), "8");
  }


  void test__STRINGIFY() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(STRINGIFY(__CHAR_BIT__), "__CHAR_BIT__");
    TS_ASSERT_EQUALS(STRINGIFY(__FILE__), "__FILE__");
    TS_ASSERT_EQUALS(STRINGIFY(__LINE__), "__LINE__");
  }


  void test__TSV() {
    SHOW_FUNC_NAME;

    // TSV(1);  // commented to avoid warning "at least one argument for '...' parameter"
    TSV(1, 2);
    TSV(1, 2, 3);
    TSV(1, 2, 3, 4);
    TSV(1, 2, 3, 4, 5);
    TSV(1, 2, 3, 4, 5, 6);
    TSV(1, 2, 3, 4, 5, 6, 7);
    TSV(1, 2, 3, 4, 5, 6, 7, 8);
    TSV(1, 2, 3, 4, 5, 6, 7, 8, 9);
    TSV(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
  }


  void test__concat_path() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(concat_path("", ""), "");
    TS_ASSERT_EQUALS(concat_path("path", ""), "path");
    TS_ASSERT_EQUALS(concat_path("", "file"), "file");

    TS_ASSERT_EQUALS(concat_path("path", "file"), "path/file");
    TS_ASSERT_EQUALS(concat_path("path/", "file"), "path/file");
    TS_ASSERT_EQUALS(concat_path("path", "/file"), "path/file");
    TS_ASSERT_EQUALS(concat_path("path/", "/file"), "path/file");
  }


  void test__duration_ms_to_string() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(duration_ms_to_string(0),
                     "0.000000ms\t= 0.000000s\t= 0.000000m\t= 0.000000h");
    TS_ASSERT_EQUALS(duration_ms_to_string(3600000),
                     "3600000.000000ms\t= 3600.000000s\t= 60.000000m\t= 1.000000h");
  }


  void test__duration_to_string() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(duration_to_string(std::chrono::duration<double>(0)),
                     "0.000000ms\t= 0.000000s\t= 0.000000m\t= 0.000000h");
    TS_ASSERT_EQUALS(duration_to_string(std::chrono::duration<double>(3600)),
                     "3600000.000000ms\t= 3600.000000s\t= 60.000000m\t= 1.000000h");
  }


  void test__file_to_string() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(file_to_string("tests/empty.txt"), "");
    TS_ASSERT_EQUALS(file_to_string("tests/hello.txt"), "Hello\nworld\n!\n");
  }


  void test__get_string() {
    SHOW_FUNC_NAME;

    const char* const argv[] = {"prog", "42", " 42 ", "-42"};

    TS_ASSERT_EQUALS(get_string(4, argv, 1), "42");
    TS_ASSERT_EQUALS(get_string(4, argv, 2), " 42 ");
    TS_ASSERT_EQUALS(get_string(4, argv, 3), "-42");

    TS_ASSERT_EQUALS(get_string(3, argv, 1), "42");
    TS_ASSERT_EQUALS(get_string(3, argv, 2), " 42 ");
    TS_ASSERT_EQUALS(get_string(3, argv, 3), "");

    TS_ASSERT_EQUALS(get_string(2, argv, 1), "42");
    TS_ASSERT_EQUALS(get_string(2, argv, 2), "");
    TS_ASSERT_EQUALS(get_string(2, argv, 3), "");
  }


  void test__get_uint() {
    SHOW_FUNC_NAME;

    const char* const argv[] = {"prog", "42", " 42 ", "-42"};

    TS_ASSERT_EQUALS(get_uint(4, argv, 1), 42);
    TS_ASSERT_EQUALS(get_uint(4, argv, 2), 42);
    TS_ASSERT_EQUALS(get_uint(4, argv, 3), -static_cast<unsigned int>(42));

    TS_ASSERT_EQUALS(get_uint(3, argv, 1), 42);
    TS_ASSERT_EQUALS(get_uint(3, argv, 2), 42);
    TS_ASSERT_EQUALS(get_uint(3, argv, 3), 0);

    TS_ASSERT_EQUALS(get_uint(2, argv, 1), 42);
    TS_ASSERT_EQUALS(get_uint(2, argv, 2), 0);
    TS_ASSERT_EQUALS(get_uint(2, argv, 3), 0);
  }


  void test__get_ulong() {
    SHOW_FUNC_NAME;

    const char* const argv[] = {"prog", "42", " 42 ", "-42"};

    TS_ASSERT_EQUALS(get_ulong(4, argv, 1), 42);
    TS_ASSERT_EQUALS(get_ulong(4, argv, 2), 42);
    TS_ASSERT_EQUALS(get_ulong(4, argv, 3), -static_cast<unsigned long>(42));

    TS_ASSERT_EQUALS(get_ulong(3, argv, 1), 42);
    TS_ASSERT_EQUALS(get_ulong(3, argv, 2), 42);
    TS_ASSERT_EQUALS(get_ulong(3, argv, 3), 0);

    TS_ASSERT_EQUALS(get_ulong(2, argv, 1), 42);
    TS_ASSERT_EQUALS(get_ulong(2, argv, 2), 0);
    TS_ASSERT_EQUALS(get_ulong(2, argv, 3), 0);
  }


  void test__is_file_exists() {
    SHOW_FUNC_NAME;

    TS_ASSERT(is_file_exists("tests/test__helper__helper.hpp"));
    TS_ASSERT(is_file_exists("tests/"));
    TS_ASSERT(is_file_exists("tests"));
    TS_ASSERT(!is_file_exists("ZZZ"));
  }

  void test__print_intern_config_compiler() {
    SHOW_FUNC_NAME;

    print_intern_config_compiler();
  }


  void test__remove_last_if_null() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(remove_last_if_null(""), "");
    TS_ASSERT_EQUALS(remove_last_if_null("\0"), "");

    TS_ASSERT_EQUALS(remove_last_if_null("Hello"), "Hello");
    TS_ASSERT_EQUALS(remove_last_if_null("Hello\0"), "Hello");

    TS_ASSERT_EQUALS(remove_last_if_null("Hell\0"), "Hell\0");
    TS_ASSERT_EQUALS(remove_last_if_null("Hell\0\0"), "Hell\0");
  }


  void test__to_string() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(to_string(false), "false");
    TS_ASSERT_EQUALS(to_string(true), "true");
  }

};
