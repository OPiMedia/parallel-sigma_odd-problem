/* -*- coding: latin-1 -*- */
/** \file common/tests/test_helper.hpp (December 19, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cxxtest/TestSuite.h>  // CxxTest http://cxxtest.com/

#include <cstdbool>

#include <iostream>

#include "../helper/helper.hpp"


#define NOT_COVERED {                           \
    std::cout << "NOT COVERED!" << std::endl;   \
    std::cout.flush();                          \
  }


#define SHOW_FUNC_NAME {                                          \
    if (tests_enable) {                                           \
      std::cout << std::endl                                      \
                << "=== " << __func__ << " ===" << std::endl;     \
      std::cout.flush();                                          \
    }                                                             \
    else {                                                        \
      return;                                                     \
    }                                                             \
  }


#define SHOW_INIT {                                             \
    std::cout << std::endl                                      \
              << "--- " << __func__ << " ---" << std::endl;     \
    std::cout.flush();                                          \
  }


#ifndef TESTS_SKIP
bool tests_enable = true;
#else
bool tests_enable = false;
#endif


#define TEST_THIS bool tests_enable = true;
