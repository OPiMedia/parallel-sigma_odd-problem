/* -*- coding: latin-1 -*- */
/** \file common/tests/test__sigmaodd__sigmaodd.hpp (January 5, 2018)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#include "test_helper.hpp"

#include "../sigmaodd/sigmaodd.hpp"
#include "../sigmaodd/primes.hpp"


using namespace sigmaodd;


class Test__sigmaodd__sigmaodd : public CxxTest::TestSuite {
public:
  void test_init() {
    SHOW_INIT;

    TS_ASSERT(read_primes_table());

    TS_ASSERT_DIFFERS(array_odd_primes_,nullptr);
  }


  void test__check_varsigma_odd() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__path_to_string() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_long() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_path() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_path__iterators() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_path_infos() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_sigmaodd__factorize() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_sigmaodd__factorize_bound() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_sigmaodd__naive() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__print_square() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__sum_odd_divisors_divided_until_odd__euler() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__sum_odd_divisors_divided_until_odd__factorize() {
    SHOW_FUNC_NAME;

    // Same results that naive method
    for (nat_type n = 1; n < 10000; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd__factorize(n),
                       sum_odd_divisors_divided_until_odd__naive(n));
    }

    for (nat_type n = 1000000; n < 1000100; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd__factorize(n),
                       sum_odd_divisors_divided_until_odd__naive(n));
    }

    for (nat_type n = 10000000; n < 10000010; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd__factorize(n),
                       sum_odd_divisors_divided_until_odd__naive(n));
    }
  }


  void test__sum_odd_divisors_divided_until_odd__factorize_bound() {
    SHOW_FUNC_NAME;

      NOT_COVERED;
  }


  void test__sum_odd_divisors_divided_until_odd__naive() {
    SHOW_FUNC_NAME;

    // Same results that with factorize method without divide by 2
    for (nat_type n = 1; n < 10000; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd__naive(n),
                       sum_odd_divisors_divided_until_odd__factorize(n));
    }

    for (nat_type n = 1000000; n < 1000100; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd__naive(n),
                       sum_odd_divisors_divided_until_odd__factorize(n));
    }

    for (nat_type n = 10000000; n < 10000010; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd__naive(n),
                       sum_odd_divisors_divided_until_odd__factorize(n));
    }
  }


  void test__sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize() {
    SHOW_FUNC_NAME;

    // Same results that naive method
    for (nat_type n = 2; n < 10000; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize(n),
                       sum_odd_divisors_divided_until_odd_iterate_until_lower__naive(n));
    }

    for (nat_type n = 1000000; n < 1000100; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize(n),
                       sum_odd_divisors_divided_until_odd_iterate_until_lower__naive(n));
    }

    for (nat_type n = 10000000; n < 10000010; ++n) {
      TS_ASSERT_EQUALS(sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize(n),
                       sum_odd_divisors_divided_until_odd_iterate_until_lower__naive(n));
    }
  }


  void test__sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize_bound() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__sum_odd_divisors_divided_until_odd_iterate_until_lower__naive() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__varsum_odd() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }


  void test__varsum_odd_big() {
    SHOW_FUNC_NAME;

    NOT_COVERED;
  }

};
