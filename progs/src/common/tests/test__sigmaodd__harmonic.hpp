/* -*- coding: latin-1 -*- */
/** \file common/tests/test__sigmaodd__harmonic.hpp (August 30, 2020)
 *
 * GPLv3 --- Copyright (C) 2017, 2018, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#include <cassert>
#include <cfenv>
#include <cmath>

#include <iomanip>
#include <limits>
#include <sstream>

#include "test_helper.hpp"

#include "../sigmaodd/harmonic.hpp"


using namespace sigmaodd;


const double delta = 1e-15;


// http://oeis.org/A001008/b001008.txt
const long double H_500_numerator = 6633384299891198065461433023874214660151383488987829406868700907802279376986364154005690172480537248349310365871218591743641116766728139494727637850449054802989613344274500453825922847052235859615378238909694581687099.0l;

const long double H_1000_numerator = 53362913282294785045591045624042980409652472280384260097101349248456268889497101757506097901985035691409088731550468098378442172117885009464302344326566022502100278425632852081405544941210442510142672770294774712708917963967779610453224692426866468888281582071984897105110796873249319155529397017508931564519976085734473014183284011724412280649074307703736683170055800293659235088589360235285852808160759574737836655413175508131522517.0l;


// http://oeis.org/A002805/b002805.txt
const long double H_500_denominator = 976528297586037954584851660253897317730151176683856787284655867127950765610716178591036797598551026470244168088645451676177520177514977827924165875515464044694152207479405310883385229609852607806002629415184926954240.0l;

const long double H_1000_denominator = 7128865274665093053166384155714272920668358861885893040452001991154324087581111499476444151913871586911717817019575256512980264067621009251465871004305131072686268143200196609974862745937188343705015434452523739745298963145674982128236956232823794011068809262317708861979540791247754558049326475737829923352751796735248042463638051137034331214781746850878453485678021888075373249921995672056932029099390891687487672697950931603520000.0l;


const double H_500 = static_cast<double>(H_500_numerator/H_500_denominator);

const double H_1000 = static_cast<double>(H_1000_numerator/H_1000_denominator);



/*
 * Lower bound of H_n
 * from Young 1991 inequality: http://mathworld.wolfram.com/HarmonicNumber.html
 */
double
harmonic_lower_bound__Young_(sigmaodd::nat_type n);

double
harmonic_lower_bound__Young_(sigmaodd::nat_type n) {
  assert(n != 0);

  const int round = fegetround();

  fesetround(FE_DOWNWARD);

  const double bound = (std::log(n)
                        + euler_gamma
                        + 1.0/static_cast<double>((n + 1)*2));

  fesetround(round);

  return bound;
}


/*
 * Upper bound of H_n
 * from Young 1991 inequality: http://mathworld.wolfram.com/HarmonicNumber.html
 */
double
harmonic_upper_bound__Young_(sigmaodd::nat_type n);

double
harmonic_upper_bound__Young_(sigmaodd::nat_type n) {
  assert(n != 0);

  const int round = fegetround();

  fesetround(FE_UPWARD);

  const double bound = (std::log(n)
                        + euler_gamma
                        + 1.0/static_cast<double>(n*2));

  fesetround(round);

  return bound;
}


class Test__sigmaodd__harmonic : public CxxTest::TestSuite {
public:
  void test__euler_gamma() {
    SHOW_FUNC_NAME;

    std::stringstream stream;

    stream << std::setprecision(100) << euler_gamma;

    const std::string s = stream.str();

    std::cout << "Euler-Mascheroni constant 0.577215664901532860606512090082402431042..."
              << std::endl
              << "            < euler_gamma " << s << std::endl;

    TS_ASSERT_LESS_THAN_EQUALS("0.577215664901532860606512090082402431042", s);
  }


  void test__diff_half_harmonic_upper_bound__a_b() {
    SHOW_FUNC_NAME;

    for (nat_type a = 1; a < 10000; ++a) {
      const double h_a = harmonic(a);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_500/2,
                                 diff_half_harmonic_upper_bound(a, 500) + delta*100);
      TS_ASSERT_LESS_THAN_EQUALS(H_500 - h_a/2,
                                 diff_half_harmonic_upper_bound(500, a) + delta*100);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_1000/2,
                                 diff_half_harmonic_upper_bound(a, 1000) + delta*10);
      TS_ASSERT_LESS_THAN_EQUALS(H_1000 - h_a/2,
                                 diff_half_harmonic_upper_bound(1000, a) + delta*100);

      for (nat_type b = 1; b < 100; ++b) {
        TS_ASSERT_LESS_THAN_EQUALS(h_a - harmonic(b)/2,
                                   diff_half_harmonic_upper_bound(a, b) + delta*10);

        TS_ASSERT_LESS_THAN_EQUALS(harmonic(b) - h_a/2,
                                   diff_half_harmonic_upper_bound(b, a) + delta*100);
      }
    }

    for (nat_type a = 1000000; a < 1000000 + 100; ++a) {
      const double h_a = harmonic(a);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_500/2,
                                 diff_half_harmonic_upper_bound(a, 500));
      TS_ASSERT_LESS_THAN_EQUALS(H_500 - h_a/2,
                                 diff_half_harmonic_upper_bound(500, a) + delta*1000);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_1000/2,
                                 diff_half_harmonic_upper_bound(a, 1000));
      TS_ASSERT_LESS_THAN_EQUALS(H_1000 - h_a/2,
                                 diff_half_harmonic_upper_bound(1000, a) + delta*1000);

      for (nat_type b = 1; b < 100; ++b) {
        TS_ASSERT_LESS_THAN_EQUALS(h_a - harmonic(b)/2,
                                   diff_half_harmonic_upper_bound(a, b));

        TS_ASSERT_LESS_THAN_EQUALS(harmonic(b) - h_a/2,
                                   diff_half_harmonic_upper_bound(b, a) + delta*1000);
      }
    }
  }


  void test__diff_half_harmonic_upper_bound__n() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(harmonic(1) - harmonic(1/2)/2, 1);
    TS_ASSERT_LESS_THAN_EQUALS(1, diff_half_harmonic_upper_bound(1));

    TS_ASSERT_EQUALS(harmonic(2) - harmonic(2/2)/2, 1);
    TS_ASSERT_LESS_THAN_EQUALS(1, diff_half_harmonic_upper_bound(2));

    TS_ASSERT_LESS_THAN_EQUALS(H_1000 - H_500/2,
                               diff_half_harmonic_upper_bound(1000));

    for (nat_type n = 1; n < 10000; ++n) {
      const double h_odd = harmonic_odd(n);
      const double bound = diff_half_harmonic_upper_bound(n);
      const double common_bound
        = ((std::log(static_cast<double>(square(n*2 + 1)) / static_cast<double>(n*2))
            + euler_gamma) / 2.0
           - (1.0/static_cast<double>(square(n + 2))
              - 1.0/static_cast<double>(square(n))) / 12.0);

      TS_ASSERT_LESS_THAN_EQUALS(1, h_odd);
      TS_ASSERT_LESS_THAN(h_odd, bound + delta*10);
      TS_ASSERT_DELTA(bound, harmonic(n) - harmonic(n/2)/2, 0.01);  // bad precision!
      TS_ASSERT_LESS_THAN(bound, common_bound);
      TS_ASSERT_LESS_THAN(common_bound, std::pow(n + 2, 1.0/4.0));
      if (n >= 34) {
        TS_ASSERT_LESS_THAN(common_bound, std::pow(n, 1.0/4.0));
      }
    }

    for (nat_type n = 1000000; n < 1000000 + 1000; ++n) {
      const double h_odd = harmonic_odd(n);
      const double bound = diff_half_harmonic_upper_bound(n);
      const double common_bound
        = ((std::log(static_cast<double>(square(n*2 + 1)) / static_cast<double>(n*2))
            + euler_gamma) / 2.0
           - (1.0/static_cast<double>(square(n + 2))
              - 1.0/static_cast<double>(square(n))) / 12.0);

      TS_ASSERT_LESS_THAN_EQUALS(1, h_odd);
      TS_ASSERT_LESS_THAN(h_odd, bound + delta*1000);
      TS_ASSERT_DELTA(bound, harmonic(n) - harmonic(n/2)/2, 0.2);  // very bad precision!
      TS_ASSERT_LESS_THAN(bound, common_bound + delta*100);
      TS_ASSERT_LESS_THAN(common_bound, std::pow(n + 2, 1.0/4.0));
      if (n >= 34) {
        TS_ASSERT_LESS_THAN(common_bound, std::pow(n, 1.0/4.0));
      }
    }
  }


  void test__diff_harmonic_upper_bound() {
    SHOW_FUNC_NAME;

    for (nat_type a = 1; a < 10000; ++a) {
      const double h_a = harmonic(a);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_500,
                                 diff_harmonic_upper_bound(a, 500) + delta*100);
      TS_ASSERT_LESS_THAN_EQUALS(H_500 - h_a,
                                 diff_harmonic_upper_bound(500, a) + delta*100);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_1000,
                                 diff_harmonic_upper_bound(a, 1000) + delta*100);
      TS_ASSERT_LESS_THAN_EQUALS(H_1000 - h_a,
                                 diff_harmonic_upper_bound(1000, a) + delta*100);

      for (nat_type b = 1; b < 100; ++b) {
        TS_ASSERT_LESS_THAN_EQUALS(h_a - harmonic(b),
                                   diff_harmonic_upper_bound(a, b) + delta*10);

        TS_ASSERT_LESS_THAN_EQUALS(harmonic(b) - h_a,
                                   diff_harmonic_upper_bound(b, a) + delta*100);
      }
    }

    for (nat_type a = 1000000; a < 1000000 + 100; ++a) {
      const double h_a = harmonic(a);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_500,
                                 diff_harmonic_upper_bound(a, 500));
      TS_ASSERT_LESS_THAN_EQUALS(H_500 - h_a,
                                 diff_harmonic_upper_bound(500, a) + delta*1000);

      TS_ASSERT_LESS_THAN_EQUALS(h_a - H_1000,
                                 diff_harmonic_upper_bound(a, 1000));
      TS_ASSERT_LESS_THAN_EQUALS(H_1000 - h_a,
                                 diff_harmonic_upper_bound(1000, a) + delta*1000);

      for (nat_type b = 1; b < 100; ++b) {
        TS_ASSERT_LESS_THAN_EQUALS(h_a - harmonic(b),
                                   diff_harmonic_upper_bound(a, b));

        TS_ASSERT_LESS_THAN_EQUALS(harmonic(b) - h_a,
                                   diff_harmonic_upper_bound(b, a) + delta*1000);
      }
    }
  }


  void test__harmonic() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(harmonic(0), 0);

    // http://oeis.org/A001008
    // / http://oeis.org/A002805
    // or see in ../../miscellaneous/harmonic/
    TS_ASSERT_EQUALS(harmonic(1), 1);
    TS_ASSERT_EQUALS(harmonic(2), 3.0/2);
    TS_ASSERT_EQUALS(harmonic(3), 11.0/6);

    TS_ASSERT_DELTA(harmonic(4), 25.0/12, delta);
    TS_ASSERT_DELTA(harmonic(5), 137.0/60, delta);
    TS_ASSERT_DELTA(harmonic(6), 49.0/20, delta);
    TS_ASSERT_DELTA(harmonic(7), 363.0/140, delta);

    TS_ASSERT_DELTA(harmonic(10), 7381.0/2520, delta);
    TS_ASSERT_DELTA(harmonic(20), 55835135.0/15519504, delta);
    TS_ASSERT_DELTA(harmonic(29), 9227046511387.0/2329089562800, delta);

    // http://oeis.org/A001008/b001008.txt
    // / http://oeis.org/A002805/b002805.txt
    TS_ASSERT_DELTA(harmonic(30), 9304682830147.0/2329089562800, delta);
    TS_ASSERT_DELTA(harmonic(40), 2078178381193813.0/485721041551200, delta);
    TS_ASSERT_DELTA(harmonic(50), 13943237577224054960759.0/3099044504245996706400.0, delta*10);
    TS_ASSERT_DELTA(harmonic(100),
                    14466636279520351160221518043104131447711.0
                    / 2788815009188499086581352357412492142272.0,
                    delta);
    TS_ASSERT_DELTA(harmonic(500), H_500, delta*10);
    TS_ASSERT_DELTA(harmonic(1000), H_1000, delta*10);

    double h = 0;

    for (nat_type n = 0; n < 10000; ++n) {
      if (n != 0) {
        h += 1.0/static_cast<double>(n);
      }

      const double h_n = harmonic(n);

      TS_ASSERT_EQUALS(h_n, h);
      TS_ASSERT_DELTA(h_n, harmonic_even(n) + harmonic_odd(n), delta*100);
    }

    for (nat_type n = 1000000; n < 1000000 + 100; ++n) {
      TS_ASSERT_DELTA(harmonic(n), harmonic_even(n) + harmonic_odd(n), delta*1000);
    }
  }


  void test__harmonic_even() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(harmonic_even(0), 0);
    TS_ASSERT_EQUALS(harmonic_even(1), 0);

    TS_ASSERT_EQUALS(harmonic_even(2), 1.0/2);
    TS_ASSERT_EQUALS(harmonic_even(3), 1.0/2);

    TS_ASSERT_EQUALS(harmonic_even(4), 3.0/4);
    TS_ASSERT_EQUALS(harmonic_even(5), 3.0/4);

    TS_ASSERT_EQUALS(harmonic_even(6), 11.0/12);
    TS_ASSERT_EQUALS(harmonic_even(7), 11.0/12);

    TS_ASSERT_DELTA(harmonic_even(8), 25.0/24, delta);
    TS_ASSERT_DELTA(harmonic_even(9), 25.0/24, delta);

    TS_ASSERT_DELTA(harmonic_even(10), 137.0/120, delta);
    TS_ASSERT_DELTA(harmonic_even(11), 137.0/120, delta);

    double h = 0;

    for (nat_type n = 0; n < 10000; ++n) {
      if (is_even(n) && (n != 0)) {
        h += 1.0/static_cast<double>(n);
      }

      const double h_n = harmonic_even(n);

      TS_ASSERT_EQUALS(h_n, h);
      TS_ASSERT_EQUALS(h_n, harmonic(n/2)/2);  // = 1/2 H_{n/2}
    }

    for (nat_type n = 1000000; n < 1000000 + 100; ++n) {
      TS_ASSERT_EQUALS(harmonic_even(n), harmonic(n/2)/2);  // = 1/2 H_{n/2}
    }


    for (nat_type n = 0; n < 10000; n += 2) {
      TS_ASSERT_EQUALS(harmonic_even(n + 1), harmonic_even(n));
    }

    for (nat_type n = 1000000; n < 1000000 + 100; n += 2) {
      TS_ASSERT_EQUALS(harmonic_even(n + 1), harmonic_even(n));
    }
  }


  void test__harmonic_odd() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(harmonic_odd(0), 0);

    TS_ASSERT_EQUALS(harmonic_odd(1), 1);
    TS_ASSERT_EQUALS(harmonic_odd(2), 1);

    TS_ASSERT_EQUALS(harmonic_odd(3), 4.0/3);
    TS_ASSERT_EQUALS(harmonic_odd(4), 4.0/3);

    TS_ASSERT_DELTA(harmonic_odd(5), 23.0/15, delta);
    TS_ASSERT_DELTA(harmonic_odd(6), 23.0/15, delta);

    TS_ASSERT_DELTA(harmonic_odd(7), 176.0/105, delta);
    TS_ASSERT_DELTA(harmonic_odd(8), 176.0/105, delta);

    TS_ASSERT_DELTA(harmonic_odd(9), 563.0/315, delta);
    TS_ASSERT_DELTA(harmonic_odd(10), 563.0/315, delta);

    double h = 0;

    for (nat_type n = 0; n < 10000; ++n) {
      if (is_odd(n)) {
        h += 1.0/static_cast<double>(n);
      }

      TS_ASSERT_EQUALS(harmonic_odd(n), h);
    }

    for (nat_type n = 1; n < 10000; n += 2) {
      TS_ASSERT_EQUALS(harmonic_odd(n + 1), harmonic_odd(n));
    }

    for (nat_type n = 1000001; n < 1000000 + 100; n += 2) {
      TS_ASSERT_EQUALS(harmonic_odd(n + 1), harmonic_odd(n));
    }
  }


  void test__harmonic_lower_bound() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(harmonic_lower_bound(500), H_500);
    TS_ASSERT_LESS_THAN_EQUALS(harmonic_lower_bound(1000), H_1000);

    for (nat_type n = 1; n < 10000; ++n) {
      TS_ASSERT_LESS_THAN_EQUALS(harmonic_lower_bound(n), harmonic(n) + delta*100);

      // Better lower bound than Young lower bound
      TS_ASSERT_LESS_THAN_EQUALS(harmonic_lower_bound__Young_(n), harmonic_lower_bound(n));
    }

    for (nat_type n = 1000000; n < 1000000 + 100; ++n) {
      TS_ASSERT_LESS_THAN_EQUALS(harmonic_lower_bound(n) - delta*1000, harmonic(n));  // bad precision!

      // Better lower bound than Young lower bound
      TS_ASSERT_LESS_THAN_EQUALS(harmonic_lower_bound__Young_(n), harmonic_lower_bound(n));
    }
  }


  void test__harmonic_upper_bound() {
    SHOW_FUNC_NAME;

    TS_ASSERT_LESS_THAN_EQUALS(H_500, harmonic_upper_bound(500));
    TS_ASSERT_LESS_THAN_EQUALS(H_1000, harmonic_upper_bound(1000));

    for (nat_type n = 1; n < 10000; ++n) {
      TS_ASSERT_LESS_THAN_EQUALS(harmonic(n), harmonic_upper_bound(n) + delta*10);

      // Better upper bound than Young upper bound
      TS_ASSERT_LESS_THAN_EQUALS(harmonic_upper_bound(n), harmonic_upper_bound__Young_(n));
    }

    for (nat_type n = 1000000; n < 1000000 + 100; ++n) {
      TS_ASSERT_LESS_THAN_EQUALS(harmonic(n), harmonic_upper_bound(n));

      // Better upper bound than Young upper bound
      TS_ASSERT_LESS_THAN_EQUALS(harmonic_upper_bound(n), harmonic_upper_bound__Young_(n));
    }
  }


  void test__sum_floor_n_harmonic_odd() {
    SHOW_FUNC_NAME;

    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(0, 0), 0);

    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(1, 1), 1);
    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(2, 2), 2);

    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(3, 3), 4);
    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(4, 4), 5);

    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(5, 5), 7);
    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(6, 6), 9);

    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(7, 7), 11);
    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(8, 8), 12);

    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(9, 9), 15);
    TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(10, 10), 17);

    for (nat_type n = 0; n < 10000; ++n) {
      if (n == 0) {
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(1, n), 0);
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(2, n), 0);
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(3, n), 0);
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(4, n), 0);
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(5, n), 0);
      }
      else {
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(1, n), 1);
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(2, n), 2);
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(3, n), 3 + (n >= 3 ? 1 : 0));
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(4, n), 4 + (n >= 3 ? 1 : 0));
        TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(5, n), 5 + (n >= 3 ? (n >= 5 ? 2 : 1) : 0));
      }

      nat_type h = 0;

      for (nat_type i = 1; i <= n; i += 2) {
        h += static_cast<nat_type>(std::floor(static_cast<double>(n)*1.0/static_cast<double>(i)));
      }

      TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(n, n), h);
    }

    for (nat_type n = 1; n < 10000; n += 2) {
      TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(n, n + 1), sum_floor_n_harmonic_odd(n, n));
    }

    for (nat_type n = 1000001; n < 1000000 + 100; n += 2) {
      TS_ASSERT_EQUALS(sum_floor_n_harmonic_odd(n, n + 1), sum_floor_n_harmonic_odd(n, n));
    }
  }

};
