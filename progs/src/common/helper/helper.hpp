/* -*- coding: latin-1 -*- */
/** \file common/helper/helper.hpp (August 9, 2020)
 * \brief
 * Some generic helper functions for programs.
 *
 * GPLv3 --- Copyright (C) 2017, 2018, 2020 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_COMMON_HELPER_HELPER_HPP_
#define PROGS_SRC_COMMON_HELPER_HELPER_HPP_

// \cond
#include <chrono>
#include <iostream>
#include <string>
// \endcond



/* ****************
 * Private macros *
 ******************/

/** \brief
 * From "Variadic macros tricks"
 * https://codecraft.co/2014/11/25/variadic-macros-tricks/
 */
#define GET_OVERLOAD_(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, NAME, ...) NAME


#define TSV1_(a) {                              \
    std::cerr << (a) << std::endl;              \
  }

#define TSV2_(a, b) {                                   \
    std::cerr << (a) << '\t' << (b) << std::endl;       \
  }

#define TSV3_(a, b, c) {                                                \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << std::endl;        \
  }

#define TSV4_(a, b, c, d) {                                             \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << std::endl; \
  }

#define TSV5_(a, b, c, d, e) {                                          \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << std::endl; \
  }

#define TSV6_(a, b, c, d, e, f) {                                       \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << std::endl;                                      \
  }

#define TSV7_(a, b, c, d, e, f, g) {                                    \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << std::endl;                       \
  }

#define TSV8_(a, b, c, d, e, f, g, h) {                                 \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << '\t' << (h) << std::endl;        \
  }

#define TSV9_(a, b, c, d, e, f, g, h, i) {                              \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << '\t' << (h) << '\t' << (i) << std::endl; \
  }

#define TSV10_(a, b, c, d, e, f, g, h, i, j) {                          \
    std::cerr << (a) << '\t' << (b) << '\t' << (c) << '\t' << (d) << '\t' << (e) << '\t' \
              << (f) << '\t' << (g) << '\t' << (h) << '\t' << (i) << '\t' << (j) << std::endl; \
  }



/* ********
 * Macros *
 **********/

/** \brief
 * Print to stderr the element of the list l, separated by comma.
 *
 * (Useful during development phase.)
 *
 * @param l iterable
 */
#define PRINT_LIST(l) {                         \
    std::cerr << '(' << (l).size() << ')';      \
    for (auto x : (l)) {                        \
      std::cerr << ',' << x;                    \
    }                                           \
    std::cerr << std::endl;                     \
  }


/** \brief
 * Convert the value of the macro x to a literal string.
 *
 * @param x macro
 */
#define STR(x) STRINGIFY(x)

/** \brief
 * Convert the name of the macro x to a literal string.
 *
 * @param x macro
 */
#define STRINGIFY(x) #x



/** \brief
 * Some generic helper functions for programs.
 */
namespace helper {

  /* ************
   * Prototypes *
   **************/

  /** \brief
   * Return the path composed by path1/path2.
   *
   * @param path1 a valid path
   * @param path2 a valid relative path
   */
  std::string
  concat_path(std::string path1, std::string path2);


  /** \brief
   * Return a string with the duration expressed in milliseconds, seconds, minutes and hours,
   * separated by tabulation.
   *
   * @return "_ms\t= _s\t= _m\t= _h"
   */
  std::string
  duration_ms_to_string(double duration_ms);


  /** \brief
   * Return a string with the duration expressed in milliseconds, seconds, minutes and hours,
   * separated by tabulation.
   *
   * @return "_ms\t= _s\t= _m\t= _h"
   */
  std::string
  duration_to_string(std::chrono::duration<double> duration_second);


  /** \brief
   * Read the file and return its content to a string.
   * If failed then print a error message and exit.
   */
  std::string
  file_to_string(std::string filename);


  /** \brief
   * Return argv[i] converted in string.
   *
   * If (help_and_exit_function != nullptr) and (i is not a valid index)
   * then call help_and_exit_function().
   */
  std::string
  get_string(int argc, const char* const argv[], unsigned int i,
             void (*help_and_exit_function)() = nullptr);


  /** \brief
   * Return argv[i] converted in integer.
   *
   * If (help_and_exit_function != nullptr) and (i is not a valid index)
   * then call help_and_exit_function().
   */
  unsigned int
  get_uint(int argc, const char* const argv[], unsigned int i,
           void (*help_and_exit_function)() = nullptr);


  /** \brief
   * Return argv[i] converted in integer.
   *
   * If (help_and_exit_function != nullptr) and (i is not a valid index)
   * then call help_and_exit_function().
   */
  unsigned long
  get_ulong(int argc, const char* const argv[], unsigned int i,
            void (*help_and_exit_function)() = nullptr);


  /** \brief
   * Return true iff the file (or directory) exists.
   */
  bool
  is_file_exists(std::string filename);


  /** \brief
   * Print to stdout the intern configuration of the compiler.
   */
  void
  print_intern_config_compiler();


  /** \brief
   * If s terminates by a null character
   * then return a copy of s without this last character,
   * else return s.
   */
  std::string
  remove_last_if_null(const std::string &s);


  /** \brief
   * Return the string "true" if b, else "false"
   */
  std::string
  to_string(bool b);

}  // namespace helper

#endif  // PROGS_SRC_COMMON_HELPER_HELPER_HPP_



/* ***********************
 * Macro always included *
 *************************/

/** \brief
 * Print to stderr all parameters separated by tabulation.
 *
 * If a NTSV macro is defined
 * then do nothing.
 *
 * (Useful during development phase.)
 */
#undef TSV

#ifdef NTSV
#define TSV(...)
#else
#define TSV(...) GET_OVERLOAD_(__VA_ARGS__,                             \
                               TSV10_, TSV9_, TSV8_, TSV7_, TSV6_, TSV5_, \
                               TSV4_, TSV3_, TSV2_, TSV1_)(__VA_ARGS__)
#endif



// To avoid cpplint warning
#ifndef PROGS_SRC_COMMON_HELPER_HELPER_HPP_
#endif  // PROGS_SRC_COMMON_HELPER_HELPER_HPP_
