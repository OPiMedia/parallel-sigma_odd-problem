/* -*- coding: latin-1 -*- */
/** \file common/helper/helper.cpp (January 8, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <fstream>
// \endcond

#include "helper.hpp"
#include "../sigmaodd/helper.hpp"


namespace helper {

  /* ******************
   * Private constant *
   ********************/
  const char sep_ = '/';



  /* ***********
   * Functions *
   *************/
  std::string
  concat_path(std::string path1, std::string path2) {
    if (path1.empty()) {
      return path2;
    }
    else if (path2.empty()) {
      return path1;
    }

    if ((path1.back() == sep_) && (path2[0] == sep_)) {
      path2 = path2.substr(1);
    }

    return ((path1.back() != sep_) && (path2[0] != sep_)
            ? path1 + sep_ + path2
            : path1 + path2);
  }


  std::string
  duration_ms_to_string(double duration_ms) {
    const double duration_second = duration_ms/1000;
    const double duration_minute = duration_ms/(1000*60);
    const double duration_hour = duration_ms/(1000*60*60);

    return std::to_string(duration_ms) + "ms"
      + "\t= " + std::to_string(duration_second) + 's'
      + "\t= " + std::to_string(duration_minute) + 'm'
      + "\t= " + std::to_string(duration_hour) + 'h';
  }


  std::string
  duration_to_string(std::chrono::duration<double> duration_second) {
    const double duration = duration_second.count();
    const double duration_millisecond = duration*1000;
    const double duration_minute = duration/60;
    const double duration_hour = duration/(60*60);

    return std::to_string(duration_millisecond) + "ms"
      + "\t= " + std::to_string(duration) + 's'
      + "\t= " + std::to_string(duration_minute) + 'm'
      + "\t= " + std::to_string(duration_hour) + 'h';
  }


  std::string
  file_to_string(std::string filename) {
    std::ifstream infile(filename, std::ios::in);
    const std::string s((std::istreambuf_iterator<char>(infile)),
                        std::istreambuf_iterator<char>());

    infile.close();
    if (infile.fail()) {
      std::cerr << "! Impossible to read file \"" << filename << '"' << std::endl;

      exit(EXIT_FAILURE);
    }

    return s;
  }


  std::string
  get_string(int argc, const char* const argv[], unsigned int i,
            void (*help_and_exit_function)()) {
    if (i < static_cast<unsigned int>(argc)) {
      return argv[i];
    }
    else {
      if (help_and_exit_function != nullptr) {
        (*help_and_exit_function)();
      }

      return "";  // to satisfy the compiler
    }
  }


  unsigned int
  get_uint(int argc, const char* const argv[], unsigned int i,
            void (*help_and_exit_function)()) {
    if (i < static_cast<unsigned int>(argc)) {
      return static_cast<unsigned int>(std::stoul(argv[i]));
    }
    else {
      if (help_and_exit_function != nullptr) {
        (*help_and_exit_function)();
      }

      return 0;  // to satisfy the compiler
    }
  }


  unsigned long
  get_ulong(int argc, const char* const argv[], unsigned int i,
            void (*help_and_exit_function)()) {
    if (i < static_cast<unsigned int>(argc)) {
      return std::stoul(argv[i]);
    }
    else {
      if (help_and_exit_function != nullptr) {
        (*help_and_exit_function)();
      }

      return 0;  // to satisfy the compiler
    }
  }


  bool
  is_file_exists(std::string filename) {
    std::ifstream file(filename);

    return static_cast<bool>(file);
  }


  void
  print_intern_config_compiler() {
    std::cout <<
#ifdef __clang__
      "clang " STR(__clang_major__) "." STR(__clang_minor__) "." STR(__clang_patchlevel__)
#else
#ifdef __GNUG__
      "GNU C++ " STR(__GNUG__) "." STR(__GNUC_MINOR__) "." STR(__GNUC_PATCHLEVEL__)
#else
      "unknown"
#endif
#endif
              << "\tsizeof(nat_type): " << sizeof(sigmaodd::nat_type)

#ifndef NDEBUG
              << "\tDEBUG mode!"
#endif
              << std::endl;
  }


  std::string
  remove_last_if_null(const std::string &s) {
    return (!s.empty() && (s[s.size() - 1] == 0)
            ? std::string(s.cbegin(), s.cend() - 1)
            : s);
  }


  std::string
  to_string(bool b) {
    return (b
            ? "true"
            : "false");
  }

}  // namespace helper
