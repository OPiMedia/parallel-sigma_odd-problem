/* -*- coding: latin-1 -*- */
/** \file common/sigmaodd/primes__inline.hpp (December 11, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_COMMON_SIGMAODD_PRIMES__INLINE_HPP_
#define PROGS_SRC_COMMON_SIGMAODD_PRIMES__INLINE_HPP_


namespace sigmaodd {

  /* *********************
   * constexpr functions *
   ***********************/

  constexpr
  unsigned int
  odd_primes_table_nb() {
    return array_odd_primes_nb_;
  }


#ifdef PRIME16
  constexpr
  nat_type
  potential_prime_offsets_table_modulo() {
    return array_potential_prime_offsets_modulo_;
  }
#endif


#ifdef PRIME16
  constexpr
  unsigned int
  potential_prime_offsets_table_nb() {
    return array_potential_prime_offsets_nb_;
  }
#endif



  /* ******************
   * inline functions *
   ********************/

  inline
  prime_type
  odd_primes_table_by_index(unsigned int i) {
    assert(i < odd_primes_table_nb());

    return array_odd_primes_[i];
  }


  inline
  prime_type
  odd_primes_table_last() {
    return array_odd_primes_[odd_primes_table_nb() - 1];
  }


  inline
  const prime_type*
  odd_primes_table_ptr() {
    return array_odd_primes_;
  }


#ifdef PRIME16
  inline
  nat_type
  potential_prime_offsets_table_by_index(unsigned int i) {
    assert(i < potential_prime_offsets_table_nb());

    return array_potential_prime_offsets_[i];
  }
#endif

}  // namespace sigmaodd

#endif  // PROGS_SRC_COMMON_SIGMAODD_PRIMES__INLINE_HPP_
