/* -*- coding: latin-1 -*- */
/** \file common/sigmaodd/harmonic__inline.hpp (December 20, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_COMMON_SIGMAODD_HARMONIC__INLINE_HPP_
#define PROGS_SRC_COMMON_SIGMAODD_HARMONIC__INLINE_HPP_


namespace sigmaodd {

  /* *********************
   * constexpr functions *
   ***********************/

  constexpr
  double
  harmonic(nat_type n) {
    double h = 0;

    for (nat_type i = 1; i <= n; ++i) {
      h += 1.0/static_cast<double>(i);
    }

    return h;
  }


  constexpr
  double
  harmonic_even(nat_type n) {
    return harmonic(n >> 1)/2;
  }


  constexpr
  double
  harmonic_odd(nat_type n) {
    double h = 0;

    for (nat_type i = 1; i <= n; i += 2) {
      h += 1.0/static_cast<double>(i);
    }

    return h;
  }

}  // namespace sigmaodd

#endif  // PROGS_SRC_COMMON_SIGMAODD_HARMONIC__INLINE_HPP_
