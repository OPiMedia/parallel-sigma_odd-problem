/* -*- coding: latin-1 -*- */
/** \file common/sigmaodd/pentagonal.hpp (December 9, 2017)
 * \brief
 * Functions to calculate pentagonal numbers
 * and one useless algorithm that used the Euler formula to calculate the sum of divisors.
 *
 * http://mathworld.wolfram.com/PentagonalNumber.html
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_COMMON_SIGMAODD_PENTAGONAL_HPP_
#define PROGS_SRC_COMMON_SIGMAODD_PENTAGONAL_HPP_

#include "helper.hpp"


namespace sigmaodd {

  /* ************
   * Prototypes *
   **************/

  /** \brief
   * Return the pentagonal number of n.
   *
   * https://oeis.org/A000326
   *
   * @return (3n - 1)n / 2
   */
  constexpr
  nat_type
  pentagonal(nat_type n);


  /** \brief
   * Return the generalized pentagonal number of -n.
   *
   * https://oeis.org/A005449
   *
   * @return (3n + 1)n / 2
   */
  constexpr
  nat_type
  pentagonal_neg(nat_type n);


  /** \brief
   * Calculates the sum of all divisors of n with the Euler formula about pentagonal numbers
   * and returns it.
   *
   * @param n != 0
   *
   * @return the sum of all divisors of n
   */
  nat_type
  sum_divisors__euler(nat_type n);


  /** \brief
   * Return true iff the cache used by sum_divisors__euler() is full.
   * The function can clean space, but it is less efficient.
   */
  bool
  sum_divisors__euler_cache_is_full();


  /** \brief
   * Return the current number of items in the cache used by sum_divisors__euler()
   */
  std::size_t
  sum_divisors__euler_cache_nb();


  /** \brief
   * Return the (constant) number of items in the table used by sum_divisors__euler()
   */
  std::size_t
  sum_divisors__euler_table_nb();

}  // namespace sigmaodd

#include "pentagonal__inline.hpp"

#endif  // PROGS_SRC_COMMON_SIGMAODD_PENTAGONAL_HPP_
