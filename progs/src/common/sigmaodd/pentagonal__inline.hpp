/* -*- coding: latin-1 -*- */
/** \file common/sigmaodd/pentagonal__inline.hpp (December 20, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_COMMON_SIGMAODD_PENTAGONAL__INLINE_HPP_
#define PROGS_SRC_COMMON_SIGMAODD_PENTAGONAL__INLINE_HPP_

#include "helper.hpp"


namespace sigmaodd {

  /* *********************
   * constexpr functions *
   ***********************/

  constexpr
  nat_type
  pentagonal(nat_type n) {
    return (is_even(n)
            ? (n*3 - 1)*(n >> 1)
            : ((n*3 - 1) >> 1)*n);
  }


  constexpr
  nat_type
  pentagonal_neg(nat_type n) {
    return (is_even(n)
            ? (n*3 + 1)*(n >> 1)
            : ((n*3 + 1) >> 1)*n);
  }

}  // namespace sigmaodd

#endif  // PROGS_SRC_COMMON_SIGMAODD_PENTAGONAL__INLINE_HPP_
