/* -*- coding: latin-1 -*- */
/** \file common/sigmaodd/helper__inline.hpp (December 20, 2017)
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_COMMON_SIGMAODD_HELPER__INLINE_HPP_
#define PROGS_SRC_COMMON_SIGMAODD_HELPER__INLINE_HPP_

// \cond
#include <cassert>
#include <cmath>
// \endcond


namespace sigmaodd {

  /* *********************
   * constexpr functions *
   ***********************/

  constexpr
  bool
  is_even(nat_type n) {
    return !is_odd(n);
  }


  constexpr
  bool
  is_odd(nat_type n) {
    return n & 1;
  }


  constexpr
  nat_type
  pow_nat(nat_type n, unsigned int k) {
    nat_type product = 1;

    while (k != 0) {
      if (is_even(k)) {
        k >>= 1;
        n *= n;
      }
      else {
        --k;
        product *= n;
      }
    }

    return product;
  }


  constexpr
  double
  square(double x) {
    return x*x;
  }


  constexpr
  nat_type
  square(nat_type n) {
    return n*n;
  }


  constexpr
  nat_type
  sum_even(nat_type n) {
    const nat_type k = n >> 1;

    // n(n + 2)/4       = k (k + 1)   if n even
    // (n - 1)(n + 1)/4 = k (k + 1)   if n odd
    return (k + 1)*k;
  }


  constexpr
  nat_type
  sum_geometric_progression(nat_type r, unsigned int k) {
    return (r > 1
            ? sum_geometric_progression_strict(r, k):
            (r == 0
             ? 1
             : r*(k + 1)));
  }


  constexpr
  nat_type
  sum_geometric_progression_strict(nat_type r, unsigned int k) {
    assert(r > 1);

    // 1 + r + r^2 + r^3 + ... + r^k = (r^(k + 1) - 1) / (r - 1)
    // = (r^k - 1) / (r - 1) + r^k  ! to avoid some possible overflows
    const nat_type rk = pow_nat(r, k);

    return (rk - 1)/(r - 1) + rk;
  }


  constexpr
  nat_type
  sum_natural(nat_type n) {
    const nat_type k = n;

    // n(n + 1)/2
    return (n % 2 == 0
            ? (k + 1)*(k >> 1)
            : ((k + 1) >> 1)*k);
  }


  constexpr
  nat_type
  sum_odd(nat_type n) {
    const nat_type k = (n + 1) >> 1;

    // n^2/4     = k^2   if n even
    // (n + 1)^4 = k^2   if n odd
    return square(k);
  }



  /* ******************
   * inline functions *
   ********************/

  inline
  nat_type
  ceil_eighth_root(nat_type n) {
    const nat_type root = floor_eighth_root(n);

    return (square(square(square(root))) == n
            ? root
            : root + 1);
  }


  inline
  nat_type
  ceil_fourth_root(nat_type n) {
    const nat_type root = floor_fourth_root(n);

    return (square(square(root)) == n
            ? root
            : root + 1);
  }


  inline
  nat_type
  ceil_square_root(nat_type n) {
    const nat_type root = floor_square_root(n);

    return (square(root) == n
            ? root
            : root + 1);
  }

}  // namespace sigmaodd

#endif  // PROGS_SRC_COMMON_SIGMAODD_HELPER__INLINE_HPP_
