/* -*- coding: latin-1 -*- */
/** \file common/sigmaodd/dot.hpp (January 2, 2018)
 * \brief
 * To produce graph DOT file.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

#ifndef PROGS_SRC_COMMON_SIGMAODD_DOT_HPP_
#define PROGS_SRC_COMMON_SIGMAODD_DOT_HPP_

#include "divisors.hpp"


namespace sigmaodd {

  /* ***********
   * Prototype *
   *************/

  /** \brief
   * Print to std::cout in a DOT format,
   * a graph of path of the sigma odd problem.
   *
   * If show_node
   * then plot nodes with number,
   * else plot only points.
   *
   * If shortcut
   * then do not print bigger step,
   * else print complete path until 1.
   *
   * If circular
   * then plot circular graph,
   * else plot tree graph.
   *
   * See https://en.wikipedia.org/wiki/DOT_(graph_description_language)
   *
   * @param first odd
   * @param last
   * @param show_node
   * @param shortcut
   * @param circular
   */
  void
  print_varsigmaodd_dot(nat_type first, nat_type last, bool show_node = true,
                        bool shortcut = false, bool circular = false);

}  // namespace sigmaodd

#endif  // PROGS_SRC_COMMON_SIGMAODD_DOT_HPP_
