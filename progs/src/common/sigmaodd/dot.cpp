/* -*- coding: latin-1 -*- */
/** \file common/sigmaodd/dot.cpp (January 2, 2018)
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <iostream>
#include <map>
#include <string>
#include <utility>
// \endcond

#include "sigmaodd.hpp"

#include "dot.hpp"


namespace sigmaodd {

  /* *******************
   * Private prototype *
   *********************/
  void
  print_arrow_(nat_type from, nat_type to, unsigned int length, bool circular);



  /* ******************
   * Private function *
   ********************/
  void
  print_arrow_(nat_type from, nat_type to, unsigned int length, bool circular) {
    const bool is_sqr = is_square(from);
    const std::string color = (is_sqr
                               ? "orange"
                               : "red");
    const unsigned int penwidth = (is_sqr
                                   ? 3
                                   : 5);

    if (from == to) {
      std::cout << "  " << from << " [style=filled"
                << (circular
                    ? ", height=3, width=3"
                    : "")
                << "];" << std::endl;
    }
    else if ((from < to) || (length > 1)) {
      std::cout << "  " << from << " [color=" << color << ", style=filled];" << std::endl;
    }

    std::cout << std::to_string(from)
              << " -> "
              << std::to_string(to);

    if ((from < to) || (length > 1)) {
      std::cout << " [color=" << color << ", penwidth=" << penwidth;
      if (length > 1) {
        std::cout << ", label=" << std::to_string(length);
      }
      std::cout << ']';
    }

    std::cout << ';' << std::endl;
  }



  /* **********
   * Function *
   ************/

  void
  print_varsigmaodd_dot(nat_type first, nat_type last, bool show_node,
                        bool shortcut, bool circular) {
    assert(is_odd(first));

    // Compute the graph
    std::map<nat_type, std::pair<nat_type, unsigned int>> map;

    map[1] = std::make_pair(1, 1);

    for (nat_type start_n = first; start_n <= last; start_n += 2) {
      if (shortcut) {
        map[start_n] = sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize(start_n);
      }
      else {
        nat_type n = start_n;

        do {
          const nat_type to = sum_odd_divisors_divided_until_odd__factorize(n);

          map[n] = std::make_pair(to, 1);
          n = to;
        } while (n != 1);
      }
    }


    // Print the graph
    std::cout << "digraph \"sigma odd\" {" << std::endl
              << (circular
                  ? "  layout=\"circo\";"
                  : "  rankdir=\"LR\";") << std::endl;
    if (!show_node) {
      std::cout << "  node[shape=point];" << std::endl;
    }
    std::cout << std::endl;

    for (std::pair<nat_type, std::pair<nat_type, unsigned int>> from_to_length : map) {
      print_arrow_(from_to_length.first, from_to_length.second.first,
                   from_to_length.second.second, circular);
    }

    std::cout << '}' << std::endl;
  }

}  // namespace sigmaodd
