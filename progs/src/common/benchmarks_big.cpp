/* -*- coding: latin-1 -*- */
/** \file common/benchmarks_big.cpp (January 5 , 2018)
 * \brief
 * Some benchmarks for the sigma_odd problem,
 * only on some "big" numbers.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <algorithm>
#include <chrono>
#include <iostream>
// \endcond

#include "helper/helper.hpp"
#include "sigmaodd/pentagonal.hpp"
#include "sigmaodd/primes.hpp"
#include "sigmaodd/sigmaodd.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: benchmarks [option]" << std::endl
            << std::endl
            << "Option:" << std::endl
            << "  --nb n   number of odd numbers to check by range (100 by default)" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  std::srand(666);  // to have deterministic "random" numbers

  const sigmaodd::nat_type first = 2305843009213693945u;
  //                               2305843009213693951 is prime, https://oeis.org/A000668
  sigmaodd::nat_type nb = 100;

  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--nb") {
      nb = helper::get_ulong(argc, argv, ++i, &help_and_exit);;
    }
    else {
      help_and_exit();
    }
  }

  const sigmaodd::nat_type last = first + nb*2;

  assert(sigmaodd::is_odd(first));
  assert(sigmaodd::is_odd(last));


  // Print intern configuration
  helper::print_intern_config_compiler();


  // Print parameter
  std::cout << "First: " << first
            << "\tLast: " << last
            << "\tNb: " << nb << std::endl;


  // Print table legend
  std::cout << std::endl
            << "         \t         \t/# nb tested\t/# nb naturals\t% tested" << std::endl;
  std::cout.flush();


  // Main calculation
  std::chrono::duration<double> total_duration = std::chrono::duration<double>(0);


  // Factorize
  {
    sigmaodd::nat_type real_nb = 0;
    const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

    for (sigmaodd::nat_type n = first; n <= last; n += 2) {
      if (sigmaodd::is_little_mersenne_prime_unitary_divide(n)) {
        continue;
      }

      ++real_nb;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
      volatile const std::pair<sigmaodd::nat_type, unsigned int> result_length
        = sigmaodd::sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize(n);
#pragma GCC diagnostic pop
    }

    const std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

    std::cout << "Factorize\taverage:\t"
              << static_cast<double>(duration.count())/static_cast<double>(real_nb) << " s\t"
              << static_cast<double>(duration.count())/static_cast<double>(nb*2) << " s\t"
              << static_cast<double>(real_nb*100)/static_cast<double>(nb*2) << "%"
              << std::endl;
    total_duration += duration;
  }
  std::cout.flush();

  // Factorize bound
  {
    sigmaodd::nat_type real_nb = 0;
    const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

    for (sigmaodd::nat_type n = first; n <= last; n += 2) {
      if (sigmaodd::is_little_mersenne_prime_unitary_divide(n)) {
        continue;
      }

      ++real_nb;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
      volatile const std::pair<sigmaodd::nat_type, unsigned int> result_length
        = sigmaodd::sum_odd_divisors_divided_until_odd_iterate_until_lower__factorize_bound(n);
#pragma GCC diagnostic pop
    }

    const std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

    std::cout << "Factorize bound\taverage:\t"
              << static_cast<double>(duration.count())/static_cast<double>(real_nb) << " s\t"
              << static_cast<double>(duration.count())/static_cast<double>(nb*2) << " s\t"
              << static_cast<double>(real_nb*100)/static_cast<double>(nb*2) << "%"
              << std::endl;
    total_duration += duration;
  }

  std::cerr << "Total duration (for all calculation): "
            << helper::duration_to_string(total_duration)
            << std::endl;

  return EXIT_SUCCESS;
}
