/* -*- coding: latin-1 -*- */
/** \file common/table.cpp (January 5, 2018)
 * \brief
 * Print tables of sigma_odd results by several algorithms.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cassert>
#include <cmath>
#include <cstdlib>

#include <iostream>
// \endcond

#include "helper/helper.hpp"
#include "sigmaodd/helper.hpp"
#include "sigmaodd/primes.hpp"
#include "sigmaodd/sigmaodd.hpp"


/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: table [options]" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --first n   first number to check (1 by default)" << std::endl
            << "  --last  n   last number to check (101 by default)" << std::endl
            << "  --nb    n   number of odd numbers to check" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  sigmaodd::nat_type first = 1;
  sigmaodd::nat_type last = 1000;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--first") {
      first = std::max(1ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
    }
    else if (param == "--nb") {
      last = first + helper::get_ulong(argc, argv, ++i, &help_and_exit)*2 - 1;
    }
    else {
      help_and_exit();
    }
  }


  // Print intern configuration
  helper::print_intern_config_compiler();


  // Print parameters
  std::cout << "First: " << first
            << "\tLast: " << last
            << "\tNb: " << (first <= last
                            ? last + 1 - first
                            : 0)
            << std::endl;


  // Print table legend
  std::cout << std::endl
            << "n\tsigma\tsigma_odd\tvarsigma_odd"
            << std::endl;


  // Print data
  for (sigmaodd::nat_type n = first; n <= last; ++n) {
    const sigmaodd::nat_type sum = sigmaodd::sum_divisors__factorize(n);
    const sigmaodd::nat_type sum_odd = sigmaodd::sum_odd_divisors__factorize(n);
    const sigmaodd::nat_type varsum_odd = sigmaodd::sum_odd_divisors_divided_until_odd__factorize(n);

    std::cout << n
              << '\t' << sum
              << '\t' << sum_odd
              << '\t' << varsum_odd
              << std::endl;
  }

  return EXIT_SUCCESS;
}
