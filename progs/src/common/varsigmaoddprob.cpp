/* -*- coding: latin-1 -*- */
/** \file common/varsigmaoddprob.cpp (January 5, 2018)
 * \brief
 * The varsigma_odd problem.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cstdlib>

#include <chrono>
#include <iostream>
// \endcond

#include "helper/helper.hpp"
#include "sigmaodd/dot.hpp"
#include "sigmaodd/primes.hpp"
#include "sigmaodd/sigmaodd.hpp"



/* ***********
 * Prototype *
 *************/

void
help_and_exit();



/* **********
 * Function *
 ************/

void
help_and_exit() {
  std::cerr << "Usage: varsigmaoddprob [options]" << std::endl
            << std::endl
            << "Method options to calculate sum of divisors:" << std::endl
            << "  --bound       factorize method with bound shortcuts (by default)" << std::endl
            << "  --factorize   factorize number with partial prime numbers table" << std::endl
            << "  --naive       try each odd possible divisor)" << std::endl
            << std::endl
            << "Options:" << std::endl
            << "  --dot              print DOT graph instead normal data" << std::endl
            << "  --dot-circular     print DOT graph instead normal data" << std::endl
            << "  --first n          first odd number to check (3 by default)" << std::endl
            << "  --last  n          last odd number to check (101 by default)" << std::endl
            << "  --nb    n          number of odd numbers to check" << std::endl
            << "  --only-longer      print number with length greater than 1 (not for all options)" << std::endl
            << "  --path             print path for each number checked" << std::endl
            << "  --print-long       print all number that require more than 1 iteration" << std::endl
            << "  --print-sqr        print all square numbers" << std::endl
            << "  --graph-point      print points instead nodes (only for graph)" << std::endl
            << "  --graph-shortcut   print shortcut graph (only for graph)" << std::endl;

  exit(EXIT_FAILURE);
}



/* ******
 * Main *
 ********/

int
main(int argc, const char* const argv[]) {
  // Load primes table
  if (!sigmaodd::read_primes_table()) {
    std::cerr << "! Impossible to load \"" << sigmaodd::prime_filename << '"' << std::endl
              << std::endl;
    help_and_exit();
  }


  enum Methods: unsigned int {factorize, factorize_bound, naive, print_long, print_sqr};

  const std::string method_strings[5] =  {"factorize",
                                          "factorize bound",
                                          "naive",
                                          "print-long",
                                          "print-sqr"};

  sigmaodd::nat_type first = 3;
  sigmaodd::nat_type last = 101;

  bool dot_circular = false;
  bool graph_point = false;
  bool graph_shortcut = false;
  unsigned int method = factorize;
  bool print_dot = false;
  bool print_only_longer = false;
  bool print_path = false;


  // Read command line parameters
  for (unsigned int i = 1; i < static_cast<unsigned int>(argc); ++i) {
    const std::string param(argv[i]);

    if (param == "--dot") {
      print_dot = true;
      dot_circular = false;
    }
    else if (param == "--dot-circular") {
      print_dot = true;
      dot_circular = true;
    }
    else if (param == "--bound") {
      method = factorize_bound;
    }
    else if (param == "--factorize") {
      method = factorize;
    }
    else if (param == "--first") {
      first = std::max(3ul, helper::get_ulong(argc, argv, ++i, &help_and_exit));
      if (sigmaodd::is_even(first)) {
        ++first;
      }
    }
    else if (param == "--graph-point") {
      graph_point = true;
    }
    else if (param == "--graph-shortcut") {
      graph_shortcut = true;
    }
    else if (param == "--last") {
      last = helper::get_ulong(argc, argv, ++i, &help_and_exit);
      if (sigmaodd::is_even(last)) {
        --last;
      }
    }
    else if (param == "--naive") {
      method = naive;
    }
    else if (param == "--nb") {
      last = first + helper::get_ulong(argc, argv, ++i, &help_and_exit)*2 - 1;
    }
    else if (param == "--only-longer") {
      print_only_longer = true;
    }
    else if (param == "--path") {
      print_path = true;
    }
    else if (param == "--print-long") {
      method = print_long;
    }
    else if (param == "--print-sqr") {
      method = print_sqr;
    }
    else {
      help_and_exit();
    }
  }

  const sigmaodd::nat_type nb = (first <= last
                                 ? (last - first)/2 + 1
                                 : 0);

  if (!print_dot) {
    // Print intern configuration
    helper::print_intern_config_compiler();

    // Print parameters
    std::cout << "First: " << first
              << "\tLast: " << last
              << "\tNb: " << nb;
    if ((method != print_long) && (method != print_sqr)) {
      std::cout << "\tPrint only longer?: " << helper::to_string(print_only_longer);
    }
    std::cout << "\tPrint path?: " << helper::to_string(print_path)
              << "\tMethod: " << method_strings[method]
              << std::endl;

    // Print table legend
    std::cout << std::endl
              << "n\tLower";
    if ((method != print_long) && (method != print_sqr)) {
      std::cout << "\tLength";
    }
    if (print_path) {
      std::cout << "\tPath";
    }
    std::cout << std::endl;
    std::cout.flush();
  }


  // Main calculation
  const std::chrono::steady_clock::time_point clock_start = std::chrono::steady_clock::now();

  if (print_dot) {
    sigmaodd::print_varsigmaodd_dot(first, last, !graph_point, graph_shortcut, dot_circular);
  }
  else if (method == factorize) {
    sigmaodd::print_sigmaodd__factorize(first, last, print_only_longer, print_path);
  }
  else if (method == factorize_bound) {
    sigmaodd::print_sigmaodd__factorize_bound(first, last, print_only_longer, print_path);
  }
  else if (method == naive) {
    sigmaodd::print_sigmaodd__naive(first, last, print_only_longer, print_path);
  }
  else if (method == print_long) {
    sigmaodd::print_long(first, last, print_path);
  }
  else if (method == print_sqr) {
    sigmaodd::print_square(first, last, print_path);
  }
  else {
    exit(EXIT_FAILURE);
  }
  std::cout.flush();


  // End
  if (!print_dot) {
    std::chrono::duration<double> duration = std::chrono::steady_clock::now() - clock_start;

    std::cout << "Total duration: " << helper::duration_to_string(duration)
              << std::endl;
  }

  return EXIT_SUCCESS;
}
