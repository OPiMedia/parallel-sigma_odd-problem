#!/bin/sh

echo "Set configuration system to boot only in console mode."
echo "Require systemd and admin authentication."
echo "Only tested on Debian GNU/Linux 9 (Stretch)"

systemctl set-default multi-user.target
