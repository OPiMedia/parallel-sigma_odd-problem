/* -*- coding: latin-1 -*- */
/** \file doxymain.hpp (September 20, 2018)
 * \brief
 * Empty header file to define main page of Doxygen documentation.
 *
 * GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
 * http://www.opimedia.be/
 */

/** \mainpage
 * &sigma;<sub>odd</sub> problem
 *
 * C++ (and OpenCL) implementations of sequential and parallel algorithms
 * to check the &sigma;<sub>odd</sub> (sigma_odd) problem by the equivalent &sigmaf;<sub>odd</sub> (varsigma_odd) problem.
 * Those are simple arithmetic conjectures
 * like the iterative 3n+1 problem (Collatz conjecture),
 * but they iterate a variant of the divisor function.
 *
 * All results, documents, C++/OpenCL and LaTeX sources are available on Bitbucket:
 * https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem
 *
 *
 * <div>
 * GPLv3
 * ------
 * Copyright (C) 2017, 2018 Olivier Pirson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 * </div>
 * <hr>
 *
 * <div align="right">
 *   &copy; Olivier <span style="font-variant:small-caps">Pirson</span>\n
 *   <a href="http://www.opimedia.be/"><tt>www.opimedia.be</tt></a>\n
 *   <a rel="nofollow" href="mailto:olivier.pirson.opi@gmail.com?subject=[sigma_odd]"><tt>olivier.pirson.opi@gmail.com</tt></a>
 * </div>
 */
