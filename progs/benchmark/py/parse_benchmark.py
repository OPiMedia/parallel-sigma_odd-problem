#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
parse_benchmark.py  filename [--speedup]

January 18, 2018

GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import re
import sys


#
# Functions
###########
def key_sort(k):
    """
    To sort all lines.
    """
    d0 = {'sequential': 0,
          'threads': 1,
          'MPI': 2,
          'OpenCL': 3}

    d1 = {'one by one': 0,
          'by range': 1,
          'dynamic': 2}

    return (d0[k[0]], d1.get(k[1], k[1]), k[2])


def print_benchmark(d):
    """
    Print a table of time, speedup, efficiency and overhead
    with a result by line.
    """
    print('Techno\tAlgo\tParam\tms\ts\tSpeedup\tEfficiency\tOverhead')
    for algo in sorted(d, key=key_sort):
        print_data(algo, d)
        print()


def print_data(algo, d):
    """
    Print Techno, algo, param, ms, ts, speedup, efficiency and overhead
    for one data.
    """
    data = d[algo]

    p = algo[2]

    sequential_ms = d[('sequential', None, 1)][0]
    par = data[0]

    speedup = sequential_ms/par
    efficiency = speedup/p
    overhead = p*par - sequential_ms

    print('{}\t{}\t{}\t{:.3f}\t{:.3f}\t{:.3f}\t{:.5f}\t{:.3f}'
          .format(algo[0], ('' if algo[1] is None
                            else algo[1]),
                  algo[2],
                  data[0], data[1],
                  speedup, efficiency, overhead),
          end='')


def print_transpose(d):
    """
    Print a table of time, speedup and efficiency
    grouped by techno/algo.
    """
    sequential_ms = d[('sequential', None, 1)][0]

    for _ in range(6):
        print('Techno\tAlgo\tParam\tms\ts\tSpeedup\tEfficiency\tOverhead', end='\t')
    print()

    for i in range(1, 8 + 1):
        for algo in sorted(d, key=key_sort):
            def print_column(t, a):
                if algo == (t, a, i):
                    if algo[0] != 'sequential':
                        print(end='\t')
                    print_data(algo, d)

            if (algo[0] == 'sequential') and (i > 1):
                print(end='\t'*7)
            else:
                print_column('sequential', None)

            print_column('threads', 'one by one')
            print_column('threads', 'by range')
            print_column('threads', 'dynamic')

            print_column('MPI', 'one by one')
            print_column('MPI', 'dynamic')
        print()


def fstr(x, decimals=3):
    """
    Return a string representation of the float x.
    """
    return '{{:{}}}'.format(decimals).format(x)


#
# Main
######
def main():
    """
    Main
    """
    if len(sys.argv[1]) <= 1:
        exit(1)

    s = None
    nb = 1

    d = dict()

    # Parse data
    with open(sys.argv[1]) as fin:
        for line in fin:
            line = line.strip()

            match = re.match('=== (.+) ===', line)
            if match:
                submatch = re.match('^(.+?)(/(.+))?$', match.group(1))
                algo = (submatch.group(1), submatch.group(3))

            match = re.match(r'Total duration:\s*(.+?)ms\s*=\s*(.+?)s', line)
            if match:
                ms = float(match.group(1))
                s = float(match.group(2))
                if algo == 'OpenCL':
                    ms *= 10
                    s *= 10

                d[algo + (nb, )] = (ms, s)

            match = re.search(r'(# threads|# process|Nb OpenCL):\s+(\S+)',
                              line)
            if match:
                nb = int(match.group(2))

    # Print table
    if (len(sys.argv) > 2) and (sys.argv[2] == '--transpose'):
        print_transpose(d)
    else:
        print_benchmark(d)

if __name__ == '__main__':
    main()
