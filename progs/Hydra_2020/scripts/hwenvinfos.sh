#!/bin/bash

# Print some information about hardware and environment
echo 'hwenvinfos.sh v.01.00.01'


CMDS=('date --utc; date'
      'hostname; hostname --domain'
      'uname --all'
      'cat /etc/os-release'
      'nproc; nproc --all'
      'cat /proc/sys/kernel/threads-max'
      'free --human --total --wide'
      'cat /proc/meminfo'
      'ulimit -a'
      'df --all --human-readable --print-type'
      'lscpu'
      'cat /proc/cpuinfo'
      'lspci -v')

for CMD in "${CMDS[@]}"
do
    echo -e '\n\n'
    echo "==================== $CMD ===================="
    eval "$CMD" 2>&1
done
