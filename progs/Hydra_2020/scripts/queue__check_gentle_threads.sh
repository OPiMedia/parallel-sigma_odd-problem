#!/bin/bash -l

#PBS -l nodes=1:ppn=20
#PBS -l walltime=120:00:00
#PBS -l mem=20gb
#PBS -l file=1gb
#PBS -o log/check_gentle_threads.log
#PBS -e log/check_gentle_threads.err
#PBS -N check_gentle_threads

cd "$HOME/sigmaodd/progs/src/threads/" || exit 1
PROGNAME='check_gentle_threads'
NAME="${PROGNAME}_further"


echo "=== Config module ==="
module purge
module load GCC


echo "=== Compile ==="
make distclean
make ndebug all
make clean


echo "=== Run job on $HOSTNAME - $(date) ==="
./hwenvinfos.sh > "log/${NAME}__hwenvinfos.log" 2>&1

"./$PROGNAME" --last 1125899906842624 > "log/${NAME}.tsv" 2>&1
# ! missing: --nb-thread 20


echo "=== Done $(date) ==="
