#!/bin/bash -l

#PBS -l nodes=4:ppn=16
#PBS -l walltime=120:00:00
#PBS -l mem=64gb
#PBS -l file=1gb
#PBS -o log/check_gentle_mpi.log
#PBS -e log/check_gentle_mpi.err
#PBS -N check_gentle_mpi

cd "$HOME/sigmaodd/progs/src/mpi/" || exit 1
PROGNAME='check_gentle_mpi'
NAME="${PROGNAME}_further"


echo "=== Config module ==="
module purge
module load OpenMPI


echo "=== Compile ==="
make distclean
make ndebug all
make clean


echo "=== Run job on $HOSTNAME - $(date) ==="
./hwenvinfos.sh > "log/${NAME}__hwenvinfos.log" 2>&1

mpirun "./$PROGNAME" --dynamic --last 1125899906842624 > "log/${NAME}.tsv" 2>&1


echo "=== Done $(date) ==="
