# sigma_odd / tables --- February 16, 2019

HTML_DESTS =  html/bads_known_infos.html


.SUFFIXES:

###########
# Options #
###########
PYTHON3      = python3
PYTHON3FLAGS =

SED   = sed -E
SHELL = sh


BADCONCAT = $(PYTHON3) $(PYTHON3FLAGS) ./py/concat_bad_tables.py

BADINFOS = $(PYTHON3) $(PYTHON3FLAGS) ./py/infos_bad_table.py

CMPSET = $(PYTHON3) $(PYTHON3FLAGS) ./py/cmp_set.py

MAXS = $(PYTHON3) $(PYTHON3FLAGS) ./py/max_in_column.py

TSV2HTMLTABLE      = tsv2htmltable  # https://bitbucket.org/OPiMedia/tsv2htmltable
TSV2HTMLTABLEFLAGS = --dest-add-column-indices --dest-html-css 'tsv2htmltable/tsv2htmltable.min.css' --dest-html-js 'tsv2htmltable/tsv2htmltable.min.js'



###
# #
###
.PHONY:	all bad bads_know build cmp concatenate gentles html maxs search

all:	build cmp

bad:
	# Compare same results computed in several manners
	$(CMPSET) tsv/sequential_check_bad_3_1000000001.tsv tsv/sequential_check_bad_3_5000000001.tsv 0 0 1000000001

	$(CMPSET) tsv/sequential_check_bad_3_1000000001.tsv tsv/sequential_check_gentle_3_1000000001.tsv
	$(CMPSET) tsv/sequential_check_bad_3_5000000001.tsv tsv/sequential_check_gentle_3_5000000001.tsv
	$(CMPSET) tsv/sequential_check_bad_5000000001_10000000001.tsv tsv/sequential_check_gentle_5000000001_10000000001.tsv

bads_know:
	# Compare concatenated files
	$(CMPSET) bads_known.txt tsv/sequential_check_gentle_3_5000000001.tsv 0 0 5000000001
	$(CMPSET) bads_known.txt tsv/sequential_check_bad_5000000001_10000000001.tsv 0 5000000001 10000000001
	$(CMPSET) bads_known.txt tsv/threads_check_gentle_10000000001_20000000001.tsv 0 10000000001 20000000001
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_20000000001_44543568825.tsv 0 20000000001 44543568825
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_44543568825_50000000001.tsv 0 44543568825 50000000001
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_50000000001_67211586225.tsv 0 50000000001 67211586225
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_67000000001_68000000001.tsv 0 67000000001 68000000001
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_68000000001_69000000001.tsv 0 68000000001 69000000001
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_69000000001_70000000001.tsv 0 69000000001 70000000001
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_70000000001_90000000001.tsv 0 70000000001 90000000001
	$(CMPSET) bads_known.txt ../Hydra/results_further/check_further_90000000001_110000000001.tsv 0 90000000001 110000000001

build:	concatenate tsv/bads_known_infos.tsv html

cmp:	bad bads_know gentles maxs search

concatenate:
	$(BADCONCAT) tsv/*_check_gentle_*.tsv ../Hydra/results_further/check_further_*.tsv > bads_known.txt

gentles:
	# Compare same results (list of bad numbers) computed in several manners
	$(CMPSET) bads_known.txt tsv/sequential_check_gentle_3_1000000001.tsv 0 0 1000000001
	$(CMPSET) bads_known.txt tsv/sequential_check_gentle_3_5000000001.tsv 0 0 5000000001
	$(CMPSET) bads_known.txt tsv/threads_check_gentle_5000000001_10000000001.tsv 0 5000000001 10000000001
	$(CMPSET) bads_known.txt tsv/threads_check_gentle_10000000001_20000000001.tsv 0 10000000001 20000000001

	$(CMPSET) tsv/sequential_check_gentle_3_1000000001.tsv tsv/sequential_check_gentle_3_5000000001.tsv 0 0 1000000001

	$(CMPSET) tsv/sequential_check_gentle_3_1000000001.tsv tsv/threads_check_gentle_3_1000000001.tsv
	$(CMPSET) tsv/sequential_check_gentle_3_5000000001.tsv tsv/threads_check_gentle_3_5000000001.tsv

	$(CMPSET) tsv/sequential_check_gentle_3_1000000001.tsv tsv/mpi_check_gentle_3_1000000001.tsv
	$(CMPSET) tsv/sequential_check_gentle_3_5000000001.tsv tsv/mpi_check_gentle_3_5000000001.tsv

	$(CMPSET) tsv/sequential_check_gentle_3_1000000001.tsv tsv/opencl_check_gentle_3_10000001.tsv 0 0 10000001
	$(CMPSET) tsv/sequential_check_gentle_3_1000000001.tsv tsv/opencl_check_gentle_3_1000000001.tsv
	$(CMPSET) tsv/sequential_check_gentle_3_5000000001.tsv tsv/opencl_check_gentle_3_5000000001.tsv

html:	$(HTML_DESTS)

html/bads_known_infos.html:	TSV2HTMLTABLEFLAGS+=--src-skip-nb-line 2 \
	--src-sep , --dest-table-header 'Divisor,Number,Percentage,Divisor*percentage' \
	--src-line-replace-regex '[^0-9.]+' ' ' --src-line-replace-regex '^ +' '' \
	--src-sep-regex '\s+' \
	--dest-html-source-data-url '../tsv/bads_known_infos.tsv'

maxs:
	# Print maximum of partial lengths and lengths
	$(MAXS) 2 4 0 < tsv/sequential_path_3_100000.tsv
	$(MAXS) 2 4 0 < tsv/sequential_path_3_100000_useless.tsv

	$(MAXS) 3 5 0 < tsv/sequential_check_bad_5000000001_10000000001.tsv

	$(MAXS) 3 5 1 < tsv/sequential_check_square_3_10000000000.tsv
	$(MAXS) 3 5 1 < tsv/sequential_check_square_3_100000000000.tsv
	$(MAXS) 3   1 < tsv/sequential_check_square_100000000001_10000000000000.tsv

	$(MAXS) 0 < bads_known.txt

search:
	# Search exception (marked by *, # or !) in these results (normally there is NO such exceptions)
	$(SED) -n '/[*#!]/p' tsv/sequential_check_bad_3_1000000001.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_check_bad_3_5000000001.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_check_bad_5000000001_10000000001.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_check_bad_3_100000000001.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_check_square_100000000001_10000000000000.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_check_square_3_100000000000.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_check_square_3_10000000000.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_path_3_100000.tsv
	$(SED) -n '/[*#!]/p' tsv/sequential_path_3_100000_useless.tsv



#########
# Rules #
#########
.PRECIOUS:

html/%.html:	tsv/%.tsv
	$(TSV2HTMLTABLE) $(TSV2HTMLTABLEFLAGS) $< -o $@



#########
# Clean #
#########
.PHONY:	clean distclean overclean

clean:

distclean:	clean

overclean:	distclean
