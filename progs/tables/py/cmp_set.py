#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
cmp_set.py

Usage: cmp_set.py filename1 filename2 [skip [first_n [last_n]]]

  filename1   first file
  filename1   second file
  skip        number of the first lines to be skipped

Read two files of table values
and compare set of these values
(i.e. check if two files contain same value regardless order).

January 7, 2018

GPLv3 --- Copyright (C) 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import sys


#
# Function
##########
def file_to_set(filename, skip, first_n, last_n):
    """
    Read the file and return a set of values between first_n and last_n.

    Skip the skip first lines.

    If last_n is None, then there is no upper bound.
    """
    assert isinstance(filename, str), type(filename)

    assert isinstance(skip, int), type(skip)
    assert skip >= 0, skip

    assert isinstance(first_n, int), type(first_n)
    assert first_n >= 0, first_n

    assert (last_n is None) or isinstance(last_n, int), type(last_n)
    assert (last_n is None) or last_n >= 0, last_n

    values = set()

    with open(filename) as fin:
        for _ in range(skip):
            fin.readline()

        for line in fin:
            line = line.strip()
            pieces = line.split()

            try:
                n = int(pieces[0])
            except:
                continue

            assert n > 0, n

            if (first_n <= n) and ((last_n is None) or (n <= last_n)):
                values.add(n)

    return values


#
# Main
######
def main():
    """
    Main
    """
    if len(sys.argv) < 3:
        exit(1)

    filename1 = sys.argv[1]
    filename2 = sys.argv[2]
    skip = 0
    first_n = 0
    last_n = None

    try:
        if len(sys.argv) > 3:
            skip = int(sys.argv[3])
            if len(sys.argv) > 4:
                first_n = int(sys.argv[4])
                if len(sys.argv) > 5:
                    last_n = int(sys.argv[5])
    except:
        exit(1)

    print(filename1, filename2, skip, first_n, last_n, sep='\t')

    set1 = file_to_set(filename1, skip, first_n, last_n)
    set2 = file_to_set(filename2, skip, first_n, last_n)

    is_same = True

    diff = set1 - set2
    if diff:
        is_same = False
        print("""file1 - file2
=============""")
        for n in sorted(diff):
            print(n)
        print('=============')

    diff = set2 - set1
    if diff:
        is_same = False
        print("""file2 - file1
=============""")
        for n in sorted(diff):
            print(n)
        print('=============')

    print('Sizes:', len(set1), len(set2), sep='\t')
    print('=============')

    if not is_same:
        exit(1)

if __name__ == '__main__':
    main()
