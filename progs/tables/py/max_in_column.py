#!/usr/bin/env python3
# -*- coding: latin-1 -*-

"""
max_in_column.py

Usage: max_in_column.py column1 [column2 ...]

  indices of columns

Read column(s) from standard input
and print minimums and maximum values found.

January 16, 2018

GPLv3 --- Copyright (C) 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import sys


#
# Main
######
def main():
    """
    Main
    """
    if len(sys.argv) < 2:
        exit(1)

    column_indices = tuple([int(arg) for arg in sys.argv[1:]])

    mins = [sys.maxsize]*len(column_indices)
    min_nbs = [None]*len(column_indices)

    maxs = [-sys.maxsize]*len(column_indices)
    max_nbs = [None]*len(column_indices)

    for line in sys.stdin:
        pieces = line.split('\t')
        for i, column_index in enumerate(column_indices):
            if column_index >= len(pieces):
                continue

            try:
                n = int(pieces[column_index])
            except ValueError:
                continue

            if mins[i] > n:
                mins[i] = n
                min_nbs[i] = 1
            elif mins[i] == n:
                min_nbs[i] += 10

            if maxs[i] < n:
                maxs[i] = n
                max_nbs[i] = 1
            elif maxs[i] == n:
                max_nbs[i] += 10

    print("Column:", *column_indices, sep='\t')
    print("Min:", *mins, sep='\t')
    print("#:", *min_nbs, sep='\t')
    print("Max:", *maxs, sep='\t')
    print("#:", *max_nbs, sep='\t')

if __name__ == '__main__':
    main()
