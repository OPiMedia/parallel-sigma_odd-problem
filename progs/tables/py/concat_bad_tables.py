#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
concat_bad_table.py filename1 [filename2...]

Usage: concat_bad_table.py filename1 [filename2...]

Read all files that they must be contains list of bad numbers
and print an ordered concatenation.

Check also some properties by assertions.

February 21, 2018

GPLv3 --- Copyright (C) 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math
import sys


#
# Function
##########
def file_to_set(filename):
    """
    Read the file and return a set of all integer values
    found in beginning of lines.
    """
    assert isinstance(filename, str), type(filename)

    values = set()

    with open(filename) as fin:
        for line in fin:
            line = line.strip()
            pieces = line.split()

            try:
                n = int(pieces[0])
            except:
                continue

            assert n >= 2205, n
            assert n & 1 == 1, n
            assert math.ceil(math.sqrt(n))**2 > n, n
            assert math.floor(math.sqrt(n))**2 < n, n
            assert n % 9 == 0, n

            values.add(n)

    return values


#
# Main
######
def main():
    """
    Main
    """
    bads = set()

    for filename in sys.argv[1:]:
        print('=== {} ==='.format(filename), file=sys.stderr)
        bads |= file_to_set(filename)

    for bad in sorted(bads):
        print(bad)

if __name__ == '__main__':
    main()
