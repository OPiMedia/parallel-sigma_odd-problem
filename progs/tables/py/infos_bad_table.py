#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
infos_bad_table.py filename

Usage: infos_bad_table.py filename

Read a file of bad numbers,
check some properties
and print some information.

February 21, 2018

GPLv3 --- Copyright (C) 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math
import sys


#
# Functions
###########
def bad_file(filename):
    """
    Read the file,
    check some properties
    and print some information.
    """
    assert isinstance(filename, str), type(filename)

    divisibles = {d: 0 for d in (5, 7, 11, 13, 17, 19, 23, 29,
                                 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
                                 73, 79, 83, 89, 97, 101)}
    nb_ignored = 0
    values = set()

    with open(filename) as fin:
        for line in fin:
            line = line.strip()
            pieces = line.split()

            try:
                n = int(pieces[0])
            except:
                nb_ignored += 1

                continue

            assert n >= 2205, n  # 2205 is the first bad number
            assert n & 1 == 1, n  # is odd
            assert math.ceil(math.sqrt(n))**2 > n, n  # is not perfect square
            assert math.floor(math.sqrt(n))**2 < n, n
            assert n % 9 == 0, n  # divisible by 9

            for k in (2, 3, 5, 7, 13, 17, 19, 31, 61):
                mersenne = 2**k - 1
                if mersenne > n:
                    break

                # Divisible by odd power of Mersenne prime
                assert p_adic(n, mersenne) % 2 == 0, (n, k, mersenne)

            assert n not in values  # no duplicate

            for d in divisibles:
                if n % d == 0:
                    divisibles[d] += 1

            values.add(n)

    print('# ignored line(s):', nb_ignored)
    print('# bad numbers:', len(values))
    for d in sorted(divisibles):
        nb = divisibles[d]
        percent = float(nb*100)/len(values)
        print('# divisible by {}:\t{}\t~= {:.3f}%\t% * divisor: {:.3f}'
              .format(d, nb, percent, percent*d))


def p_adic(n, p):
    """
    Return the maximum alpha such that p^alpha divides n.
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    assert isinstance(p, int), type(p)
    assert p > 0, p

    nb = 0
    while n % p == 0:
        n //= p
        nb += 1

    return nb


#
# Main
######
def main():
    """
    Main
    """
    if len(sys.argv) != 2:
        exit(1)

    bad_file(sys.argv[1])

if __name__ == '__main__':
    main()
