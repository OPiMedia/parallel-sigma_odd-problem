# Sequential and Parallel Numerical Verification of the σ\_odd problem (with theoretical results)

C++ (and OpenCL) implementations of sequential and parallel algorithms
to check the σ\_odd (sigma\_odd) problem by the equivalent ς\_odd (varsigma\_odd) problem.
Those are simple arithmetic conjectures
like the iterative 3n+1 problem (Collatz conjecture),
but they iterate a variant of the divisor function.
See examples below.

  - `sigma_odd-and-varsigma_odd-problems--Olivier-Pirson-2019.pdf`: (*coming soon*) main document, with statement of the problem, theoretical results and numerical verifications
  - [`parallel-sigma_odd-problem--Olivier-Pirson-2018--slides.pdf`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/parallel-sigma_odd-problem--Olivier-Pirson-2018--slides.pdf): **presentation slides**. Summary of important points of the problem and presentation of parallelization results
    (on Speaker Deck: <https://speakerdeck.com/opimedia/parallel-numerical-verification-of-the-s-odd-problem>)
  - [`big_data/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/big_data/): must be contains the binary file `prime28.bin` (available in a compressed file)
  - [`progs/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/)
      - [`src/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/): **main C++ and OpenCL sources**
          - [`common/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/common/): general C++ sources with a lot of stuffs and experiments (with [README](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/common/README.md) and Makefile to compile and to generate results)
          - [`sequential/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/sequential/): final C++ sources for sequential implementation
          - [`threads/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/threads/): final C++ sources for multi-threads implementation
          - [`mpi/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/mpi/): final C++ sources for OpenMPI implementation
          - [`opencl/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/opencl/): final C++ sources for OpenCL implementation
          - [`libs/cxxtest-4.4/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/src/libs/cxxtest-4.4/): CxxTest to allow unit tests even on systems that have not installed it
      - [`benchmark/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/benchmark): final benchmarks results
      - [`graphs/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/graphs/): some graphs in PostScript and SVG formats
      - [`Hydra/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/Hydra/): results collected on the [Hydra](https://hpc.ulb.be/hydra.php) cluster
      - [`tables/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/progs/tables/): some results in TSV and HTML formats
  - [`miscellaneous/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/miscellaneous/): some experiments (essentially in Python) and programs to precalculate some values
  - `report_src/`: (*coming soon*) LaTeX sources of the main report
  - [`slides_src/`](https://bitbucket.org/OPiMedia/parallel-sigma_odd-problem/src/master/slides_src/): LaTeX sources of the presentation slides


**Online documentation** of the code:
<http://www.opimedia.be/DS/online-documentations/parallel-sigma_odd-problem/html/>



## Brief statement of the problem and Examples

Let σ(n) = sum of all divisors of n
and **σ\_odd(n) = sum of *odd* divisors of n**

For n = 18,
all divisors are: {1, 2, 3, 6, 9, 18}.

*Odd* divisors are: {1, 3, 9}, so σ\_odd(18) = 13.

If we **iterate the σ\_odd(n) function**,
we see that we always end up at fixed point 1.

For example if we start with n = 18:

  - σ\_odd(18) = 1 + 3 + 9 = 13
  - σ\_odd(13) = 1 + 13 = 14
  - σ\_odd(14) = 1
  - σ\_odd(1) = 1

We **conjecture** that is true for all n (integer > 0).

We have numerically verified this conjecture until n = 194,835,486,825 ≃ 1.94 × 10^11 ≃ 1.4 × 2^37.

Instead the function σ(n), we can iterate on the modified function ς\_odd(n):
**ς\_odd(n) = σ(n) divided by 2 until to be odd**

For example if we start with n = 18:

  - ς\_odd(18) = 1 + 3 + 9 = 13
  - ς\_odd(13) = (1 + 13)/2 = 14/2 = 7
  - ς\_odd(7) = (1 + 7)/(2^3) = 8/(2^3) = 1
  - ς\_odd(1) = 1

Paths of odd integers ≤ 101 and of odd integers ≤ 1001:

![varsigma_odd problem circular](http://www.opimedia.be/probleme-3n-plus-1/_png/varsigma_odd--101-odd--332x800.png)
![varsigma_odd problem circular](http://www.opimedia.be/probleme-3n-plus-1/_png/varsigma_odd--1001-odd--circular--225x200.png)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬

🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/>

- 📧 <olivier.pirson.opi@gmail.com>
- Mastodon: <https://mamot.fr/@OPiMedia> — Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)

Copyright (C) 2017-2020 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



![varsigma_odd problem](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2019/Nov/05/11550091-5-parallel-sigma_odd-problem-logo_avatar.png)
