#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
nb_divisors.py

Usage: nb_divisors.py [n]

Print the complete path of the iteration of the number of divisors function
and the complete path of the iteration of the number of odd divisors function,
for all numbers from 2 to n.

February 24, 2018

GPLv3 --- Copyright (C) 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import sys

N = 101


#
# Functions
###########
def nb_divisors(n):
    """
    Return the number of divisors of n.
    (By inefficient algorithm.)
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    nb = 0
    for d in range(1, n + 1):
        if n % d == 0:
            nb += 1

    return nb


def nb_divisors_odd(n):
    """
    Return the number of odd divisors of n.
    (By inefficient algorithm.)
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    nb = 0
    for d in range(1, n + 1, 2):
        if n % d == 0:
            nb += 1

    return nb


#
# Main
######
def main():
    """
    Main
    """
    for start_n in range(2, N + 1):
        n = start_n
        print(n, end=' ')
        while n != 2:
            n = nb_divisors(n)
            print(n, end=' ')

        n = start_n
        print('\t{}'.format(n), end=' ')
        while n != 1:
            n = nb_divisors_odd(n)
            print(n, end=' ')

        print()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            N = int(sys.argv[1])
        except:
            pass

    main()
