/* -*- coding: latin-1 -*- */
/** \file div2.cpp (November 18, 2017)
 *
 * Benchmarks on several implementations of the divide_until_odd() function.
 *
 * GPLv3 --- Copyright (C) 2017 Olivier Pirson
 * http://www.opimedia.be/
 */

// \cond
#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <cstring>  // for ffsl, https://linux.die.net/man/3/ffs
#include <strings.h>  // for ffs

#include <chrono>
#include <iostream>
// \endcond



/* *******
 * Macro *
 *********/
#define STR(x) STRINGIFY(x)

#define STRINGIFY(x) #x


#define TRIAL(nbbits, f, shift) {                                       \
    std::cout << #f;                                                    \
    std::cout.flush();                                                  \
                                                                        \
    const std::chrono::steady_clock::time_point                         \
      clock_start = std::chrono::steady_clock::now();                   \
                                                                        \
    for (uint ## nbbits ## _t i = 1; i <= NB; ++i) {                    \
      const uint ## nbbits ## _t n = i << shift;                        \
                                                                        \
      if (n > 0) {                                                      \
        volatile const uint ## nbbits ## _t odd = f(n);                 \
                                                                        \
        assert(odd == static_cast<uint ## nbbits ## _t>(divide_until_odd_64__ffsl(n))); \
      }                                                                 \
    }                                                                   \
                                                                        \
    const double                                                        \
      duration = std::chrono::duration<double>(std::chrono::steady_clock::now() \
                                               - clock_start).count();  \
                                                                        \
    std::cout << ":\t" << duration << 's' << std::endl;                 \
    std::cout.flush();                                                  \
  }



/* ***********
 * Functions *
 *************/
inline
uint32_t
divide_until_odd_32__asm(uint32_t n) {
  assert(n != 0);

  __asm__
    ("bsf %1, %%ecx\n"  // bsf: http://x86.renejeschke.de/html/file_module_x86_id_19.html
     "shr %%cl, %0\n"
     : "=r"(n)
     : "r"(n)
     : "ecx");

  return n;
}


inline
uint64_t
divide_until_odd_64__asm(uint64_t n) {
  assert(n != 0);

  __asm__
    ("bsf %1, %%rcx\n"  // bsf: http://x86.renejeschke.de/html/file_module_x86_id_19.html
     "shr %%cl, %0\n"
     : "=r"(n)
     : "r"(n)
     : "rcx");

  return n;
}


inline
uint32_t
divide_until_odd_32__ffs(uint32_t n) {
  assert(n != 0);

  const int i = ffs(n);  // https://linux.die.net/man/3/ffs

  assert(i > 0);

  return n >> (i - 1);
}


inline
uint64_t
divide_until_odd_64__ffsl(uint64_t n) {
  assert(n != 0);

  const int i = ffsl(n);  // https://linux.die.net/man/3/ffs

  assert(i > 0);

  return n >> (i - 1);
}


inline
uint32_t
divide_until_odd_32__while(uint32_t n) {
  assert(n != 0);

  while (n % 2 == 0) {
    n >>= 1;
  }

  return n;
}


inline
uint64_t
divide_until_odd_64__while(uint64_t n) {
  assert(n != 0);

  while (n % 2 == 0) {
    n >>= 1;
  }

  return n;
}


inline
uint32_t
id_32(uint32_t n) {
  return n;
}


inline
uint64_t
id_64(uint64_t n) {
  return n;
}



/* ******
 * Main *
 ********/
int
main() {
  const unsigned int NB = 2000000000;

    std::cout <<
#ifdef __clang__
      "clang " STR(__clang_major__) "." STR(__clang_minor__) "." STR(__clang_patchlevel__)
#else
#ifdef __GNUG__
      "GNU C++ " STR(__GNUG__) "." STR(__GNUC_MINOR__) "." STR(__GNUC_PATCHLEVEL__)
#else
      "unknown"
#endif
#endif
              << "\tsizeof(int): " << sizeof(int)
              << "\tsizeof(long int): " << sizeof(long int)
              << "\tNB: " << NB

#ifdef OPTI_ASM
              << "\tOPTI_ASM"
#endif
#ifndef NDEBUG
              << "\tDEBUG mode!"
#endif
              << std::endl;

  assert(sizeof(int) >= 4);
  assert(sizeof(long int) >= 8);

  for (unsigned int shift = 0; shift <= 16; ++shift) {
    std::cout << std::endl
              << "=== On naturals shifted by " << shift << " ===" << std::endl;
#ifdef NDEBUG
    TRIAL(32, id_32, shift);
    TRIAL(64, id_64, shift);
#endif

    TRIAL(32, divide_until_odd_32__asm, shift);
    TRIAL(64, divide_until_odd_64__asm, shift);

    TRIAL(32, divide_until_odd_32__ffs, shift);
    TRIAL(64, divide_until_odd_64__ffsl, shift);

    TRIAL(32, divide_until_odd_32__while, shift);
    TRIAL(64, divide_until_odd_64__while, shift);
  }

  return EXIT_SUCCESS;
}
