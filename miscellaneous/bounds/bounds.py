#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
bounds.py

Usage: bounds.py

Upper bounds for half number difference.

February 9, 2018

GPLv3 --- Copyright (C) 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math


#
# Constants
###########
ALPHA = 0.425

ALPHABIS = 0.7

BETA = 0.40580406

EULER_GAMMA = 0.577215664901532860606512090082402431042


#
# Functions
###########
def floor_lg(n):
    return int(math.floor(math.log(n, 2)))


def upper_bound(k):
    return (1/2.0 * math.log((2*k + 1)**2/(2.0*(k + 1 if k % 2 == 0
                                                else k)))
            + EULER_GAMMA/2 + upper_bound_fraction_terms(k))


def upper_bound_fraction_terms(k):
    return 1/(24*(k**2 + k + BETA)) - 1/(12*((k + 1 if k % 2 == 0
                                              else k)**2 + ALPHABIS))


def upper_bound_fraction_terms_even():
    return - (2.3 + 2*BETA)/(232.8*(6 + BETA))


def upper_bound_fraction_terms_odd():
    return - (2.3 + 2*BETA)/(40.8*(2 + BETA))


def upper_bound_lg(k):
    return (floor_lg(k) + 3.6)/2


#
# Main
######
def main():
    """
    Main
    """
    print(EULER_GAMMA/2 + upper_bound_fraction_terms_odd())
    print(EULER_GAMMA/2 + upper_bound_fraction_terms_even())

    print(upper_bound_fraction_terms_odd())
    print(upper_bound_fraction_terms_even())

    for k in range(1, 51):
        print(k, upper_bound_fraction_terms(k), upper_bound(k), upper_bound_lg(k),
              upper_bound_lg(k) - upper_bound(k),
              math.pow(k + 2, 1/4.0) - upper_bound(k),
              math.pow(k + 2, 1/4.0) - upper_bound_lg(k),
              sep='\t')

if __name__ == '__main__':
    main()
