#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
pow3_div2.py

December 7, 2017

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function


def padic2(n):
    nb = 0
    while n % 2 == 0:
        n //= 2
        nb += 1

    return nb


#
# Main
######
def main():
    """
    Main
    """
    for alpha in range(1, 100):
        n = 3**alpha - 1
        nb = padic2(n)
        print(alpha, nb, n, sep='\t')

        if alpha % 2 == 0:
            assert nb >= 3, (alpha, nb, n)
            if alpha % 4 != 0:
                assert nb == 3, (alpha, nb, n)
            elif alpha % 8 != 0:
                assert nb == 4, (alpha, nb, n)
            elif alpha % 16 != 0:
                assert nb == 5, (alpha, nb, n)
            else:
                assert nb >= 6, (alpha, nb, n)

            assert nb == padic2(alpha) + 2, (padic2(alpha), alpha, nb, n)
        else:
            assert nb == 1, (alpha, nb, n)

if __name__ == '__main__':
    main()
