#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
offsets.py

Usage: offsets.py [n]

Calculate offsets to iterate on possible prime numbers.
Used to put precalculated offsets in C++ codes.

November 13, 2017

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import sys

PRIMES = (2, 3, 5, 7, 11, 13, 17)


#
# Functions
###########
def remains(n):
    """
    Return a tuple of modulos coprime with the primorial of n,
    and a tuple of corresponding offsets.

    :param n: 0 <= int <= len(PRIMES)

    :return: (tuple of int, tuple of int)
    """
    assert isinstance(n, int), type(n)
    assert 0 <= n <= len(PRIMES), n

    primo = primorial(n)
    rems_set = set(range(primo))

    for prime in PRIMES:
        for rem in frozenset(rems_set):
            if (primo % prime == 0) and (rem % prime == 0):
                rems_set.remove(rem)

    rems = tuple(sorted(rems_set))
    offs = []

    for i in range(1, len(rems)):
        offs.append(rems[i] - rems[i - 1])
    offs.append(primo + rems[0] - rems[-1])

    assert primo == sum(offs), (primo, sum(offs))

    return (rems, tuple(offs))


def primorial(n):
    """
    Return the primorial of n, i.e. the product of the n first prime numbers.

    https://oeis.org/A002110

    :param n: 0 <= int <= len(PRIMES)

    :return: int >= 1
    """
    assert isinstance(n, int), type(n)
    assert 0 <= n <= len(PRIMES), n

    product = 1

    for i in range(n):
        product *= PRIMES[i]

    return product


#
# Main
######
def main():
    """
    Main
    """
    print('n:\tprimor.\t#\t%\tremains\toffsets')
    for n in range(len(PRIMES) + 1):
        primo = primorial(n)
        rems, offs = remains(n)

        print('{}:\t{}\t{}\t{:.1f}%\t{}{}\t{}{}'
              .format(n, primo, len(rems),
                      float(len(rems)*100)/primo,
                      rems[:10], ('' if len(rems) <= 10
                                  else '...'),
                      offs[:10], ('' if len(offs) <= 10
                                  else '...')))
        sys.stdout.flush()

    n = 3
    try:
        n = max(0, int(sys.argv[1]))
    except (IndexError, ValueError):
        pass

    if n <= len(PRIMES):
        primo = primorial(n)
        rems, offs = remains(n)

        print('\n{}:\t{}\t{}\t{:.1f}%'.format(n, primo, len(rems),
                                              float(len(rems)*100)/primo))
        print('Remains:', rems)
        print('Offsets:', offs)

if __name__ == '__main__':
    main()
