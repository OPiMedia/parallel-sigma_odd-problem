#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
harmonic.py

Usage: harmonic.py [options]

Options:
  --first   first number to check (1 by default)
  --last    last number to check (50 by default)
  --nb      number of numbers to check

Print from 1 to last
several values linked to harmonic number
and Global Upper Bound for the sigma_odd function.

November 15, 2017

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import fractions
import math
import sys


#
# Constant
##########
EULER_GAMMA = 0.577215664901532860606512090082402431042


#
# Functions
###########
def diff_half_harmonic_upper_bound(n):
    """
    Return an upper bound of H_n - 1/2 H_k with k = floor(n/2).

    Use DeTemple 1991 inequality:
    http://mathworld.wolfram.com/HarmonicNumber.html

    :return: int > 0
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    return (((math.log(float((n*2 + 1)**2) / ((n+1)*2)) + EULER_GAMMA) / 2 -
             (1.0/((n+2)**2) - 1.0/(n**2)) / 12)
            if is_even(n)
            else ((math.log(float((n*2 + 1)**2) / (n*2)) + EULER_GAMMA) / 2 -
                  (1.0/((n+1)**2) - 1.0/(n**2)) / 12))


def divide_until_odd(n):
    """
    Return n divided by 2 until the result is odd.

    :param n: int > 0

    :return: int
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    while is_even(n):
        n //= 2

    return n


def floor(x):
    """
    Return the floor integer value of x.

    :param x: float or Fraction

    :return: int
    """
    assert isinstance(x, float) or isinstance(x, fractions.Fraction), x

    return int(math.floor(x))


def floor_square_root(n):
    """
    Return floor(sqrt(n))

    :param n: int >= 0

    :return: int >= 0
    """
    assert isinstance(n, int), type(n)
    assert n >= 0, n

    sqrt = floor(math.sqrt(n))

    assert sqrt**2 <= n < (sqrt + 1)**2, n

    return sqrt


def gub(n):
    """
    Return the Global Upper Bound for sigma_odd(n).

    :param n: int > 0

    :return: Fraction > 0
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    sqrt_n = floor_square_root(n)
    sqrt_n1 = floor_square_root(n - 1)

    return sum_odd(sqrt_n) + floor(harmonic_odd(sqrt_n1)*n)


def harmonic_odd(n):
    """
    Return 1/1 + 1/3 + 1/5 + 1/7 + ... + (1/n or 1/(n-1)).

    :param n: int >= 0

    :return: Fraction > 0
    """
    assert isinstance(n, int), type(n)
    assert n >= 0, n

    harm = fractions.Fraction(0)

    for i in range(1, n + 1, 2):
        harm += fractions.Fraction(1, i)

    return harm


def is_even(n):
    """
    Return true iff n is even.

    :param n: int >= 0

    :return: int
    """
    assert isinstance(n, int), type(n)
    assert n >= 0, n

    return n & 1 == 0


def sum_odd(n):
    """
    Return 1 + 3 + 5 + 7 + ... + (n or n-1) = k^2 with k floor((n+1)/2)

    sum_odd(n - 1) = https://oeis.org/A008794

    :param n: int > 0

    :return: int > 0
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    return ((n + 1)//2)**2


def sum_floor_n_harmonic_odd(n, to_n):
    """
    Return floor(n/1) + floor(n/3) + floor(n/5) + floor(n/7)
    + ... + (n/to_n or floor(1/(to_n-1))).

    :param n: int >= 0
    :param to_n: int >= 0

    :return: int > 0
    """
    assert isinstance(n, int), type(n)
    assert n >= 0, n

    assert isinstance(to_n, int), type(to_n)
    assert to_n >= 0, n

    harm = 0

    for i in range(1, to_n + 1, 2):
        harm += floor(fractions.Fraction(n, i))

    return harm


def sum_odd_divisors(n):
    """
    Calculates the sum of odd divisors of n by the naive method
    and returns it.

    https://oeis.org/A000593

    :param n: int > 0

    :return: int > 0
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    n = divide_until_odd(n)

    sum_odd_div = 0

    sqrt_n = floor_square_root(n)

    for i in range(1, sqrt_n + 1, 2):
        if n % i == 0:
            sum_odd_div += i + n//i

    if sqrt_n**2 == n:
        sum_odd_div -= sqrt_n

    return sum_odd_div


def sum_odd_divisors_upper_bound__detemple(n):
    """
    Return an upper bound of sum odd divisors of  n
    using the DeTemple inequalities.

    :param n: int > 0

    :return: int > 0
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    sqrt_n = floor_square_root(n)
    sqrt_n1 = floor_square_root(n - 1)

    return ((((sqrt_n + 1)//2)**2 +
             floor(diff_half_harmonic_upper_bound(sqrt_n1)*n))
            if n > 1
            else 1)


#
# Main
######
def main():
    """
    Main
    """
    print_fractions = False

    # Read command line parameters
    first = 1
    last = 50
    i = 1
    while i < len(sys.argv):
        param = sys.argv[i]

        if param == '--first':
            i += 1
            try:
                first = max(1, int(sys.argv[i]))
            except (IndexError, ValueError):
                pass
        elif param == '--last':
            i += 1
            try:
                last = max(1, int(sys.argv[i]))
            except (IndexError, ValueError):
                pass
        elif param == '--nb':
            i += 1
            try:
                last = first + max(0, int(sys.argv[i])) - 1
            except (IndexError, ValueError):
                pass

        i += 1

    # Print table legend
    print('n\tsqrt\t|s_odd\tbound\tGUB\tDeTemp.\t|sum_o.\t|Half bound\tdiff h.\tfl.harm._odd\tharmonic_odd')

    # Calculate and print data
    for n in range(first, last + 1):
        sqrt_n = floor_square_root(n)
        sqrt_n1 = floor_square_root(n - 1)

        exact = sum_odd_divisors(n)

        bound = (sum_odd(sqrt_n) +
                 sum_floor_n_harmonic_odd(n, floor_square_root(n - 1)))
        gub_bound = gub(n)
        bound_detemple = sum_odd_divisors_upper_bound__detemple(n)
        sum_odd_n = sum_odd(n)

        diff_half = (0 if sqrt_n1 == 0
                     else diff_half_harmonic_upper_bound(sqrt_n1))
        h_odd = harmonic_odd(n)

        print(n, sqrt_n,
              exact, bound, gub_bound, bound_detemple,
              sum_odd_n,
              '{:.10f}'.format(diff_half_harmonic_upper_bound(n)),
              sum_floor_n_harmonic_odd(n, n),
              '{:.10f}'.format(diff_half),
              '{:.10f}'.format(float(h_odd)),
              sep='\t', end='')
        if print_fractions:
            print('\t', h_odd, sep='', end='')
        print()
        sys.stdout.flush()

        assert exact <= bound, (n, exact, bound)
        assert exact <= gub_bound, (n, exact, gub_bound)
        assert exact <= bound_detemple, (n, exact, bound_detemple)

        assert bound <= gub_bound, (n, bound, gub_bound)
        assert gub_bound <= bound_detemple, (n, gub_bound, bound_detemple)

        assert exact <= sum_odd_n, (n, exact, sum_odd_n)
        assert (n in (2, 4)) or (bound <= sum_odd_n), (n, bound, sum_odd_n)

if __name__ == '__main__':
    main()
