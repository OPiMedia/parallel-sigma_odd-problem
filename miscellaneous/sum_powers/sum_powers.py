#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
sum_powers.py

Usage: sum_powers.py [n]

Try some formulas of sum of powers
with lower and upper bounds.

November 5, 2019

GPLv3 --- Copyright (C) 2018, 2019 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math
import sys

N = 101


#
# Functions
###########
def associated_sum_powers(x, k, n):
    """
    :return: float
    """
    assert isinstance(x, int), type(x)
    assert x > 1, x

    assert isinstance(k, int), type(k)
    assert k > 0, k

    assert isinstance(n, int), type(n)
    assert n > 0, n

    s = 0.0
    power = x

    for i in range(x, k + 1):
        if i == power:
            s += n / power
            power *= x

    return s


def floor_log(x, base):
    r"""
    Returns :math:`floor(\log_{base}(x))`.

    :return: int
    """
    assert isinstance(x, int), type(x)
    assert x >= 1, x

    assert isinstance(base, int), type(base)
    assert base >= 2, base

    if base == 1:
        return 0

    i = 0
    power = 1

    while power <= x:
        i += 1
        power *= base

    return i - 1


def sum_powers(x, k):
    r"""
    Returns :math:`\sum_{j=1, x^j<=k} x^j`.

    :return: int
    """
    assert isinstance(x, int), type(x)
    assert x >= 2, x

    assert isinstance(k, int), type(k)
    assert k >= 1, k

    s = 0
    power = x

    for i in range(x, k + 1):
        if i == power:
            s += power
            power *= x

    return s


def sum_powers_formula(x, k):
    r"""
    Returns sum_powers(x, k) == :math:`\sum_{j=1, x^j<=k} x^j`
    by direct computation
    :math:`\frac{x (x^{floor(\log_{base}(x))} - 1)}{x - 1}`.

    :return: int
    """
    assert isinstance(x, int), type(x)
    assert x > 1, x

    assert isinstance(k, int), type(k)
    assert k > 0, k

    return x * (x**floor_log(k, x) - 1) // (x - 1)


def sum_powers_lower_bound(x, k):
    """
    Strict lower bound of sum_powers(x, k).

    :return: float
    """
    assert isinstance(x, int), type(x)
    assert x > 1, x

    assert isinstance(k, int), type(k)
    assert k > 0, k

    return (k - x) / (x - 1)


def sum_powers_upper_bound(x, k):
    """
    Upper bound of sum_powers(x, k).
    """
    assert isinstance(x, int), type(x)
    assert x > 1, x

    assert isinstance(k, int), type(k)
    assert k > 0, k

    return x * (k - 1) / (x - 1)


#
# Main
######
def main():
    """
    Main
    """
    x = 3
    for n in range(1, N + 1):
        k = int(math.floor(math.sqrt(n)))
        print(n, k,
              sum_powers_lower_bound(x, k),
              sum_powers(x, k),
              sum_powers_formula(x, k),
              sum_powers_upper_bound(x, k),
              associated_sum_powers(x, k, k),
              sep='\t')

        assert sum_powers(x, k) == sum_powers_formula(x, k), (x, k)
        assert sum_powers_lower_bound(x, k) < sum_powers(x, k), (x, k)
        assert sum_powers(x, k) <= sum_powers_upper_bound(x, k), (x, k)

if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            N = int(sys.argv[1])
        except ValueError:
            pass

    main()
