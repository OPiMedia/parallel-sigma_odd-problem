#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
nbbits.py

Usage: nbbits.py

Print the number of bits of sigma_odd(n)
computable with on k bits.

December 6, 2017

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math


#
# Functions
###########
def bound_pow9_8(n):
    """
    Return an upper bound of GUB(n).
    """
    return int(math.floor(math.pow(n, 9.0/8)*2))


def bound_sqrt8(n):
    """
    Return an upper bound of GUB(n).
    """
    return int(math.floor((math.pow(n - 1, 1.0/8) + 0.5)*n))


def ceil_lg(n):
    """
    Return the logarithm of base 2 of n, rounded to the ceil.
    """
    return int(math.ceil(math.log(n, 2)))


#
# Main
######
def main():
    """
    Main
    """
    print('k:\tsqrt8\tpow9_8')
    for k in range(1, 64):
        n = 2**k
        print(k, ceil_lg(bound_sqrt8(n)), ceil_lg(bound_pow9_8(n)), sep='\t')

if __name__ == '__main__':
    main()
