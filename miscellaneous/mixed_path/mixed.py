#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
mixed.py

Usage: mixed.py [n]

Print a graph in dot format of all mixed path
for all numbers from 1 to n.

February 24, 2018

GPLv3 --- Copyright (C) 2017, 2018 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import math
import sys

N = 101


#
# Functions
###########
def is_even(n):
    """
    Return True iff n is even.
    """
    assert isinstance(n, int), type(n)

    return (n % 2) == 0


def is_square(n):
    """
    Return True iff n is a perfect square.
    """
    assert isinstance(n, int), type(n)
    assert n >= 0, n

    return int(math.sqrt(n))**2 == n


def odd(n):
    """
    Return n divided by 2 until to be odd.
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    while is_even(n):
        n //= 2

    return n


def sigma_odd(n):
    """
    Return the sum of odd divisors of n.
    (By inefficient algorithm.)
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    s = 0
    for d in range(1, n + 1, 2):
        if n % d == 0:
            s += d

    return s


def varsigma_odd(n):
    """
    Return the sum of odd divisors of n,
    divided by 2 until to be odd.
    (By inefficient algorithm.)
    """
    assert isinstance(n, int), type(n)
    assert n > 0, n

    return odd(sigma_odd(n))


#
# Main
######
def main():
    """
    Main
    """
    colors = {sigma_odd: 'black',
              varsigma_odd: 'blue',
              odd: 'lightgray'}
    fs = (sigma_odd, varsigma_odd, odd)

    # Collect numbers n
    ns = set((1, ))
    for i, f in enumerate(fs):
        for started_n in range(1, N + 1, 1):
            n = started_n
            result = 0
            while result != 1:
                ns.add(n)
                result = f(n)
                n = result
                if f == odd:
                    break

    # Collect graphs
    graphs = [dict(), dict(), dict()]
    for i, f in enumerate(fs):
        for started_n in sorted(ns):
            n = started_n
            result = 0
            while result != 1:
                result = f(n)
                graphs[i][n] = (result, colors[f])
                n = result
                if f == odd:
                    break

    # Print graph
    print("""digraph "sigma odd 1 to {} (sigma_odd in black, varsigma_odd in blue, Odd in gray)" {{
  rankdir="LR";

  1 [style=filled];""".format(N))

    for n in ns:
        if (n != 1) and is_square(n):
            print('  {} [color=orange, style=filled];'.format(n))

    for graph in graphs:
        for n, result in graph.items():
            print('{} -> {} [color={}];'.format(n, *result))

    print('}')

if __name__ == '__main__':
    if len(sys.argv) > 1:
        try:
            N = int(sys.argv[1])
        except:
            pass

    main()
