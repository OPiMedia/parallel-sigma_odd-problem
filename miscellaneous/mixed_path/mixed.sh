#!/bin/sh

for N in 21 51 101
do
    echo === Build for 1 to $N ===
    ./mixed.py $N > mixed_1_$N.dot

    dot -Teps mixed_1_$N.dot > mixed_1_$N.eps
    dot -Tsvg mixed_1_$N.dot > mixed_1_$N.svg
    epstopdf -o mixed_1_$N.pdf mixed_1_$N.eps
done
