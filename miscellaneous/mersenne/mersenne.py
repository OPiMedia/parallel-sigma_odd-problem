#!/usr/bin/env python
# -*- coding: latin-1 -*-

"""
mersenne.py

Usage: mersenne.py

Calculate % of numbers useless to check in the sigma_odd problem
with respect to some Mersenne prime numbers considered.

December 8, 2017

GPLv3 --- Copyright (C) 2017 Olivier Pirson
http://www.opimedia.be/
"""

from __future__ import division
from __future__ import print_function

import sys

MERSENNE_PRIMES = (3, 7, 31)


#
# Functions
###########
def remains(n):
    """
    Return a tuple of products coprime with 2*("Mersenne primorial" of n)^2,
    and a tuple of corresponding offsets.

    :param n: 0 <= int <= len(MERSENNE_PRIMES)

    :return: (tuple of int, tuple of int)
    """
    assert isinstance(n, int), type(n)
    assert 0 <= n <= len(MERSENNE_PRIMES), n

    product = (mersenne_primorial(n)**2)*2
    rems_set = set(range(1, product, 2))

    for prime in MERSENNE_PRIMES:
        for rem in frozenset(rems_set):
            if (product % prime == 0) and (rem % prime == 0) and (rem % (prime**2) != 0):
                rems_set.remove(rem)

    rems = tuple(sorted(rems_set))
    offs = []

    for i in range(1, len(rems)):
        offs.append(rems[i] - rems[i - 1])
    offs.append(product + rems[0] - rems[-1])

    assert product == sum(offs), (product, sum(offs))

    return (rems, tuple(offs))


def mersenne_primorial(n):
    """
    Return the Mersenne primorial" of n,
    the product of the n first Mersenne prime numbers.

    :param n: 0 <= int <= len(MERSENNE_PRIMES)

    :return: int >= 1
    """
    assert isinstance(n, int), type(n)
    assert 0 <= n <= len(MERSENNE_PRIMES), n

    product = 1

    for i in range(n):
        product *= MERSENNE_PRIMES[i]

    return product


#
# Main
######
def main():
    """
    Main
    """
    print('n:\tMersen.\tproduc.\t#\t%\tremains\toffsets')
    for n in range(len(MERSENNE_PRIMES) + 1):
        product = (mersenne_primorial(n)**2)*2
        rems, offs = remains(n)

        print('{}:\t{}\t{}\t{}\t{:.1f}%\t{}{}\t{}{}'
              .format(n, ((MERSENNE_PRIMES[n - 1]) if n > 0
                          else 'x'),
                      product, len(rems),
                      float(len(rems)*100)/product,
                      rems[:10], ('' if len(rems) <= 10
                                  else '...'),
                      offs[:10], ('' if len(offs) <= 10
                                  else '...')))
        sys.stdout.flush()

if __name__ == '__main__':
    main()
