# sigma\_odd — `big_data/`

This directory must be contains the file `prime28.bin` (56 MiB)
that contains all prime numbers < 2^28.

This file is available in the compressed file `prime28.bin.gz` (19 MiB).
Decompress it by `gzip -d -k prime28.bin.gz`.

For experiments,
a bigger file (775 MiB) with all prime numbers < 2^32
is also available in an compressed file on Internet:

  - `prime32.7z` (165 MiB):
      - <http://bit.ly/2nHygp3>
      - or <https://mega.nz/#!jQtWGY5Y!jHj0PXwJ52Rd69cSuFzo_X_3lk-N5GJHC5JiiR0K7wE>
  - `prime32.bin.gz` (272 MiB):
      - <http://bit.ly/2kHrzOU>
      - or <https://mega.nz/#!jU9FDRBK!wLfU4YncXYR5fZbUTqRGNZX7eL_j6BlG2j4mSr3YHBM>
